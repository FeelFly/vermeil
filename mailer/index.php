<?php

if(!isset($_POST)){
	exit();
}

parse_str($_POST['data'], $data);
foreach($data as $k => $i){
	$data[$k] = trim($i);
}


$to  = "info@servero.ru ";
$subject = "Новый заказ сервера";

$headers  = "Content-type: text/html; charset=UTF-8 \r\n";
$headers .= "From: От кого письмо <from@example.com>\r\n";
$headers .= "Reply-To: reply-to@example.com\r\n";

$message = '<div>ФИО: '.$data['fio'].'</div>
<div>Телефон: '.$data['phone'].'</div>
<div>Email: '.$data['email'].'</div>
<div>Комментарий: '.$data['comment'].'</div>
<hr>
<div>'.$data['order'].'</div>';

mail($to, $subject, $message, $headers);

echo 'ok';