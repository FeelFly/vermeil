<?php

// no direct access
defined('_JEXEC') or die;

?>


<?php
if ($this->mid <= 0)
{
	JHtml::_('stylesheet', JUri::root().'modules/mod_jsfilter/assets/layout/default/style.css');
	
	echo '<script type="text/javascript">'
			.'MJSF_TARGETS['.$this->mid.']="'.$this->ajaxSelector.'";'
		.'</script>';

	echo '<div id="jsfilter_ajax_sample" style="display:none;">'
			.'<div class="">'
				.'<div class="loader">'
					.'<img src="'.JUri::root().'media/system/images/modal/spinner.gif" />'
				.'</div>'
				.'<div class="msg">'
					.JText::_('MJSF_LOADER_MSG')
				.'</div>'
			.'</div>'
		.'</div>';

	echo '<form id="smart_filter_'.$this->mid.'" name="smart_filter_'.$this->mid.'" action="'.JUri::base().'index.php?option=com_jshopping&controller=jsfilter" method="post">';
}

	$currTmpl = dirname(__FILE__).'/../search/sortpanel.php';

	if ( JFile::exists($currTmpl) )
	{
		include($currTmpl);
	}
	else
	{
		include(JPATH_ROOT."/components/com_jshopping/templates/default/search/sortpanel.php");
	}

if ($this->mid <= 0)
{
?>	
	<input id="mid" type="hidden" name="mid" value="<?php echo $this->mid; ?>" />
	<input id="sf_orderby" type="hidden" name="sf_orderby" value="<?php echo $this->orderby; ?>" />
	<input id="sf_order" type="hidden" name="sf_order" value="<?php echo $this->order; ?>" />
	<input id="id" type="hidden" name="id" value="<?php echo $this->mid; ?>" />
	<input type="hidden" name="sf_start" value="" />
	<input type="hidden" name="category_id" value="<?php echo JRequest::getInt('category_id'); ?>" />
	<input type="hidden" name="manufacturer_id" value="<?php echo JRequest::getInt('manufacturer_id'); ?>" />
	<?php
	if ($this->showStockSelector)
	{
		echo '<input id="stock_state" type="hidden" name="sf[999][stock][checkbox][]" value="'.( ($this->stockVal == 1) ? 'true' : '' ).'" />';
	}
	?>
</form>
<?php
}
?>
