<?php

// no direct access
defined('_JEXEC') or die;
?>

<div id="<?php echo $this->panelId; ?>" class="sf_panel">
	<?php
	if ($this->sortFields)
	{
	?>
		<div class="sf_sortbtn">
			<div class="title"><?php echo JText::_('MJSF_SORT_PANEL_TITLE'); ?></div>
			<?php
			foreach ($this->sortFields as $field => $name)
			{
				echo '<div class="item" rel="'.$field.'" onclick="sf_doSort(\'smart_filter_'.$this->mid.'\', this);">'
						.$name
						.'<span class="sort'
							.( ($this->orderby == $field && $this->order) ? ' '.$this->order : '' )
						.'"></span>'
					.'</div>';
			}
			?>
		</div>
	<?php
	}
	?>

	<?php
	if ($this->limitList)
	{
	?>
		<div class="sf_sortcount">
			<?php
			echo JText::_('MJSF_PRODUCTS_ON_PAGE')
				.'&nbsp;'
				.JHtml::_(
					'select.genericlist',
					$this->limitList,
					'sf_limit',
					'onchange="sf_onChangeLimits(this, '.$this->mid.')" autocomplete="off"',
					'value',
					'text',
					$this->limit,
					'sf_limit'
				);
			?>
		</div>
	<?php
	}
	?>
	
	<?php
	if ($this->stockVal >= 0)
	{
	?>
		<div class="sf_stock">
			<select name="sf_dummy[stock]" onchange="sf_doPostFilter('smart_filter_<?php echo $this->mid; ?>', this);">
				<option value="0" <?php echo (!$this->stockVal) ? 'selected="selected"' : ''; ?>>
					<?php echo JText::_('MJSF_STOCK_INACTIVE_NAME'); ?>
				</option>
				<option value="1" <?php echo ($this->stockVal) ? 'selected="selected"' : '' ?>>
					<?php echo JText::_('MJSF_STOCK_ACTIVE_NAME'); ?>
				</option>
			</select>
		</div>
	<?php
	}
	?>

	<?php
	if ($this->show_product_count)
	{
	?>
		<div class="sf_prodcount">
			<?php echo _JSHOP_DISPLAY_NUMBER.": "; ?>
			<?php echo $this->product_count; ?>
        </div>
	<?php
	}
	?>
</div>
