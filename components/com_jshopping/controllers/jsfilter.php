<?php

// no direct access
defined('_JEXEC') or die;
if ( !defined('DS') ) define('DS', DIRECTORY_SEPARATOR);

if ( version_compare(JPlatform::getShortVersion(), '12.0') >= 0 ) {
	// Joomla 3.x
	define('IS_J2x', false);
} else {
	// Joomla 2.5
	define('IS_J2x', true);
}

// Вывод только сообщений об ошибках (иначе предупреждения могут мешать работе скрипта при ajax'е)
error_reporting(E_ERROR);


jimport('joomla.application.component.controller');

require_once( JPATH_ROOT.DS.'modules'.DS.'mod_jsfilter'.DS.'helper'.DS.'helper.php' );


if (IS_J2x) {
	class JshoppingControllerJsfilterBase extends JController {}
} else {
	class JshoppingControllerJsfilterBase extends JControllerLegacy {}
}

class JshoppingControllerJsfilter extends JshoppingControllerJsfilterBase
{
    
    // фильтрация товаров через helper
    // и вывод данных через view search
    function display ( $cachable = false, $urlparams = array() )
    {
		$app		= JFactory::getApplication(); 
		$db			= JFactory::getDbo();
		$id			= JRequest::getInt('id', 0);
		$jsCfg		= JSFactory::getConfig();
		$jslang		= JSFactory::getLang();
		$dispatcher = JDispatcher::getInstance();
		$sf 		= JsfilterHelper::getParamsFromRequest();

		// Подготовка хелпера к фильтрации
		$cfg = JsfilterHelper::getConfig($id);
		$helper	= new JsfilterHelper();
		$helper->loadConfig($cfg);
		// Передача параметров фильтрации в хелпер
		// и получение набора ID товаров, удовлетворяющих данным условиям
		$pids = $helper->doFilter($sf);

		
		if ( !count($pids) )
		{
			// нет результатов
            $view_name = "search";
            $view_config = array("template_path" => JPATH_COMPONENT."/templates/".$jsCfg->template."/".$view_name);
            $view = &$this->getView($view_name, getDocumentType(), '', $view_config);

            $view->setLayout("noresult");
            $view->display();
            
            return 0;
        }

        // Формирование хэша параметров фильтрации
        $hash = JRequest::getCmd('sf_params', '');

        if (!$hash && $sf)
        {
        	$hash = $this->buildHash($sf);

			if ($hash)
			{
				echo '<div id="sf_hash" style="display:none">'.$hash.'</div>';
			}
        }

		$this->_show_result($pids, $helper);
	}


	// ------------------------------------------------------------------------
	//					Обработчик AJAX-запросов фильтра
	// ------------------------------------------------------------------------
	
	public function request ()
	{
		$app		= JFactory::getApplication(); 
		$db			= JFactory::getDbo();
		$jsCfg		= JSFactory::getConfig();
		$jslang		= JSFactory::getLang();
		$id			= JRequest::getInt('id', 0);
		$pre 		= JRequest::getInt('pre', 0);
		$sf 		= JsfilterHelper::getParamsFromRequest();


		// Подготовка хелпера к фильтрации
		if ($id >= 0)
		{
			$cfg = JsfilterHelper::getConfig($id);
		}
		else
		{
			$cfg = array('mid' => -1);
		}
		
		$baseCfg = JsfilterHelper::getBaseCfg();
		$helper	= new JsfilterHelper();
		$helper->loadConfig($cfg);


		// Передача параметров фильтрации в хелпер
		// и получение набора ID товаров, удовлетворяющих данным условиям
		$pids = $helper->doFilter($sf);

		if ($pre) {
			// Предзапрос. Вывод подсказки с количеством найденых товаров
			$result = array(
				'status'	=> 1,
				'count'		=> count($pids)
			);

			// Добавление списка актуальных значений
			if ( $cfg['deactivate_values'] ) {
				$values = $helper->getActiveValues($pids);
				$result['values'] = $values;
			}

			echo json_encode($result);

		} else {

			// Вывод (части) страницы с результатом поиска

			// Получение актуальных значений для элементов фильтра и вывод результата в скрытом блоке
			if ( $cfg['deactivate_values'] ) {
				$values = $helper->getActiveValues($pids);
				echo '<div id="sf_actual_values" style="display:none">'.json_encode($values).'</div>';
			}

			// Формирование хэша параметров фильтрации
			$hash = JRequest::getCmd('sf_params', '');

	        if (!$hash && $sf)
	        {
	        	$hash = $this->buildHash($sf);

				if ($hash)
				{
					echo '<div id="sf_hash" style="display:none">'.$hash.'</div>';
				}
	        }
	        
			$this->_show_result($pids, $helper);
		}

		$app->close();
	}


	// ------------------------------------------------------------------------
	//					Обработчик сброса параметров фильтрации
	// ------------------------------------------------------------------------
	
	public function reset ()
	{
		$sess = JFactory::getSession();
		$sess->clear('jsfilter.params.cid');
		$result = array('status' => 1);

		// Загрузка актуальных значений при активации элемента доп.фильтрации по наличию товара
		// (значение "только в наличии")
		$cfgId = JRequest::getUint('id', 0);
		$cfg = JsfilterHelper::getConfig($cfgId);
		$baseCfg = JsfilterHelper::getBaseCfg();

		if ($baseCfg->stock_state == 1 && $cfg['deactivate_values']) {
			// Фильтрация товаров только по наличию

			$helper	= new JsfilterHelper();
			$helper->loadConfig($cfg);
			
			$sf = array();
			$sf[999]['stock']['checkbox'][] = 'true';
			$pids = $helper->doFilter($sf);
			// Определение актуальных значений после искусственной фильтрации
			$activeValues = $helper->getActiveValues($pids);

			$result['values'] = $activeValues;
		}

		echo json_encode($result);

		$app = JFactory::getApplication();
		$app->close();
	}


	// ------------------------------------------------------------------------
	//					Формирование списка найденных товаров
	// ------------------------------------------------------------------------

	private function _show_result (&$pids, &$helper)
	{
		$app		= JFactory::getApplication(); 
		$db			= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$mid		= $helper->getModuleId();
		$jsCfg		= JSFactory::getConfig();
		$jslang		= JSFactory::getLang();
		$dispatcher = JDispatcher::getInstance();
		$cid		= JRequest::getInt('category_id');
		$cfgId 		= JRequest::getInt('id', 0);
		$cfg 		= JsfilterHelper::getConfig($cfgId);
		$baseCfg 	= JsfilterHelper::getBaseCfg();


		// выбор из БД данных, необходимых для отрисовки каталога
		$context	= "jshoping.searclist.front.product";
		$orderby 	= $app->getUserStateFromRequest($context.'orderby', 'sf_orderby', 	$cfg['default_sorting'] || $baseCfg->sort[0],		'cmd');
        $order		= $app->getUserStateFromRequest($context.'order', 	'sf_order', 	(($cfg['default_sorting_dir']) ? 'desc' : 'asc'), 	'cmd');
        $limit 		= $app->getUserStateFromRequest($context.'limit', 	'sf_limit',		$jsCfg->count_products_to_page, 					'int');
        $limit		= ($limit) ? $limit : $jsCfg->count_products_to_page;
        $limitstart	= $app->getUserStateFromRequest($context.'sf_start', 'sf_start', 0, 'int');

        // Проверка наличия аддона "Product price User group"
		$addon = JTable::getInstance('addon', 'jshop');
		$addon->loadAlias('user_group_product_price');
		$addonPresent = ($addon->id);
		$userGid = 0;

		if ($addonPresent)
		{
			$jsUser = JSFactory::getUserShop();
			$userGid = $jsUser->usergroup_id;
		}

        
		$adv_query	= "";
		$adv_from 	= "";
		$filters	= ($cid && $pids === null) ? array( 'categorys' => $helper->buildCatList($cid) ) : array();
		$product	= JTable::getInstance('product', 'jshop');
		$adv_result = $product->getBuildQueryListProductDefaultResult();
		$product->getBuildQueryListProduct("search", "list", $filters, $adv_query, $adv_from, $adv_result);

		switch ( $orderby )
		{
			case "name":
				$order_query = "prod.`".$jslang->get('name')."`";
				$order_query .= ( $order == 'desc' ) ? ' DESC' : '';
			break;

			case "price":
				$order_query = ($userGid)
								? "IF (
	                               		ppg.`price` > 0,
			                            ppg.`price`,
			                            IF (
											prod.`min_price`,
											prod.`min_price`,
											prod.`product_price`
										)
									) / cr.`currency_value`"
								: "IF (
										prod.`min_price`,
										prod.`min_price`,
										prod.`product_price`
									) / cr.`currency_value`";
				$order_query .= ( $order == 'desc' ) ? ' DESC' : '';
			break;

			case "hits":
				$order_query = "prod.`hits`";
				$order_query .= ( $order == 'desc' ) ? ' DESC' : '';
			break;

			case "date":
				$order_query = "prod.`date_modify`";
				$order_query .= ( $order == 'desc' ) ? ' DESC' : '';
			break;

			case "mnf":
				$order_query = "mnf.`".$jslang->get('name')."`";
				$order_query .= ( $order == 'desc' ) ? ' DESC' : '';
			break;

			case "qnty":
				$order_query = "prod.`unlimited`";
				$order_query .= ( $order == 'desc' ) ? ' DESC' : '';

				$order_query .= ", prod.`product_quantity`";
				$order_query .= ( $order == 'desc' ) ? ' DESC' : '';
			break;

			default:
				$order_query = "prod.`product_id`";
			break;
		}
		
		// Поддержка плагина цен для групп
		$dispatcher->trigger('onBeforeQueryGetProductList', array("search", &$adv_result, &$adv_from, &$adv_query, &$order_query, &$filters) );

		$query 	= 	"SELECT SQL_CALC_FOUND_ROWS
						$adv_result,
						prod.`product_price` as pr,
						cr.`currency_value` as curr,
						prod.`product_price` / cr.`currency_value` as price
					FROM `#__jshopping_products` AS prod
					LEFT JOIN `#__jshopping_products_to_categories` AS pr_cat
						ON pr_cat.`product_id` = prod.`product_id`
					LEFT JOIN `#__jshopping_categories` AS cat
						ON pr_cat.`category_id` = cat.`category_id`
					LEFT JOIN `#__jshopping_manufacturers` AS mnf
						ON ( prod.`product_manufacturer_id` = mnf.`manufacturer_id` )
					LEFT JOIN `#__jshopping_currencies` AS cr
						ON ( prod.`currency_id` = cr.`currency_id` )
					$adv_from
					WHERE
						prod.`product_publish` = 1
					".( ($pids)
							? "AND prod.product_id IN ( ".join(',', $pids).")"
							: ( ($pids === null) ? '' : "AND 1 != 1" )
					)."
					".( ( $adv_query )
							? $adv_query
							: ""
					)."
					GROUP BY prod.`product_id`
					ORDER BY ".$order_query;

        $db->setQuery($query, $limitstart, $limit);
        $rows = $db->loadObjectList();

        $db->setQuery("SELECT FOUND_ROWS()");
        $total = $db->loadResult();

        $rows = listProductUpdateData($rows);
        addLinkToProducts($rows, 0, 1);


		$_review = JTable::getInstance('review', 'jshop');
        $allow_review = $_review->getAllowReview();
        
        $dispatcher->trigger( 'onBeforeDisplayProductList', array(&$rows) );

        jimport('joomla.html.pagination');
        $pagination = new JPagination($total, $limitstart, $limit, 'sf_');
        $pagenav 	= $pagination->getPagesLinks();
        

        $view_name = ($baseCfg->prod_tmpl) ? $baseCfg->prod_tmpl : "category";
        $view = $this->getView( $view_name, getDocumentType() );


        // Добавление директории по умолчанию для поиска шаблонов (с минимальным приоритетом)
        $pathList = $view->get('_path');
        $pathList = $pathList['template'];

        array_pop($pathList);
        $pathList = array_reverse($pathList);

        // Шаблон по умолчанию (запасной вариант для панели)
        $view->addTemplatePath(JPATH_COMPONENT."/templates/default/".$view_name);
        // Директория текущего шаблона (приоритет выше, чем у предыдущего)
        if ($jsCfg->template != 'default')
        {
        	$view->addTemplatePath(JPATH_COMPONENT."/templates/".$jsCfg->template."/".$view_name);
        }

        // Повторное добавление предыдущих путей шаблонов
        foreach ($pathList as $tmplPath)
        {
        	$view->addTemplatePath($tmplPath);
        }


        if ($rows)
        {
			$view->setLayout("products");
			$view->assign('display_list_products', 1);
		} else {
			if ($view_name == 'search')
			{
				$view->setLayout("noresult");
			} else {
				$view->setLayout("products");
				$view->assign('display_list_products', 0);
			}
		}


        // Данные для панели сортировки
        $view->assign("mid", 				$mid);
        $view->assign("orderby", 			$orderby);
        $view->assign("order", 				$order);
        $view->assign("limit", 				$limit);
        $view->assign('product_count',		$total);


		// Список товаров        
        $view->assignRef('total', 						$total);
        $view->assignRef('config', 						$jsCfg);
        $view->assign('template_block_list_product',	"list_products/list_products.php");
        $view->assign('template_block_form_filter', 	"list_products/form_filters.php");
        $view->assign('template_block_pagination', 		"list_products/block_pagination.php");
        $view->assign('path_image_sorting_dir', 		$jsCfg->live_path.'images/'.$image_sort_dir);
        $view->assign('filter_show', 					0);
        $view->assign('filter_show_category', 			0);
        $view->assign('filter_show_manufacturer',		$jsCfg->product_list_show_manufacturer);
        $view->assignRef('pagination', 					$pagenav);
        $view->assign('display_pagination', 			$pagenav!="");
        $view->assign('sorting', 						$panel);
        $view->assign('count_product_to_row', 			$jsCfg->count_products_to_row);
        $view->assignRef('rows',						$rows);
        $view->assign('allow_review', 					$allow_review);
        $view->assign('shippinginfo',					SEFLink('index.php?option=com_jshopping&controller=content&task=view&page=shipping',1));
        
        $dispatcher->trigger('onBeforeDisplayProductListView', array(&$view) );
        
        $view->display();
	}


	private function buildHash ($params)
	{
		if (!$params) return '';

		$db = JFactory::getDbo();
		$str = serialize($params);
		$hash = md5($str);


		$db->setQuery("SELECT * FROM `#__jsfilter` WHERE `hash` = ".$db->Quote($hash)." LIMIT 1");
		$result = $db->loadObject();

		if ($result)
		{
			// Увеличение счетчика запросов и обновление времени доступа
			$q = "UPDATE `#__jsfilter`
				SET
					`hits` = `hits` + 1,
					`date` = CURDATE()
				WHERE `hash` = ".$db->Quote($hash)."
				LIMIT 1";
			$db->setQuery($q);
			$db->query();
		}
		else
		{
			// Сохранение нового запроса
			$q = "INSERT INTO `#__jsfilter`
				SET
					`hash` = ".$db->Quote($hash).",
					`date` = CURDATE(),
					`hits` = 1,
					`params` = ".$db->Quote($str);
			$db->setQuery($q);
			$db->query();
		}

		return $hash;
	}
}
?>
