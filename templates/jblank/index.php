<?php
/**
 * J!Blank Template for Joomla by JBlank.pro (JBZoo.com)
 *
 * @package    JBlank
 * @author     SmetDenis <admin@jbzoo.com>
 * @copyright  Copyright (c) JBlank.pro
 * @license    http://www.gnu.org/licenses/gpl.html GNU/GPL
 * @link       http://jblank.pro/ JBlank project page
 */

defined('_JEXEC') or die;


// init $tpl helper
require dirname(__FILE__) . '/php/init.php';

?><?php echo $tpl->renderHTML(); ?>
<head>
	
	    <?php $tpl->css('/templates/jblank/bootstrap-3.3.7-dist/css/bootstrap.css'); ?>
	   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	
	<script src="/templates/jblank/js/h/jquery.matchHeight.js" type="text/javascript"></script>
	<script src="/templates/jblank/js/h/themes.js" type="text/javascript"></script>

	
	
	
	
<?php $tpl->css('jshopping.css'); ?>

    <jdoc:include type="head" />
</head>
<body class="<?php echo $tpl->getBodyClasses(); ?>">

    <!-- show modules and components -->
  
        <!-- show Joomla messages -->
        <?php if ($tpl->isError()) : ?>
            <jdoc:include type="message" />
      
      
      
        <?php endif; ?>





<div class="site">
	
	
	
	
	

    <div class="container">

         <div class="head_block" >

            <div class="row"  >

                <div class="col-sm-4 col-md-3">

                    <jdoc:include type="modules" name="logo"/>

                </div>
                <div class="col-sm-5 col-md-3" style="padding-top: 20px">



                    <jdoc:include type="modules" name="search"/>


                </div>
             <div class=" col-md-3 hidden-sm " >



                    <jdoc:include type="modules" name="phone"/>


                </div>
                
                 <div class="col-sm-3 col-md-3 ">



                    <jdoc:include type="modules" name="cart"/>


                </div>
                
            
            </div>
            
         </div>    
	</div>


  <div class=menu_block><div class=container><jdoc:include type="modules" name="menu"/></div></div> 


       
      <?php if ($this->countModules('sliders')) : ?>

<div class=container>
            <jdoc:include type="modules" name="sliders" style="xhtml"/>
            
                </div>    
               
            
              </div>    
               
            <div class=preim>  <div class="container">
<jdoc:include type="modules" name="preim" style="xhtml"/></div></div>


 <div class="container">


       <div style="margin-top: 20px;">  
           
                     <jdoc:include type="modules" name="user1" style="xhtml"/></div>

 </div>
            
 <div class=gray_block>
              <div class="container">
	             <jdoc:include type="modules" name="otz" style="xhtml"/>
        </div>


</div>
         
         
          
    
    
    
         
         

 <?php else: ?>
	
	
	
	 
      <?php if(strpos($_SERVER['REQUEST_URI'], 'product') !== false || strpos($_SERVER['REQUEST_URI'], 'category') !== false || strpos($_SERVER['REQUEST_URI'], 'catalog') !== false  || strpos($_SERVER['REQUEST_URI'], 'configurator') !== false )  : ?>

	
	       
	
	
 <div class=content>
  <div class="container ">
            <div class="row"  >

                <div class="col-md-3">

                  <jdoc:include type="modules" name="left" style="xhtml"/>


                </div>
                <div class="col-md-9 ">
                    
                 

                          <div class=nav> <jdoc:include type="modules" name="nav" style="xhtml"/></div>
					
					
					<jdoc:include type="message" />    <jdoc:include type="component"/>

                    
                    
                </div>
                
            </div>


    </div>
 
 </div>

   
         

 <?php else: ?>
	
 <div class=content>
  <div class="container ">
            <div class="row"  >

               
                <div class="col-md-12 ">
                    
                 
  <div class=nav> <jdoc:include type="modules" name="nav" style="xhtml"/></div>
                      
					
					
					<jdoc:include type="message" />    <jdoc:include type="component"/>

                    
                    
                </div>
                
            </div>


    </div>
 
 </div>

	
	
	
	
	<?php endif; ?>

	
	
	
	
	
<?php endif; ?>

    

<div class=green_line></div>
             <div class=bottom_block>
                 
                  <div class="container">
	         
	         
	         
	         
	          <div class="row"  >

                <div class="col-md-4">  <jdoc:include type="modules" name="bottom_menu1" style="xhtml"/></div>
	                 <div class="col-md-4"> <jdoc:include type="modules" name="bottom_menu2" style="xhtml"/></div>
	                  
	                       <div class="col-md-4"> <jdoc:include type="modules" name="desite" style="xhtml"/></div>
	          </div>   
	                
	                
	                
	                 </div> 
	                
         </div>
         
        
	  
<div class="modal fade bs-example-modal-md" id="form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
					 aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
   
      <div class="modal-body"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		  
		  <h2 style="text-align: center"> Перезвоните мне</h2>
		  
      <jdoc:include type="modules" name="form" style="xhtml"/>
      </div>
    </div>
  </div>
</div>
    


	  

</body></html>