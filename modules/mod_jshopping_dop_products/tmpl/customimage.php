<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="jshop dop_products owlcarousel custom_image load-hidden <?php print $moduleclass_sfx; if ($nav_position!=0) print " topnav";?> <?php print "button-size-".$button_size;?>" <?php if ($carousel_width) {print 'style="max-width:'.$carousel_width.'px"';}?>>

<div id="owl-carousel-id-<?php print $module->id; ?>" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">

<?php 
$custom_fields = json_decode($params->get('dopprodModFields'),true);

if ($custom_fields){
	$cf_count=count($custom_fields['dopprodModFieldsCount'])-1;
	$html="";
	for ($i=0; $i <= $cf_count; $i++) {
		//Open First div
		$html .=' 
		<div class="modopprod_item '.$custom_fields['custom_image_color'][$i].' '.$custom_fields['custom_image_style'][$i].' '.$custom_fields['custom_image_font'][$i].'">
		';
			//Image
			if ($custom_fields['c_image'][$i]!="-1"  && !$custom_fields['v_image'][$i]) {
				$html .='
					<div class="custom_image">
						<img src="/images/custom_image/'.$custom_fields['c_image'][$i].'" alt="" />
					</div>
				';
			}
						
			//Begin caption
			if ($carousel_caption_position=='0') {
				$html .='
					<div class="owlcarousel-caption" style="position:absolute; bottom:0; top:0; left:0; right:0; width:100%;">
				';
			} else {
				$html .='
				<div class="owlcarousel-caption">
			';
			}
			
			//Image Header + text
			$html .='
				<div class="pos_middle">
			';
			
			if ($custom_fields['h_image'][$i]){
				$html .='
					<div class="pos_middle_header">
						<h3>'.$custom_fields['h_image'][$i].'</h3>
					</div>
				';
			}
			
			if ($custom_fields['e_image'][$i]){
				$html .='
					<div class="pos_middle_text">
						<h3>'.$custom_fields['e_image'][$i].'</h3>
					</div>
				';
			}
			
			//End Image Header + text
			$html .='
				</div>
			';
			
			
			//End caption
			$html .='
				</div>
			';	
        //Close First div	
        $html .='
		</div>
		';		
	}
	print $html;
    
} ?>
</div>
<div class="cleared"></div>
</div>