<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php foreach($dop_cat as $curr){ ?>
<div class="modopprod_item <?php if ($autoheight=="1") {print "autoheight";}?>" data-id="<?php print $curr["id"] ?>">
  	<?php if ($show_image) { ?>
       <div id="modid_<?php print $module->id;?>_item_image_<?php print $curr["id"];?>" class="modopprod_item_image">
           <a href="<?php print $curr["link"];?>">
           <img src = "<?php print $jshopConfig->image_category_live_path?>/<?php if ($curr["img"]) print $curr["img"]; else print $noimage?>" alt="<?php print $curr["name"]?>" />         
           </a>
       </div>
       <?php } ?>
       
       <div class="modopprod_item_name">
           <a href="<?php print $curr["link"];?>"><?php print $curr["name"];?></a>
       </div>
       
        <?php if ($show_sd) { ?>
       	<div class="modopprod_item_sd"><?php print $curr["sd"];?></div> 
        <?php } ?> 
  </div>
 <?php } ?>