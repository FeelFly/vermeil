<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="jshop dop_products brooksusCarousel" <?php if ($carousel_width) print 'style="max-width:'.$carousel_width.'px"'; ?>>
<div id="brooksusCarousel_<?php print $module->id; ?>" class="carousel slide">
<div class="carousel-inner">
<?php foreach($dop_prod as $curr){ ?>
<?php $buyLink = SEFLink('index.php?option=com_jshopping&controller=cart&task=add&category_id='.$curr->category_id.'&product_id='.$curr->product_id, 1);
?>
  <div class="modopprod_item item">
	 <?php 
          if ($show_image) {
              include( dirname(__FILE__).'/__imgblock.php' );
          } 
      ?>                   
  <div class="carousel-caption" <?php if ($carousel_caption_position=='1') print 'style="position:relative;"'; ?>>
       <p class="modopprod_item_name lead">
           <a href="<?php print $curr->product_link?>"><?php print $curr->name?></a>
      </p>
      <?php if ($show_buylink) { 
	  	if ($show_attr) {	
				print modJshopping_dop_productsHelper::showAttributes($products, $curr, $module);
			}
			if ($show_qty) {
				print modJshopping_dop_productsHelper::showQty($curr, $buyLink, $minus="-", $plus="+", $module );
			}
	  ?>
      <div class="modopprod_item_buylink">
      <p class="lead">
       <a id="modid_<?php print $module->id;?>_buy_item_<?php print $curr->product_id ?>" class="button_buy btn btn-danger" href="<?php print ($buyLink);?>"><?php include( dirname(__FILE__).'/__priceblock.php' ); ?> <?php echo _JSHOP_BUY?></a></p>
      </div>
      <?php } ?> 
      <p class="modopprod_item_sd <?php if($show_sd=="2"){print 'show-hovered';}?>"><?php if ($show_sd) { ?>
       <?php print $curr->short_description; ?> 
        <?php } ?>
       </p>    
  </div>
		</div>	      
<?php } ?>
</div>
<!-- Carousel nav -->
  <a class="carousel-control left" href="#brooksusCarousel_<?php print $module->id; ?>" data-slide="prev"><span class="<?php print $nav_left_bs;?>"></span></a>
  <a class="carousel-control right" href="#brooksusCarousel_<?php print $module->id; ?>" data-slide="next"><span class="<?php print $nav_right_bs;?>"></span></a>
</div>
</div>
<div class="cleared"></div>