<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="mod-butons-block">
<?php if ($show_buylink) {
	$mli=str_replace("_:",$i++."_",$modlayout);
	if ($show_attr) {	
		print '<div class="modopprod_attrib">'.modJshopping_dop_productsHelper::showAttributes($products, $curr, $module, $mli).'</div>';
	}
	if ($show_qty) {
		print '<div class="modopprod_count">'.modJshopping_dop_productsHelper::showQty($curr, $buyLink, $qtyminus, $qtyplus, $module, $mli).'</div>';
	}
?>
    <div class="modopprod_item_buylink <?php if ($show_comparelink || $show_wllink) print "display-inline" ?>">
        <a id="modid_<?php print $module->id;?>_<?php print $mli;?>_buy_item_<?php print $curr->product_id ?>" class="button_buy btn list-btn" href="<?php print ($buyLink);?>"><span class="fa fa-shopping-cart"> <?php echo _JSHOP_BUY?></span></a>
    </div>
<?php } ?>
<?php if ($show_wllink) { ?>
    <div class="modopprod_item_buylink wllink">
        <?php  print modJshopping_dop_productsHelper::setWishlistButtons($curr, $params->get('view_wllink','icon-heart'));?>
    </div>
<?php } ?>
<?php if ($show_comparelink) { ?>
    <div class="modopprod_item_buylink comparelink">
        <?php  print modJshopping_dop_productsHelper::setCompareButtons($curr, $params->get('view_comparelink','icon-signal'));?>
    </div>
<?php } ?> 
</div>