<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="modid_<?php print $module->id;?> jshop dop_products default owlcarousel whiteitem load-hidden <?php print $moduleclass_sfx; if ($nav_position!=0) print " topnav";?> <?php print "button-size-".$button_size;?> <?php if ($add_link) {print "add-link-border";}?>" <?php if ($carousel_width) {print 'style="max-width:'.$carousel_width.'px"';}?>>
    <div id="owl-carousel-id-<?php print $module->id; ?>" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">
        <?php foreach($dop_prod as $curr){
            include( dirname(__FILE__).'/_owlblock.php' );
        } ?>
    </div>
<div class="cleared"></div>
<?php if ($add_link){
	include( dirname(__FILE__).'/__addlink.php' );
} ?>
</div>