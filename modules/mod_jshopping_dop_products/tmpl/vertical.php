<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="modid_<?php print $module->id;?> jshop dop_products vertical owlcarousel load-hidden <?php print $moduleclass_sfx; if ($nav_position!=0) print " topnav";?> <?php print "button-size-".$button_size;?> <?php if ($add_link) {print "add-link-border";}?>" <?php if ($carousel_width) {print 'style="max-width:'.$carousel_width.'px"';}?>>
<div id="owl-carousel-id-<?php print $module->id; ?>" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">
<?php $i=0; ?> 
<div class="modopprod_item clearfix">
<?php foreach($dop_prod as $curr){ 
$buyLink = SEFLink('index.php?option=com_jshopping&controller=cart&task=add&category_id='.$curr->category_id.'&product_id='.$curr->product_id, 1);?>
<?php if ($i && !($i % $vertical_count)){ ?>   
</div>
<div class="modopprod_item  clearfix">
	<?php } ?>
    <?php 
        if ($show_image) {
            include( dirname(__FILE__).'/__imgblock.php' );
        } 
    ?> 
    <div class="owlcarousel-caption clearfix">
        <div class="modopprod_item_name">
           <a href="<?php print $curr->product_link?>"><?php print $curr->name?></a>
        </div>
        <div class="modopprod_item_price jshop_price">
        	<?php include( dirname(__FILE__).'/__priceblock.php' ); ?>
        </div>
        
        <?php include( dirname(__FILE__).'/__buttonsblock.php' );?>
        
        <?php if ($show_sd!="0") {?>
        	<div class="modopprod_item_sd">
				<?php print $curr->short_description; ?>
            </div>
        <?php } ?> 
    </div>    
    <?php $i++; } ?>
</div>
</div>
<div class="cleared"></div>
<?php if ($add_link){
	include( dirname(__FILE__).'/__addlink.php' );
} ?>
</div>