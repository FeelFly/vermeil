<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php $buyLink = SEFLink('index.php?option=com_jshopping&controller=cart&task=add&category_id='.$curr->category_id.'&product_id='.$curr->product_id, 1);?>
<div class="modopprod_item">
	<?php 
		if ($show_image) {
        	include( dirname(__FILE__).'/__imgblock.php' );
     	} 
	 ?> 
    <div class="owlcarousel-caption">
        <div class="modopprod_item_name">
           <a href="<?php print $curr->product_link?>"><?php print $curr->name?></a>
        </div>
        <?php if ($modlayout=="_:whiteitem" || $modlayout=="_:whiteitemblock") { ?>
			<div class="modopprod_item_price jshop_price clearfix">
				<span class="te"><?php include( dirname(__FILE__).'/__priceblock.php' );?></span>
				<div class="pull-right"><?php print showMarkStar($curr->average_rating);?></div>
			</div>
         <?php } else {?>
         	<div class="modopprod_item_price jshop_price">
            	<?php include( dirname(__FILE__).'/__priceblock.php' );?>
        	</div>
         <?php } ?>
        <?php include( dirname(__FILE__).'/__buttonsblock.php' );?>
        
        <?php if ($show_sd!="0") {?>
        	<div class="modopprod_item_sd <?php if($show_sd=="2"){print 'show-hovered';}?>">
				<?php print $curr->short_description; ?>
            </div>
        <?php } ?>
	</div>
</div>