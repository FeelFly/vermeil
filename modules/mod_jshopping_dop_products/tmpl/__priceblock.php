<?php defined('_JEXEC') or die('Restricted access');?>
<?php if ($show_oldprice && $curr->product_old_price>0){?>
	<span class="mod-old-price"><?php print formatprice($curr->product_old_price);?></span>
<?php } ?>
<span><?php print formatprice($curr->product_price);?></span>