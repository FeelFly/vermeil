<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="modid_<?php print $module->id;?> jshop dop_products default owlcarousel load-hidden <?php print $moduleclass_sfx; if ($nav_position!=0) print " topnav";?> <?php print "button-size-".$button_size;?>" <?php if ($carousel_width) {print 'style="max-width:'.$carousel_width.'px"';}?>>

<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a class="js-dp-tabs" data-tabType="hits" href="#hits-dp-<?php print $module->id; ?>" aria-controls="hits-dp-<?php print $module->id; ?>" role="tab" data-toggle="tab"><?php print $hits_text; ?></a></li>
   <?php if ($bs_show){?>
    	<li role="presentation"><a class="js-dp-tabs" data-tabType="bs" href="#bs-dp-<?php print $module->id; ?>" aria-controls="bs-dp-<?php print $module->id; ?>" role="tab" data-toggle="tab"><?php print $bs_text; ?></a></li>
    <?php } ?>
    <?php if ($new_show){?>
    	<li role="presentation"><a class="js-dp-tabs" data-tabType="new" href="#new-dp-<?php print $module->id; ?>" aria-controls="new-dp-<?php print $module->id; ?>" role="tab" data-toggle="tab"><?php print $new_text; ?></a></li>
    <?php } ?>
    <?php if ($label_show){?>
    	<li role="presentation"><a class="js-dp-tabs" data-tabType="label" href="#label-dp-<?php print $module->id; ?>" aria-controls="label-dp-<?php print $module->id; ?>" role="tab" data-toggle="tab"><?php print $label_text; ?></a></li>
    <?php } ?>
    <?php if ($customid_show){?>
    	<li role="presentation"><a class="js-dp-tabs" data-tabType="customid" href="#customid-dp-<?php print $module->id; ?>" aria-controls="customid-dp-<?php print $module->id; ?>" role="tab" data-toggle="tab"><?php print $customid_text; ?></a></li>
    <?php } ?>
 </ul>
 
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="hits-dp-<?php print $module->id; ?>">
        <div id="owl-carousel-id-<?php print $module->id; ?>-hits" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">
        <?php $dop_class="_hits_dp"; foreach($dop_prod_hits as $curr){include( dirname(__FILE__).'/_owlblock.php' );}?>
        </div>
	</div>
    <?php if ($bs_show){?>
        <div role="tabpanel" class="tab-pane" id="bs-dp-<?php print $module->id; ?>">
            <div id="owl-carousel-id-<?php print $module->id; ?>-bs" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">
            <?php $dop_class="_bs_dp"; foreach($dop_prod_bs as $curr){include( dirname(__FILE__).'/_owlblock.php' );}?>
            </div>
        </div>
    <?php } ?>
    
    <?php if ($new_show){?>
        <div role="tabpanel" class="tab-pane" id="new-dp-<?php print $module->id; ?>">
            <div id="owl-carousel-id-<?php print $module->id; ?>-new" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">
            <?php $dop_class="_new_dp"; foreach($dop_prod_last as $curr){include( dirname(__FILE__).'/_owlblock.php' );}?>
            </div>
        </div>
    <?php } ?>
    
    <?php if ($label_show){?>
        <div role="tabpanel" class="tab-pane" id="label-dp-<?php print $module->id; ?>">
            <div id="owl-carousel-id-<?php print $module->id; ?>-label" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">
            <?php $dop_class="_label_dp"; foreach($dop_prod_label as $curr){include( dirname(__FILE__).'/_owlblock.php' );}?>
            </div>
        </div>
    <?php } ?>
    
    <?php if ($customid_show){?>
        <div role="tabpanel" class="tab-pane" id="customid-dp-<?php print $module->id; ?>">
            <div id="owl-carousel-id-<?php print $module->id; ?>-customid" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">
            <?php $dop_class="_customid_dp"; foreach($dop_prod as $curr){include( dirname(__FILE__).'/_owlblock.php' );}?>
            </div>
        </div>
    <?php } ?>
</div>
<div class="cleared"></div>
</div>