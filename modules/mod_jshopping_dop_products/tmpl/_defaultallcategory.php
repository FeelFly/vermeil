<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php foreach($dop_cat as $curr){ ?>
<div class="modopprod_item">
	<?php if ($show_image) { ?>
        <div id="modid_<?php print $module->id;?>_item_image_<?php print $curr["id"];?>" class="modopprod_item_image">
            <a href="<?php print $curr["link"];?>">
            <img src = "<?php print $jshopConfig->image_category_live_path?>/<?php if ($curr["img"]) print $curr["img"]; else print $noimage?>" alt="<?php print $curr["name"]?>" />         
            </a>
        </div>
    <?php } ?> 
    <div class="owlcarousel-caption" <?php if ($carousel_caption_position=='0') print 'style="position:absolute; bottom:0px; left:0; right:0; width:100%;"'; ?>>
        <div class="modopprod_item_name">
        	<a href="<?php print $curr["link"];?>"><?php print $curr["name"];?></a>
        </div>
        <?php if ($show_sd!="0") {?>
        	<div class="modopprod_item_sd">
        		<?php print $curr["sd"];?>
        	</div> 
        <?php } ?> 
    </div>
</div>	      
<?php } ?>