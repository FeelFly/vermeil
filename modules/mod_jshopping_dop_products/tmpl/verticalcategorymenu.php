<?php defined('_JEXEC') or die('Restricted access'); 
if (!empty($dop_cat)) { ?>
<div id="wrap_<?php print $module->id;?>" class="jshop dop_products category_menu vertical-resp <?php print $moduleclass_sfx; if ($animation_fadeto=="1") {print " fade-to";} if ($large_first=="1"){print " large-first";}?> ">
<?php 
$previousLevel = 0;
foreach($dop_cat as $curr) {?>
<?php if ($previousLevel && $curr["level"] < $previousLevel){?>
		<?php print str_repeat("</ul></ul>", ($previousLevel - $curr["level"]));?>
	<?php } ?>
	<?php if ($curr["level"]==1){ ?>		
		<?php print "<li>".$curr["name"]."</li>"; ?>       
	<?php } ?>
<?php $previousLevel = $curr["level"];?>
<?php } ?>
<?php if ($previousLevel > 1){//close last item tags?>
	<?php echo str_repeat("</ul></ul>", ($previousLevel-1) );?>
<?php } ?>
</div>
<?php }?>
<div class="cleared"></div>