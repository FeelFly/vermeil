<?php defined('_JEXEC') or die('Restricted access'); ?>
    <div id="modid_<?php print $module->id;?>_item_image_<?php print $curr->product_id.$dop_class;?>" class="modopprod_item_image <?php print $add_class_v; ?>" data-bylink="<?php print ($buyLink);?>">
        <?php if ($curr->_label_image && $curr->_label_name && $show_label=="1"){?>
            <div class="product_label"><img src="<?php print $curr->_label_image;?>" alt="<?php print $curr->_label_name;?>" /></div>
        <?php } 
            if (!$curr->_label_image && $curr->_label_name && $show_label=="1") { ?>
                <div class="product_label"><span class="label_name"><?php print $curr->_label_name;?></span></div>
        <?php } ?>
        <a href="<?php print $curr->product_link?>">
            <?php if ($jshop_img=='2'){ ?>
                <img <?php print $inline_style; ?> src = "<?php print $jshopConfig->image_product_live_path?>/<?php if ($curr->product_thumb_image) print str_replace('thumb','full',$curr->product_thumb_image); else print $noimage?>" alt="<?php print $curr->name?>" />
            <?php } ?>
            <?php if ($jshop_img=='1'){ ?>
                <img <?php print $inline_style; ?> src = "<?php print $jshopConfig->image_product_live_path?>/<?php if ($curr->product_name_image) print $curr->product_name_image; else print $noimage?>" alt="<?php print $curr->name?>" />
            <?php } ?>
            <?php if ($jshop_img=='0'){ ?>
                <img <?php print $inline_style; ?> src = "<?php print $jshopConfig->image_product_live_path?>/<?php if ($curr->product_thumb_image) print $curr->product_thumb_image; else print $noimage?>" alt="<?php print $curr->name?>" />
            <?php } ?>
        </a>
        <?php if ($modlayout=="_:vertical") {?>
        	<div class="rating">
				<?php print showMarkStar($curr->average_rating);?>
        	</div>
        <?php } ?>
    </div>