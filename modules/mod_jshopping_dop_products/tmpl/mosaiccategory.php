<?php defined('_JEXEC') or die('Restricted access'); ?>
<div id="wrap_<?php print $module->id;?>" class="jshop dop_products mosaic-block <?php print $moduleclass_sfx; if ($animation_fadeto_mosaic=="1") {print " fade-to";}?>">
    <div class="mosaic <?php if ($show_childcat==0) print "hidden-childcat"?>">
        <?php if ($catids[0]!=""){ ?>
        
            <?php include( dirname(__FILE__).'/_mosaicselectcategory.php' ); ?>
        
        <?php } else { ?>
        
            <?php include( dirname(__FILE__).'/_mosaicallcategory.php' ); ?>
        
        <?php } ?>
    </div>
</div>
<div class="cleared"></div>