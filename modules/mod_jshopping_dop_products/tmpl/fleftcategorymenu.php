<?php defined('_JEXEC') or die('Restricted access'); 
if (!empty($dop_cat)) { ?>
<div id="wrap_<?php print $module->id;?>" class="jshop dop_products category_menu fleft <?php print $moduleclass_sfx; if ($animation_fadeto=="1") {print " fade-to";} if ($large_first=="1"){print " large-first";}?> ">
<?php 
$previousLevel = 0;
foreach($dop_cat as $curr) {?>
	<?php if ($previousLevel && $curr["level"] < $previousLevel){?>
		<?php print str_repeat("</div></div>", ($previousLevel - $curr["level"]));?>
	<?php } ?>
    <?php if ($curr["active"]){
		$active_class="active-category";
	}?> 
	<?php if ($curr["parent"]){?>
		<?php if ($curr["level"] == 1){?>
			<div class="modopprod_item <?php if ($curr["active"]){ print "have-active-category";} if ($autoheight=="1") {print " autoheight";}?>" data-id="<?php print $curr["id"] ?>">
					<div class="modopprod_item_name js-open-subcategory js-parentcategory <?php if ($curr["active"]){ print "active-category";}?>">
           			<i class="fa fa-plus-square-o"></i>&nbsp;<a href="<?php print $curr["link"];?>"><?php print $curr["name"];?></a>
       				</div>
				
				<div class="level_<?php print $curr["level"]?> <?php if ($curr["level"]>0) {print "hidden-xs";}?>">
		<?php } else {?>
				<div class="<?php if ($curr["active"]){ print "level-active-category";}?>">				
					<div class="modopprod_item_name js-open-subcategory js-subcategory">
           			<i class="fa fa-angle-down"></i>&nbsp;<a href="<?php print $curr["link"];?>"><?php print $curr["name"];?></a>
       				</div>
				<div class="level_<?php print $curr["level"]?>">
		<?php }?>
	<?php } else {?>
			<?php if ($curr["level"] == 1){?>
					<div class="modopprod_item <?php if ($autoheight=="1") {print "autoheight";}?>" data-id="<?php print $curr["id"] ?>">			
					<div class="modopprod_item_name <?php if ($curr["active"]){ print "active-category";}?>">
           			<i class="fa fa-minus-square-o"></i>&nbsp;<a href="<?php print $curr["link"];?>"><?php print $curr["name"];?></a>
       				</div>
				</div>
			<?php } else {?>
				<div>
					
					<div class="modopprod_item_name <?php if ($curr["active"]){ print "active-category";}?>">
           			<a href="<?php print $curr["link"];?>"><?php print $curr["name"];?></a>
       				</div>
				</div>
			<?php } ?>
	<?php } ?>
	<?php $previousLevel = $curr["level"];?>
<?php }?>
<?php if ($previousLevel > 1){//close last item tags?>
	<?php echo str_repeat("</div></div>", ($previousLevel-1) );?>
<?php } ?>
</div>
<?php }?>
<div class="cleared"></div>