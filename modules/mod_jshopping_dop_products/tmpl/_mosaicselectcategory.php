<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php foreach($dop_cat as $curr){
if (in_array($curr["id"],$catids,true)){
if ($show_childcat==0) {
	if ($curr["level"]<2){ ?>
  	<div class="mosaic-item modopprod_item <?php if ($curr["parent"]) print "parent-category";?>" data-id="<?php print $curr["id"] ?>">
	 <?php } if ($curr["level"]>1) {?>
	 <div class="child-category">
 	<?php } 
} else {?> 
<div class="mosaic-item modopprod_item" data-id="<?php print $curr["id"];?>">
<?php }?>
       <?php if ($show_image) { ?>
        <div id="modid_<?php print $module->id;?>_item_image_<?php print $curr["id"];?>" class="modopprod_item_image">
           <a href="<?php print $curr["link"];?>">
           <img src = "<?php print $jshopConfig->image_category_live_path?>/<?php if ($curr["img"]) print $curr["img"]; else print $noimage?>" alt="<?php print $curr["name"]?>" />         
           </a>
       </div>
       <?php } ?>
       <div class="<?php if ($curr["level"]<2) print "singlecat";?>">
       <div class="modopprod_item_name level_<?php print $curr["level"];?>" data-id="<?php print $curr["id"];?>">
           <a href="<?php print $curr["link"];?>"><?php if ($show_childcat==0 && $curr["level"]>1) print '<i class="fa fa-angle-right"></i>';?><?php print $curr["name"];?></a>
       </div>
       
        <?php if ($show_sd && $curr["level"]<2) { ?>
       	<div class="modopprod_item_sd"><?php print $curr["sd"];?></div> 
        <?php } ?> 
        </div>  						
	</div>	
             
<?php } } ?>