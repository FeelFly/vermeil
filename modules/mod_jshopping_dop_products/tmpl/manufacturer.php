<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="jshop dop_products default owlcarousel load-hidden <?php print $moduleclass_sfx; if ($nav_position!=0) print " topnav";?> <?php print "button-size-".$button_size;?>" <?php if ($carousel_width) {print 'style="max-width:'.$carousel_width.'px"';}?>>
<div id="owl-carousel-id-<?php print $module->id; ?>" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">

<?php foreach($dop_man as $curr){ 

if (is_array($manuf_prod) && in_array($curr->manufacturer_id, $manuf_prod)) continue;

?>
	<?php if ($link_type=="1") {
			$lt=$curr->link;
			} elseif ($link_type=="2" && $curr->manufacturer_url) {
			$lt=$curr->manufacturer_url;
			} elseif ($link_type=="2" && !$curr->manufacturer_url) {
			$lt=$curr->link;
			}?>
<div class="modopprod_item">
       <?php if ($show_image) { ?>
       <div id="modid_<?php print $module->id;?>_item_image_<?php print $curr->manufacturer_id;?>" class="modopprod_item_image" data-bylink="<?php print ($buyLink);?>">
           <a href="<?php print $lt;?>">
           
           <img src = "<?php print $jshopConfig->image_manufs_live_path."/".$curr->manufacturer_logo;?>" alt="<?php print $curr->name?>" />
                      
           </a>
       </div>
       <?php } ?> 
       <div class="owlcarousel-caption" <?php if ($carousel_caption_position=='0') print 'style="position:absolute; bottom:0px; left:0; right:0; width:100%;"'; if ($show_name=="0") print 'style="padding:0;"';?>>
       <?php if ($show_name!="0") {?>
       <div class="modopprod_item_name">
           <a href="<?php print $lt;?>"><?php print $curr->name;?></a>
       </div>
       <?php } ?>
       
       <?php if ($show_sd!="0") {?>
       <div class="modopprod_item_sd <?php if($show_sd=="2"){print 'show-hovered';}?>"><?php print $curr->short_description; ?></div>
       <?php } ?> 
       </div>
  </div>	      
<?php } ?>
</div>
<div class="cleared"></div>
</div>