<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php $buyLink = SEFLink('index.php?option=com_jshopping&controller=cart&task=add&category_id='.$curr->category_id.'&product_id='.$curr->product_id, 1);?>
    <div class="<?php print $add_class;?> modopprod_item" data-id="<?php print $curr->product_id ?>">
		<?php 
			if ($show_image) {
				include( dirname(__FILE__).'/__imgblock.php' );
			} 
        ?>     
        <div class="modopprod_item_name">
        	<a href="<?php print $curr->product_link?>"><?php print $curr->name?></a>
        </div>
        <?php if ($show_sd) { ?>
        	<div class="modopprod_item_sd">
				<?php print $curr->short_description; ?>
            </div> 
        <?php } ?>
        <div class="modopprod_item_price jshop_price">
        	<?php include( dirname(__FILE__).'/__priceblock.php' ); ?>
        </div>
        
        <?php include( dirname(__FILE__).'/__buttonsblock.php' );?>           
    </div>	   