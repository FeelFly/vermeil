<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="modid_<?php print $module->id;?> jshop dop_products default owlcarousel load-hidden <?php print $moduleclass_sfx; if ($nav_position!=0) print " topnav";?> <?php print "button-size-".$button_size;?>" <?php if ($carousel_width) {print 'style="max-width:'.$carousel_width.'px"';}?>>
    <div id="owl-carousel-id-<?php print $module->id; ?>" class="owl-dp-carousel owl-height <?php if ($mouse_wheel=="1") print 'mouse-wheel'; ?>">
		<?php if ($catids[0]!=""){ ?>
        
        <?php include( dirname(__FILE__).'/_defaultselectcategory.php' ); ?>
        
        <?php } else { ?>
        
        <?php include( dirname(__FILE__).'/_defaultallcategory.php' ); ?>
        
        <?php } ?>
    </div>
<div class="cleared"></div>
</div>