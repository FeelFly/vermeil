<?php
/**
* @version      3.1.0 12.09.2017
* @author       Brooksus
* @package      JoomShopping
* @copyright    Copyright (C) 2016 Brooksite.ru. All rights reserved.
* @license      2016. Brooksite.ru (http://brooksite.ru/litsenzionnoe-soglashenie.html).
*/

defined('_JEXEC') or die('Restricted access');
error_reporting(error_reporting() & ~E_NOTICE);

if (!file_exists(JPATH_SITE.'/components/com_jshopping/jshopping.php')){
JError::raiseError(500,"Please install component \"joomshopping\"");
}

require_once (JPATH_SITE.'/components/com_jshopping/lib/factory.php'); 
require_once (JPATH_SITE.'/components/com_jshopping/lib/functions.php');
JSFactory::loadCssFiles();
JSFactory::loadLanguageFile();
$jshopConfig = JSFactory::getConfig();
$jshopConfig->cur_lang = $jshopConfig->frontend_lang;
$LinkToCart = SEFLink('index.php?option=com_jshopping&controller=cart&task=add', 1);
$noimage = $jshopConfig->noimage ? $jshopConfig->noimage : "noimage.gif";
$document = JFactory::getDocument(); 
$products = JTable::getInstance('product', 'jshop');
require_once dirname(__FILE__).'/helper.php';
	
	//PARAMS
	$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
	$moduleid=$module->id;
	$layout=$params->get('layout', 'default');
	$order_by = $params->get('order_by','prod.product_id');
	$sort_by = $params->get('sort_by','ASC');
	$product_select = $params->get('product_select','');
	$productid = $params->get('productid',1);
	$custom_orderby = $params->get('custom_orderby',0);
	$cat_str = $params->get('catids',NULL); 
	$catids = $params->get('catids');
	$label_str = $params->get('labelid',NULL); 
	$manuf_prod = $params->get('manufacturerid',NULL); 
	$catids_auto = $params->get('catids_auto',0);
	$carousel_caption_position = $params->get('carousel_caption_position',0);
	$show_childcat = $params->get('show_childcat',0);
	$jshop_img = $params->get('jshop_img',2);
	$owl_off = $params->get('owl_off');
	$css_off = $params->get('css_off');
	$show_image = $params->get('show_image',1);
	$show_sd = $params->get('show_sd',0);
	$show_oldprice = $params->get('show_oldprice',0);
	$show_buylink = $params->get('show_buylink',1);
	$show_wllink = $params->get('show_wllink',0);
	$show_comparelink = $params->get('show_comparelink',0);
	$show_name = $params->get('show_name',1);
	$link_type = $params->get('link_type',1);
	$show_label = $params->get('show_label',0);
	$show_attr = $params->get('show_attr',0);
	$show_qty = $params->get('show_qty',0);
	$view_qtyminus = $params->get('view_qtyminus','');
	$view_qtyplus = $params->get('view_qtyplus','');
	$font_awesome=$params->get('font_awesome',0);
	$carousel_width = $params->get('carousel_width','');
	$carousel_img_height = $params->get('carousel_img_height','');
	$resp_desctop = $params->get('resp_desctop','3');
	$resp_tablet = $params->get('resp_tablet','2');
	$resp_mobile = $params->get('resp_mobile','1');
	$loop = $params->get('loop');
	$nav = $params->get('nav');
	$nav_position = $params->get('nav_position',0);
	$nav_left = $params->get('nav_left','icon-chevron-left');
	$nav_right = $params->get('nav_right','icon-chevron-right');
	$button_size = $params->get('button_size',2);
	$nav_left_bs = $params->get('nav_left_bs','navleft-bs');
	$nav_right_bs = $params->get('nav_right_bs','navright-bs');
	$dots = $params->get('dots');
	$autoplay = $params->get('autoplay');
	$autoplay_timeout = $params->get('autoplay_timeout','5000');
	$autoplay_speed = $params->get('autoplay_speed','1000');
	$mouse_wheel = $params->get('mouse_wheel');
	$name_color_default=$params->get('name_color_default','#ff0000');
	$sd_color_default=$params->get('sd_color_default','#ffffff');
	$buy_color_default=$params->get('buy_color_default','#ff0000');
	$price_color_default=$params->get('price_color_default','#444444');
	$name_color_wi=$params->get('name_color_wi','#ff0000');
	$sd_color_wi=$params->get('sd_color_wi','#646464');
	$buy_color_wi=$params->get('buy_color_wi','#646464');
	$price_color_wi=$params->get('price_color_wi','#444444');
	$name_color_mosaic=$params->get('name_color_mosaic','#ff0000');
	$sd_color_mosaic=$params->get('sd_color_mosaic','#444444');
	$buy_color_mosaic=$params->get('buy_color_mosaic','#ff0000');
	$price_color_mosaic=$params->get('price_color_mosaic','#444444');
	$name_color_fleft=$params->get('name_color_fleft','#ff0000');
	$sd_color_fleft=$params->get('sd_color_fleft','#444444');
	$buy_color_fleft=$params->get('buy_color_fleft','#ff0000');
	$price_color_fleft=$params->get('price_color_fleft','#444444');
	$max_width_default = $params->get('max_width_default',"");
	$max_height_default = $params->get('max_height_default',"");
	$margin_fleft = $params->get('margin_fleft',10);
	$padding_fleft = $params->get('padding_fleft',20);
	$margin_top_bottom = $params->get('margin_top_bottom',10);
	$margin_left_right = $params->get('margin_left_right',10);
	$count_block = $params->get('count_block',3);
	$one_col = $params->get('one_col',400);
	$two_col = $params->get('two_col',600);
	$one_col_mosaic = $params->get('one_col_mosaic',400);
	$two_col_mosaic = $params->get('two_col_mosaic',600);
	$count_block_mosaic = $params->get('count_block_mosaic',3);
	$minheight_block = $params->get('minheight_block',250);
	$minwidth_block = $params->get('minwidth_block',0);
	$animation_fadeto = $params->get('animation_fadeto');
	$animation_fadeto_mosaic = $params->get('animation_fadeto_mosaic');
	$bg_color = $params->get('bg_color', '#ffffff');
	$bg_color_mosaic = $params->get('bg_color_mosaic', '#ffffff');
	$medium_size = $params->get('medium_size',5);
	$large_size = $params->get('large_size',0);
	$border_color = $params->get('border_color', '#eeeeee');
	$border_color_mosaic = $params->get('border_color_mosaic', '#eeeeee');
	$large_first = $params->get('large_first');
	$autoheight = $params->get('autoheight');
	$hits_show=$params->get('hits_show',1);
	$hits_text=$params->get('hits_text');
	$bs_show=$params->get('bs_show',1);
	$bs_text=$params->get('bs_text');
	$new_show=$params->get('new_show',1);
	$new_text=$params->get('new_text');
	$label_show=$params->get('label_show',1);
	$label_text=$params->get('label_text');
	$customid_show=$params->get('customid_show',1);
	$customid_text=$params->get('customid_text');
	$u_image = $params->get('u_image');
	$vertical_count = $params->get('vertical_count',3);
	$add_link = $params->get('add_link','');
	$add_link_title = $params->get('add_link_title','');
	$modlayout=json_decode($module->params)->{"layout"};
	$create_file=$params->get('create_file',0);
	if ($nav=="1") {$nav_opt="true";} else {$nav_opt="false";}
	if ($dots=="1") {$dots_opt="true";} else {$dots_opt="false";}
	if ($autoplay=="1") {$autoplay_opt="true";} else {$autoplay_opt="false";}
	if (!$view_qtyminus) {$qtyminus="-";} else {$qtyminus='<i class="'.$view_qtyminus.'"></i>';}
	if (!$view_qtyplus) {$qtyplus="+";} else {$qtyplus='<i class="'.$view_qtyplus.'"></i>';}
	
	$jinput = JFactory::getApplication()->input;
	$category_id = $jinput->getInt('category_id');//JRequest::getInt('category_id');
	$product_id = $jinput->getInt('product_id');
	
	if ($catids_auto=="1"){
		$cat_arr[] = intval($category_id);
	}
	if ($catids_auto=="1" && $category_id){
		$cat_arr[] = intval($category_id);
	} else {
		if (is_array($cat_str)) {    
			$cat_arr = array();
			foreach($cat_str as $key=>$curr){
			if (intval($curr)) {
				$cat_arr[$key] = intval($curr);
			}
		}  
		} else {
			$cat_arr = array();
			if (intval($cat_str)) {
				$cat_arr[] = intval($cat_str);
			}
		}
	}
	$count_prod = modJshopping_dop_productsHelper::getDopProducts($params->get('count_products', 2), $cat_arr,"","","","");
	if ($loop=="1" && count($count_prod)>1){
		$loop_opt="true";
	} else {
		$loop_opt="false";
	}
	
	if ($product_select=="1"){
		$dop_prod = modJshopping_dop_productsHelper::getDopProducts($params->get('count_products', 5), $cat_arr, "", "",$order_query="ORDER BY $order_by",$sort_by); 
	} 
	elseif ($product_select=="2"){
		if ($custom_orderby=="1"){ 
			$dop_prod = modJshopping_dop_productsHelper::getDopProducts($params->get('count_products', 5), "", "", "AND prod.product_id in ($productid)",$order_query="ORDER BY FIELD (prod.product_id, $productid)",$sort_by);
		} else {
			$dop_prod = modJshopping_dop_productsHelper::getDopProducts($params->get('count_products', 5), "", "", "AND prod.product_id in ($productid)",$order_query="ORDER BY $order_by",$sort_by);
		}
	} 
	elseif ($product_select=="3"){
		///$product = JTable::getInstance('product', 'jshop');
		$dop_prod = $products->getProductLabel($label_str, $params->get('count_products', 5), $cat_arr, $filters=array(), $order_query="ORDER BY $order_by");
	} 
	elseif ($product_select=="4"){
		//$product = JTable::getInstance('product', 'jshop');
		$dop_prod = $products->getLastProducts($params->get('count_products', 5), $cat_arr);   
	}
	elseif ($product_select=="5"){
		$dop_man = modJshopping_dop_productsHelper::getDopManufacturers($order_by, $sort_by);
	}
	elseif ($product_select=="6"){
		if (is_array($manuf_prod)) {   
			foreach($manuf_prod as $key=>$curr){
				if (intval($curr)) $filters['manufacturers'][$key] = intval($curr);
			}  
		} else {
			if (intval($manuf_prod)) $filters['manufacturers'][] = intval($manuf_prod);
		}
		$dop_prod = modJshopping_dop_productsHelper::getDopProducts($params->get('count_products', 5), $cat_arr, $filters, "",$order_query="ORDER BY $order_by",$sort_by); 
	} 
	elseif ($product_select=="7"){
		$category = JTable::getInstance('category', 'jshop');
		$dop_cat = modJshopping_dop_productsHelper::getCatsArray($order_by, $sort_by, $category_id, $category);
	}
	elseif ($product_select=="8"){
		//$product = JTable::getInstance('product', 'jshop');
		$dop_prod = $products->getRandProducts($params->get('count_products', 5), $cat_arr, $filters);
	}
	elseif ($product_select=="9"){
		//$product = JTable::getInstance('product', 'jshop');
		$dop_prod = $products->getTopHitsProducts($params->get('count_products', 5), $cat_arr, $filters);
	}
	elseif ($product_select=="10"){
		//$product = JTable::getInstance('product', 'jshop');
		$dop_prod = $products->getBestSellers($params->get('count_products', 5), $cat_arr, $filters);
	}
	elseif ($product_select=="11"){
		//$product = JTable::getInstance('product', 'jshop');
		$dop_prod_hits = $products->getTopHitsProducts($params->get('count_products', 5), $cat_arr, $filters);
		if ($bs_show){
			$dop_prod_bs = $products->getBestSellers($params->get('count_products', 5), $cat_arr, $filters);
		}
		if ($new_show){
			$dop_prod_last = $products->getLastProducts($params->get('count_products', 5), $cat_arr);
		}
		if ($label_show){
			$dop_prod_label = $products->getProductLabel($label_str, $params->get('count_products', 5), $cat_arr, $filters=array(), $order_query="ORDER BY $order_by");
		}
		if ($customid_show){
			$dop_prod = modJshopping_dop_productsHelper::getDopProducts($params->get('count_products', 5), "", "", "AND prod.product_id in ($productid)",$order_query="ORDER BY $order_by",$sort_by); 
		}
	}
	elseif ($product_select=="12"){
		$session=JFactory::getSession();
		//$session->clear('last_visited_products');
		$last_visited_products = array_reverse($session->get('last_visited_products', array()));
		if (count($last_visited_products)) {
			$curent_product_id = JFactory::getApplication()->input->getInt('product_id');
			$productids=array();
			foreach ($last_visited_products as $product){
				//if ($product->product_id == $curent_product_id) {
				//	continue;
				//}
				if (count($productids) < $params->get('count_products', 5)) {
					$productids[] = $product->product_id;
				} else {
					break;
				}
			}
			if (count($productids)) {
				$productids = join( ', ', $productids);
				$dop_prod = modJshopping_dop_productsHelper::getDopProducts($params->get('count_products', 5), "", "", "AND prod.product_id in ($productids)",$order_query="ORDER BY $order_by",$sort_by);
			} else {
				//$product = JTable::getInstance('product', 'jshop');
				//$dop_prod = $product->getTopHitsProducts(3, $cat_arr, $filters);
				$layout='empty';
			}
		} else {
			//$product = JTable::getInstance('product', 'jshop');
			//$dop_prod = $product->getTopHitsProducts(3, $cat_arr, $filters);
			$layout='empty';
		}
	} elseif ($product_select=="13"){
		$dop_prod = modJshopping_dop_productsHelper::getDopOldPriceProducts($params->get('count_products', 5), $cat_arr, "", "",$order_query="ORDER BY $order_by",$sort_by); 
	} elseif ($product_select=="14") {
		$dop_prod = modJshopping_dop_productsHelper::getDopRelatedProducts((int)$product_id);
	}
	if ($product_select=="1" || $product_select=="2" || $product_select=="3" || $product_select=="4" || $product_select=="6" || $product_select=="8" || $product_select=="9" || $product_select=="10" || $product_select=="11" || ($product_select=="12" && $dop_prod) || $product_select=="13" || $product_select=="14"){	
	  foreach($dop_prod as $key=>$value){
	  	$dop_prod[$key]->product_link = SEFLink('index.php?option=com_jshopping&controller=product&task=view&category_id='.$value->category_id.'&product_id='.$value->product_id, 1); 
	  }
	}
	if ($font_awesome=="1"){
		$document->addStyleSheet(JURI::base().'modules/mod_jshopping_dop_products/assets/css/font-awesome.min.css');
	}
	if ($css_off!="1"){
		$document->addStyleSheet(JURI::base().'modules/mod_jshopping_dop_products/assets/css/default.css');	
	}
	if ($show_attr=="1"){
		if (version_compare(JVERSION, '3.0', 'ge')){JHtml::_('jquery.framework');}
		$document->addScript(JURI::base().'plugins/jshoppingproducts/attr_for_list_product/attr_for_list.js','text/javascript', true, false);
	}
	
		
	if ($modlayout=="_:default" || $modlayout=="_:defaultblock" || $modlayout=="_:defaultcategory"){
		$line_style='.modid_'.$moduleid.'.dop_products.default .modopprod_item_name a{color:'.$name_color_default.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.default .modopprod_item_sd{color:'.$sd_color_default.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.default .modopprod_item_buylink a{color:'.$buy_color_default.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.default .modopprod_item_price{color:'.$price_color_default.'}';
		$line_style.='.modid_'.$moduleid.'.scrollbar-inner .scroll-element .scroll-bar, .modid_'.$moduleid.'.scrollbar-rail .scroll-element .scroll-bar {background-color:'.$name_color_default.'}';
		$document->addStyleDeclaration(''.$line_style.'');
	}
	if ($modlayout=="_:whiteitem" || $modlayout=="_:whiteitemblock" || $modlayout=="_:whiteitemcategory"){
		$line_style='.modid_'.$moduleid.'.dop_products.default.whiteitem .modopprod_item_name a{color:'.$name_color_wi.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.default.whiteitem .modopprod_item_sd{color:'.$sd_color_wi.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.default.whiteitem .modopprod_item_buylink a{color:'.$buy_color_wi.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.default.whiteitem .modopprod_item_price{color:'.$price_color_wi.'}';
		$line_style.='.modid_'.$moduleid.'.scrollbar-inner .scroll-element .scroll-bar, .modid_'.$moduleid.'.scrollbar-rail .scroll-element .scroll-bar {background-color:'.$name_color_wi.'}';
		$document->addStyleDeclaration(''.$line_style.'');
	}
	if ($modlayout=="_:vertical"){
		$add_class_v="thumbnail pull-left clearfix";
		$line_style='.modid_'.$moduleid.'.dop_products.vertical .modopprod_item_name a{color:'.$name_color_wi.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.vertical .modopprod_item_sd{color:'.$sd_color_wi.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.vertical .modopprod_item_buylink a{color:'.$buy_color_wi.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.vertical .modopprod_item_price{color:'.$price_color_wi.'}';
		$document->addStyleDeclaration(''.$line_style.'');
	}
		if ($modlayout=="_:mosaic"){
		$line_style='.modid_'.$moduleid.'.dop_products.mosaic-block .modopprod_item_name a{color:'.$name_color_mosaic.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.mosaic-block .modopprod_item_sd{color:'.$sd_color_mosaic.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.mosaic-block .modopprod_item_buylink a{color:'.$buy_color_mosaic.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.mosaic-block .modopprod_item_price{color:'.$price_color_mosaic.'}';
		$document->addStyleDeclaration(''.$line_style.'');
	}
	if ($modlayout=="_:fleft"){
		$line_style='.modid_'.$moduleid.'.dop_products.fleft .modopprod_item_name a{color:'.$name_color_fleft.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.fleft .modopprod_item_sd{color:'.$sd_color_fleft.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.fleft .modopprod_item_buylink a{color:'.$buy_color_fleft.'}';
		$line_style.='.modid_'.$moduleid.'.dop_products.fleft .modopprod_item_price{color:'.$price_color_fleft.'}';
		$document->addStyleDeclaration(''.$line_style.'');
	}
	if (($modlayout=="_:default" || $modlayout=="_:defaultcategory" || $modlayout=="_:defaultblock" || $modlayout=="_:customimage" || $modlayout=="_:whiteitem" || $modlayout=="_:whiteitemblock" || $modlayout=="_:whiteitemcategory" || $modlayout=="_:vertical") && $owl_off!="1"){
		if (version_compare(JVERSION, '3.0', 'ge')){JHtml::_('jquery.framework');}
		$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/owl.carousel.js','text/javascript', true, false);
	}
	if ($modlayout=="_:customimage"){
		$ll="false";
	} else {
		$ll="true";
	}
	if ($modlayout=="_:default" || $modlayout=="_:defaultcategory" || $modlayout=="_:customimage" || $modlayout=="_:whiteitem" || $modlayout=="_:whiteitemcategory" || $modlayout=="_:vertical" || $modlayout=="_:manufacturer"){
		if (!empty($max_width_default) && !empty($max_height_default)){
			$line_style_mwh='.modid_'.$moduleid.' .owl-carousel .owl-item img{max-width:'.$max_width_default.'px; max-height:'.$max_height_default.'px; margin:auto;}';
			$document->addStyleDeclaration(''.$line_style_mwh.'');
		}
		if ($product_select!="14"){
			if ($create_file!="1"){
				$owl_script=modJshopping_dop_productsHelper::owlScript($moduleid, $resp_desctop, $resp_tablet, $resp_mobile, $nav_opt, $loop_opt, $dots_opt, $autoplay_opt, $autoplay_timeout, $autoplay_speed, $ll, $nav_left, $nav_right);
				$document->addScriptDeclaration($owl_script);
			} else {
				$f_name="js_owl_".$moduleid.".js";
			  	$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/createfile/'.$f_name.'','text/javascript', true, false);
			}
		} elseif ($product_select=="14"){
			if ($create_file!="1"){
				$related_script=modJshopping_dop_productsHelper::relatedScript($moduleid, $resp_desctop, $resp_tablet, $resp_mobile, $nav_opt, $loop_opt, $dots_opt, $autoplay_opt, $autoplay_timeout, $autoplay_speed, $ll, $nav_left, $nav_right);
				$document->addScriptDeclaration($related_script);
			} else {
				$f_name="js_related_".$moduleid.".js";
			  	$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/createfile/'.$f_name.'','text/javascript', true, false);
			}
		}
	if (version_compare(JVERSION, '3.0', 'ge')){JHtml::_('jquery.framework');}
	$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/owlcarousel.mw.js','text/javascript', true, false);	
	}
	
	if ($modlayout=="_:whiteitemblock" || $modlayout=="_:defaultblock"){
		if (!empty($max_width_default) && !empty($max_height_default)){
			$line_style_mwh='.modid_'.$moduleid.' .owl-carousel .owl-item img{max-width:'.$max_width_default.'px; max-height:'.$max_height_default.'px; margin:auto;}';
			$document->addStyleDeclaration(''.$line_style_mwh.'');
		}
		if ($create_file!="1"){
			$owlblock_script=modJshopping_dop_productsHelper::owlblockScript($moduleid, $resp_desctop, $resp_tablet, $resp_mobile, $nav_opt, $loop_opt, $dots_opt, $autoplay_opt, $autoplay_timeout, $autoplay_speed, $ll, $nav_left, $nav_right);
			$document->addScriptDeclaration($owlblock_script);
		} else {
			$f_name="js_owlblock_".$moduleid.".js";
			$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/createfile/'.$f_name.'','text/javascript', true, false);
		}
		if (version_compare(JVERSION, '3.0', 'ge')){JHtml::_('jquery.framework');}
		$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/owlcarousel.mw.js','text/javascript', true, false);	
	}
	
	if ($modlayout=="_:bscarousel"){
		if ($carousel_img_height) {$inline_style='style="max-height:'.$carousel_img_height.'px"';}
		if (version_compare(JVERSION, '3.0', 'ge')){JHtml::_('jquery.framework');}
		$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/bscarousel.js','text/javascript', true, false);
	}
	
	if ($modlayout=="_:fleft" || $modlayout=="_:fleftcategory"){
		if ($autoheight=="1") {
			$add_class="autoheight ";
		}
		$line_style='#wrap_'.$moduleid.'.fleft .modopprod_item{position:relative;display:block;float:left;height:100%;text-align: center;background:'.$bg_color.';margin:'.$margin_fleft.'px;padding:'.$padding_fleft.'px;min-height:'.$minheight_block.'px; border:1px solid '.$border_color.';}#wrap_'.$moduleid.'.fleft .modopprod_item:hover{box-shadow:0 0 3px '.$border_color.';-moz-box-shadow:0 0 3px '.$border_color.';-webkit-box-shadow:0 0 3px '.$border_color.'; z-index:99;}#wrap_'.$moduleid.'.fleft.fade-to{opacity:0;}';
		$document->addStyleDeclaration(''.$line_style.'');			
		if ($create_file!="1"){
			$fleft_script=modJshopping_dop_productsHelper::fleftScript($moduleid, $one_col, $two_col, $count_block, $minwidth_block, $margin_fleft, $padding_fleft);
			$document->addScriptDeclaration($fleft_script);
		} else {
			$f_name="js_fleft_".$moduleid.".js";
		  	$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/createfile/'.$f_name.'','text/javascript', true, false);
		}
		
	}
	
	if ($modlayout=="_:fleftcategorymenu"){
		$line_style='#wrap_'.$moduleid.'.category_menu.fleft .modopprod_item{height:100%;margin:'.$margin_fleft.'px;padding:'.$padding_fleft.'px;}#wrap_'.$moduleid.'.fleft .modopprod_item:hover{z-index:99;}#wrap_'.$moduleid.'.fleft.fade-to{opacity:0;}';
		$document->addStyleDeclaration(''.$line_style.'');	
		$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/extramenu.js','text/javascript', true, false);
		if ($create_file!="1"){
			$fleft_script=modJshopping_dop_productsHelper::fleftcategorymenuScript($moduleid, $one_col, $two_col, $count_block, $minwidth_block, $margin_fleft, $padding_fleft);
			$document->addScriptDeclaration($fleft_script);
		} else {
			$f_name="js_fleftcategorymenu_".$moduleid.".js";
		  	$document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/createfile/'.$f_name.'','text/javascript', true, false);
		}
	}
	
	if ($modlayout=="_:mosaic" || $modlayout=="_:mosaiccategory"){
		 $add_class="mosaic-item";
		 $line_style='#wrap_'.$moduleid.'.mosaic-block .mosaic-item{background:'.$bg_color_mosaic.'; padding:20px; border:1px solid '.$border_color_mosaic.'; text-align:center;-webkit-transition: top 1s ease, left 1s ease;	-moz-transition: top 1s ease, left 1s ease;	-o-transition: top 1s ease, left 1s ease;	-ms-transition: top 1s ease, left 1s ease;}#wrap_'.$moduleid.'.mosaic-block .mosaic-item:hover{box-shadow:0 0 3px '.$border_color_mosaic.';-moz-box-shadow:0 0 3px '.$border_color_mosaic.';-webkit-box-shadow:0 0 3px '.$border_color_mosaic.'; z-index:99;}#wrap_'.$moduleid.'.mosaic-block.fade-to .mosaic{opacity:0;}';
		$document->addStyleDeclaration(''.$line_style.'');	
	   if (version_compare(JVERSION, '3.0', 'ge')){JHtml::_('jquery.framework');}
		  $document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/blocksit.min.js','text/javascript', true, false);
		  if ($show_childcat=="1") {
		  $document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/childcat.js','text/javascript', true, false);
		  }
		  if ($create_file!="1"){
			  $mosaic_script=modJshopping_dop_productsHelper::mosaicScript($moduleid, $medium_size, $large_size, $one_col_mosaic, $two_col_mosaic, $count_block_mosaic, $margin_left_right, $margin_top_bottom);
			  $document->addScriptDeclaration($mosaic_script);
		  } else {
			  $f_name="js_mosaic_".$moduleid.".js";
			  $document->addScript(JURI::base().'modules/mod_jshopping_dop_products/assets/js/createfile/'.$f_name.'','text/javascript', true, false);
		  }
	}
	require(JModuleHelper::getLayoutPath('mod_jshopping_dop_products',$layout));
?>