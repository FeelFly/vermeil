<?php defined( '_JEXEC' ) or die;
class JFormFieldCreatefile extends JFormField{
	//protected $type = 'Createfile';
	
    public function getInput(){
		jimport('joomla.application.component.model');
		jimport( 'joomla.filesystem.file' );
		require_once (JPATH_SITE.'/components/com_jshopping/lib/factory.php'); 
		require_once (JPATH_SITE.'/components/com_jshopping/lib/functions.php');
		JSFactory::loadCssFiles();
		JSFactory::loadLanguageFile();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$formData=$this->form->getData();
		$jsd=json_decode($formData);
		$create_file=$jsd->params->create_file;
		$modlayout=$jsd->params->layout;
		
		if ($app->isAdmin() && $user->get('isRoot') && $create_file=="1"){
			require_once (JPATH_SITE.'/modules/mod_jshopping_dop_products/helper.php');
			$moduleid=$jsd->id;
			$jshopConfig=JSFactory::getConfig();	
			$product_select=$jsd->params->product_select;		
			$medium_size=$jsd->params->medium_size;
			$large_size=$jsd->params->large_size;
			$one_col_mosaic=$jsd->params->one_col_mosaic;
			$two_col_mosaic=$jsd->params->two_col_mosaic;
			$count_block_mosaic=$jsd->params->count_block_mosaic;
			$margin_left_right=$jsd->params->margin_left_right;
			$margin_top_bottom=$jsd->params->margin_top_bottom;
			
			$resp_desctop = $jsd->params->resp_desctop;
			$resp_tablet = $jsd->params->resp_tablet;
			$resp_mobile = $jsd->params->resp_mobile;
			$loop = $jsd->params->loop;
			$nav = $jsd->params->nav;
			$nav_position = $jsd->params->nav_position;
			$nav_left = $jsd->params->nav_left;
			$nav_right = $jsd->params->nav_right;
			$dots = $jsd->params->dots;
			$autoplay = $jsd->params->autoplay;
			$autoplay_timeout = $jsd->params->autoplay_timeout;
			$autoplay_speed = $jsd->params->autoplay_speed;
			
			$one_col = $jsd->params->one_col;
			$two_col = $jsd->params->two_col;
			$count_block = $jsd->params->count_block;
			$margin_fleft = $jsd->params->margin_fleft;
			$padding_fleft = $jsd->params->padding_fleft;
			$minwidth_block = $jsd->params->minwidth_block;
			
			if ($nav=="1") {$nav_opt="true";} else {$nav_opt="false";}
			if ($dots=="1") {$dots_opt="true";} else {$dots_opt="false";}
			if ($autoplay=="1") {$autoplay_opt="true";} else {$autoplay_opt="false";}
			if ($modlayout=="_:customimage"){
				$ll="false";
			} else {
				$ll="true";
			}
			
			if ($loop=="1"){
				$loop_opt="true";
			} else {
				$loop_opt="false";
			}
			
			//Mosaic
			if ($modlayout=="_:mosaic" || $modlayout=="_:mosaiccategory"){
			$f_name="js_mosaic_".$moduleid.".js";
			$file=dirname(__FILE__) . '/../js/createfile/'.$f_name.'';
			$mosaic_script=modJshopping_dop_productsHelper::mosaicScript($moduleid, $medium_size, $large_size, $one_col_mosaic, $two_col_mosaic, $count_block_mosaic, $margin_left_right, $margin_top_bottom);
			JFile::write($file, $mosaic_script); 
			}
			
			//OWL & Related
			if ($modlayout=="_:default" || $modlayout=="_:defaultcategory" || $modlayout=="_:customimage" || $modlayout=="_:whiteitem" || $modlayout=="_:whiteitemcategory" || $modlayout=="_:vertical" || $modlayout=="_:manufacturer") {
				if ($product_select!="14"){
					$f_name="js_owl_".$moduleid.".js";
					$file=dirname(__FILE__) . '/../js/createfile/'.$f_name.'';
					$owl_script=modJshopping_dop_productsHelper::owlScript($moduleid, $resp_desctop, $resp_tablet, $resp_mobile, $nav_opt, $loop_opt, $dots_opt, $autoplay_opt, $autoplay_timeout, $autoplay_speed, $ll, $nav_left, $nav_right);
					JFile::write($file, $owl_script);
				}
				elseif ($product_select=="14"){
					$f_name="js_related_".$moduleid.".js";
					$file=dirname(__FILE__) . '/../js/createfile/'.$f_name.'';
					$related_script=modJshopping_dop_productsHelper::relatedScript($moduleid, $resp_desctop, $resp_tablet, $resp_mobile, $nav_opt, $loop_opt, $dots_opt, $autoplay_opt, $autoplay_timeout, $autoplay_speed, $ll, $nav_left, $nav_right);
					JFile::write($file, $related_script);
				}
			}
			
			//OWL Block
			if ($modlayout=="_:whiteitemblock" || $modlayout=="_:defaultblock"){
				$f_name="js_owlblock_".$moduleid.".js";
				$file=dirname(__FILE__) . '/../js/createfile/'.$f_name.'';
				$owlblock_script=modJshopping_dop_productsHelper::owlblockScript($moduleid, $resp_desctop, $resp_tablet, $resp_mobile, $nav_opt, $loop_opt, $dots_opt, $autoplay_opt, $autoplay_timeout, $autoplay_speed, $ll, $nav_left, $nav_right);
				JFile::write($file, $owlblock_script);
			}
			
			//Fleft
			if ($modlayout=="_:fleft" || $modlayout=="_:fleftcategory"){
				$f_name="js_fleft_".$moduleid.".js";
				$file=dirname(__FILE__) . '/../js/createfile/'.$f_name.'';
				$fleft_script=modJshopping_dop_productsHelper::fleftScript($moduleid, $one_col, $two_col, $count_block, $minwidth_block, $margin_fleft, $padding_fleft);
				JFile::write($file, $fleft_script);
			}
			if ($modlayout=="_:fleftcategorymenu"){
				$f_name="js_fleftcategorymenu_".$moduleid.".js";
				$file=dirname(__FILE__) . '/../js/createfile/'.$f_name.'';
				$fleftcategorymenu_script=modJshopping_dop_productsHelper::fleftcategorymenuScript($moduleid, $one_col, $two_col, $count_block, $minwidth_block, $margin_fleft, $padding_fleft);
				JFile::write($file, $fleftcategorymenu_script);
			}
			
		}
	}
}