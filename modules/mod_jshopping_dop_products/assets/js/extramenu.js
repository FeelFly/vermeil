jQuery(function(){ 
"use strict";
	//Clone catalog
	if (jQuery(".dop_products.extra").length){
		jQuery(".js-show-catalog").on("mouseover",function(){
			jQuery(".js-clone-show-catalog").html("");
			jQuery(".dop_products.extra").clone().appendTo(".js-clone-show-catalog");
		});
	}
	jQuery(".category_menu .js-open-subcategory i").click(function(){
		jQuery(this).parent("div").next("div").toggle();
	});
});