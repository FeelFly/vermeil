jQuery(function(){
"use strict";
	jQuery(".hidden-childcat .child-category").each(function(){
		var $this=jQuery(this);
		$this.appendTo($this.prev(".parent-category"));
	});
	
	jQuery(".parent-category").each(function(){
		var $this=jQuery(this),
		pn=$this.find(".singlecat .modopprod_item_name");
		$this.children(".child-category").wrapAll("<div class='child-categories' />");
		pn.clone().prependTo($this.children(".child-categories"));	
	});
	
});