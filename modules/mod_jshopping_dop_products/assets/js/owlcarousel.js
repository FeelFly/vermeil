jQuery(function(){
"use strict";
jQuery(".dop_products.default .modopprod_item").hover(function(){
jQuery(this).addClass("hovered");
jQuery(".hovered .modopprod_item_sd").stop(true,true).toggle("slow");
},function(){
jQuery(this).removeClass("hovered");
jQuery(".modopprod_item_sd").hide("slow");
});
});

(function(jQuery){
"use strict";
jQuery.fn.initOwlCarusel = function(options) {
var defaults = {	
   loop:false,
	video:true,
    margin:15,
    nav:true,
	navText:['<','>'],
	autoWidth:false,
	autoHeight:false,
	autoplay:true,
	autoplaySpeed:1000,
	autoplayTimeout:5000,
	center:false,
	stagePadding:0,
	dots:false,
	dotData:false,
	dotsEach:false,
	lazyLoad:true,
	autoplayHoverPause:true,
	//slideBy:'page',
	responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
};
	options = jQuery.extend(defaults, options);
	return this.each(function() {
	var this_id_prefix = "#"+this.id+" ";
	var owl = jQuery(this_id_prefix);
	owl.owlCarousel({
	loop: options.loop,
	video:options.video,
	margin:options.margin,
	nav:options.nav,
	navText:options.navText,
	autoWidth:options.autoWidth,
	autoHeight:options.autoHeight,
	autoplay:options.autoplay,
	autoplayTimeout:options.autoplayTimeout,
	autoplaySpeed:options.autoplaySpeed,
	center:options.center,
	stagePadding:options.stagePadding,
	dots:options.dots,
	dotData:options.dotData,
	dotsEach:options.dotsEach,
	lazyLoad:options.lazyLoad,
	autoplayHoverPause:options.autoplayHoverPause,
	//slideBy:'page',
	responsive:{
		0:{
			items:options.items_mobile
		},
		600:{
			items:options.items_tablet
		},
		1000:{
			items:options.items_desctop
		}
    }
});			
});
};
 
})(jQuery);