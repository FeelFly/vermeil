<?php defined('_JEXEC') or die('Restricted access');
class modJshopping_dop_productsHelper{	
   
 	public static function getDopProducts($count, $array_categories = null, $filters = array(), $productids, $order_query, $sort_by){
        $jshopConfig = JSFactory::getConfig();
        $db = JFactory::getDBO();
        $product = JTable::getInstance('product', 'jshop');
        $adv_query = ""; $adv_from = ""; $adv_result = $product->getBuildQueryListProductDefaultResult();
        $product->getBuildQueryListProductSimpleList("dop", $array_categories, $filters, $adv_query, $adv_from, $adv_result);
        $query = "SELECT $adv_result FROM `#__jshopping_products` AS prod
                  INNER JOIN `#__jshopping_products_to_categories` AS pr_cat ON pr_cat.product_id = prod.product_id
                  LEFT JOIN `#__jshopping_categories` AS cat ON pr_cat.category_id = cat.category_id
                  $adv_from
                  WHERE prod.product_publish = '1' AND cat.category_publish='1' $productids ".$adv_query."
                  GROUP BY prod.product_id $order_query $sort_by LIMIT ".$count;
        $db->setQuery($query);
        $products = $db->loadObjectList();
        $products = listProductUpdateData($products);
        return $products;							
	}
	public static function getDopOldPriceProducts($count, $array_categories = null, $filters = array(), $productids, $order_query, $sort_by){
        $jshopConfig = JSFactory::getConfig();
        $db = JFactory::getDBO();
        $product = JTable::getInstance('product', 'jshop');
        $adv_query = ""; $adv_from = ""; $adv_result = $product->getBuildQueryListProductDefaultResult();
        $product->getBuildQueryListProductSimpleList("dop", $array_categories, $filters, $adv_query, $adv_from, $adv_result);
        $query = "SELECT $adv_result FROM `#__jshopping_products` AS prod
                  INNER JOIN `#__jshopping_products_to_categories` AS pr_cat ON pr_cat.product_id = prod.product_id
                  LEFT JOIN `#__jshopping_categories` AS cat ON pr_cat.category_id = cat.category_id
                  $adv_from
                  WHERE prod.product_publish = '1' AND cat.category_publish='1' AND prod.product_old_price<>0 $productids ".$adv_query."
                  GROUP BY prod.product_id $order_query $sort_by LIMIT ".$count;
        $db->setQuery($query);
        $products = $db->loadObjectList();
        $products = listProductUpdateData($products);
        return $products;							
	}
	public static function getDopRelatedProducts($product_id){
        $jshopConfig = JSFactory::getConfig();
		$db = JFactory::getDBO();
		$product = JTable::getInstance('product', 'jshop');
        $adv_query = ""; $adv_from = ""; $adv_result = $product->getBuildQueryListProductDefaultResult();
        $filters = array();
        $product->getBuildQueryListProductSimpleList("dop_related", null, $filters, $adv_query, $adv_from, $adv_result);
        $order_query = "order by relation.id";
        $dispatcher = JDispatcher::getInstance();
        $query = "SELECT $adv_result FROM `#__jshopping_products_relations` AS relation
                INNER JOIN `#__jshopping_products` AS prod ON relation.product_related_id = prod.product_id
                LEFT JOIN `#__jshopping_products_to_categories` AS pr_cat ON pr_cat.product_id = relation.product_related_id
                LEFT JOIN `#__jshopping_categories` AS cat ON pr_cat.category_id = cat.category_id
                $adv_from
                WHERE relation.product_id=".(int)$product_id." AND cat.category_publish=1 AND prod.product_publish=1 ".$adv_query." 
				group by prod.product_id ".$order_query;
        $db->setQuery($query);
        $products = $db->loadObjectList();
        $products = listProductUpdateData($products);
        return $products;		
    }
		
	public static function getDopManufacturers($order_by, $sort_by){
		$manufacturer_id = JRequest::getInt('manufacturer_id');
		$manufacturer = JTable::getInstance('manufacturer', 'jshop');    
		$dop_man = $manufacturer->getAllManufacturers(1, $order_by, $sort_by);
		foreach ($dop_man as $key => $value){
	  	$dop_man[$key]->link = SEFLink('index.php?option=com_jshopping&controller=manufacturer&task=view&manufacturer_id='.$dop_man[$key]->manufacturer_id, 2);
	  	}
		return $dop_man; 
	}

	//** CATEGORY **//
	public static function getTreeCats($category, $order_by, $sort_by, $cat_active_id, $cat_parent_id = 0, $level = 1){		
		$rows  = $category->getSubCategories($cat_parent_id, $order_by, $sort_by, 1);
		$catarr = array();
		if(count($rows)){
			foreach($rows as $row) {
				$child = $category->getSubCategories($row->category_id, $order_by, $sort_by, 1);
				$catarr[] = array(
					'name' => $row->name, 
					'link' => $row->category_link, 
					'active' => in_array($row->category_id, $cat_active_id), 
					'publish' => $row->category_publish, 
					'parent' => count($child) ? true : false, 
					'level' => $level, 
					'img' => $row->category_image,
					'sd' => $row->short_description,
					'id' =>$row->category_id
					);	
				if(count($child)) {
					$level++;
					$catarr = array_merge($catarr, modJshopping_dop_productsHelper::getTreeCats($category, $order_by, $sort_by, $cat_active_id, $row->category_id, $level));
					$level--;
				}
			}
		}
		
		return $catarr;
    }
    
    public static function getCatsArray($order_by, $sort_by, $cat_active_id, $category){
	    $category->load($cat_active_id);
    	$categories_id = $category->getTreeParentCategories();
	   return modJshopping_dop_productsHelper::getTreeCats($category, $order_by, $sort_by, $categories_id);
    }
	//** END CATEGORY **//	
	
	public static function setCompareButtons($product, $link_class){
		$comparray=array();
		$comparray=$_SESSION['comparep'];
		$jshopConfig = JSFactory::getConfig();
		$rt='data-rel="tooltip"';
		if (isset($comparray) && in_array($product->product_id, $comparray)){
		$compare_toggle_dnone="compare_dnone";
		$compare_toggle_dblock="";
		} else {
		$compare_toggle_dnone="";
		$compare_toggle_dblock="compare_dnone";
		}
		$text_link_add_to_compare='<i class="fa fa-bar-chart"></i>';
		$text_link_go_to_compare='<i class="fa fa-bar-chart"></i>';
		$title_add = JText::_('PLG_ADD_TO_COMPARE');
		$title_go = JText::_('PLG_GO_TO_COMPARE');
		$title_del = JText::_('PLG_DEL_COMPARE');
		$seflink=SEFLink('index.php?option=com_jshopping&controller=compare&task=view', 1);
		$html .='<a '.$rt.' data-placement="top" data-original-title="'.$title_add.'" data-compare="name='.htmlspecialchars($product->name).'&link='.$product->product_link.'&price='.number_format(round($product->product_price,2),2).$jshopConfig->currency_code.'&image='.$product->image.'" data-id="comparelist_'.$product->product_id.'" class="btn list-btn compare_link_to_list '.$compare_toggle_dnone.'" href="#"><i class="'.$link_class.'"></i></a>';
		$html .='<a '.$rt.' data-placement="top" data-original-title="'.$title_go.'" data-id="gotocomparelist_'.$product->product_id.'" class="btn list-btn go_to_compre_list '.$compare_toggle_dblock.'" href="'.$seflink.'"><i class="'.$link_class.'"></i></a>';
		$html .='<a '.$rt.' data-placement="top" data-original-title="'.$title_del.'" data-id="removelistid_'.$product->product_id.'" class="btn list-btn remove_compare_list '.$compare_toggle_dblock.'" href="#" title="'.$title_del.'">x</a>';
		return $html;
	}
	
	public static function setWishlistButtons($product, $link_class){
		$rt='data-rel="tooltip"';
		$html .='<a '.$rt.' data-placement="top" title="'._JSHOP_ADD_TO_WISHLIST.'" data-productlink="'.$product->product_id.'" class="product-button-wishlist btn list-btn" href = "'.SEFLink('index.php?option=com_jshopping&controller=cart&task=add&to=wishlist&category_id='.$product->category_id.'&product_id='.$product->product_id, 1).'"><i class="'.$link_class.'"></i></a>';
		return $html;
	}
	
	public static function showAttributes($products, $product, $module, $modlayout=""){
		$fe="data-fe='1'";
		$html .='
		<div class="attrib attrforprodid_'.$product->product_id.'" data-uri-base="'.JURI::base().'" data-attrforprodid="'.$product->product_id.'" '.$fe.' data-text="'._JSHOP_PRODUCT_NOT_AVAILABLE_THIS_OPTION.'" data-textsel="'._JSHOP_SELECT.'" data-wrapclass=".modopprod_item">';
		$products->load($product->product_id);
		$attributesDatas = $products->getAttributesDatas($back_value["attr"]);
		$products->setAttributeActive($attributesDatas["attributeActive"]);
		$attributeValues=$attributesDatas["attributeValues"];
		$attributes=$products->getBuildSelectAttributes($attributeValues, $attributesDatas["attributeSelected"]);
		if (count($attributes)){
			$_attributevalue=JTable::getInstance("attributValue", "jshop");
			$all_attr_values = $_attributevalue->getAllAttributeValues();
		}else{
			$all_attr_values = array();
		}
		if (count($attributes)){
			$fe="data-fe='1'";
			$replace_radio = array(
				'name="jshop_attr_id' => 'name="mod_'.$module->id.'_'.$modlayout.'_pid'.$product->product_id.'-jshop_attr_id',
				'id="jshop_attr_id' => 'id="mod_'.$module->id.'_'.$modlayout.'_pid_'.$product->product_id.'_jshop_attr_id',
				'for="jshop_attr_id' => 'for="mod_'.$module->id.'_'.$modlayout.'_pid_'.$product->product_id.'_jshop_attr_id',
				'id="prod_attr_img_' => 'id="mod_'.$module->id.'_'.$modlayout.'_pid_'.$product->product_id.'_prod_attr_img_',
				'onclick="setAttrValue(' =>'onclick="notUseAttrValue(',
				'onchange="setAttrValue(' =>'onchange="notUseAttrValue('
			);
			$html .='
			<div class="jshop_prod_attributes">
			<div class="jshop">';
			foreach($attributes as $attribut){
				$html .=
				'<div class="att_none">
				<div class="attributes_title"><span class="attributes_name">'.$attribut->attr_name.':</span><span class="attributes_description">'.$attribut->attr_description.'</span></div>
				<div class="attributes_value"><span class="attr_arr" data-id="'.$attribut->attr_id.'" data-attrprefix="mod_'.$module->id.'_'.$modlayout.'_pid_'.$product->product_id.'">'.str_replace(array_keys($replace_radio), $replace_radio, $attribut->selects).'</span></div>
				</div><div class="clear"></div>';
			}
			$html .='
			</div>
			</div>';
		}
		$html .=
		'</div>';
		return $html;
	}
	
	public static function showQty($product, $buyLink, $minus="-", $plus="+", $module, $modlayout=""){
		$html .='
		<div class="input-append count_block">
			<button type="button" class="btn list-btn count p_m" 
			onclick = "
			var qty_el = document.getElementById(\'quantity'.$product->product_id.'-modid'.$module->id.'_'.$modlayout.'\');
			var qty = qty_el.value;
			if( !isNaN( qty ) && qty > 1) qty_el.value--;
			var url_el = document.getElementById(\'modid_'.$module->id.'_'.$modlayout.'_buy_item_'.$product->product_id.'\');
			url_el.href=\''.$buyLink.'&quantity=\'+qty_el.value;return false;">
			'.$minus.'
			</button>
			<input type = "text" name = "quantity'.$product->product_id.'-modid'.$module->id.'_'.$modlayout.'" id = "quantity'.$product->product_id.'-modid'.$module->id.'_'.$modlayout.'"
			class = "btn list-btn quantity inputbox" value = "1" onkeyup="
			var qty_el = document.getElementById(\'quantity'.$product->product_id.'-modid'.$module->id.'_'.$modlayout.'\');
			var url_el = document.getElementById(\'modid_'.$module->id.'_'.$modlayout.'_buy_item_'.$product->product_id.'\');
			url_el.href=\''.$buyLink.'&quantity=\'+qty_el.value;return false;" />
			<button type="button" class="btn list-btn count p_p" 
			onclick = "
			var qty_el = document.getElementById(\'quantity'.$product->product_id.'-modid'.$module->id.'_'.$modlayout.'\');
			var qty = qty_el.value;
			if( !isNaN( qty )) qty_el.value++;
			var url_el = document.getElementById(\'modid_'.$module->id.'_'.$modlayout.'_buy_item_'.$product->product_id.'\');
			url_el.href=\''.$buyLink.'&quantity=\'+qty_el.value;return false;">
			'.$plus.'
			</button>
		</div>
		';
		return $html;
	}
	public static function mosaicScript($moduleid, $medium_size, $large_size, $one_col_mosaic, $two_col_mosaic, $count_block_mosaic, $margin_left_right, $margin_top_bottom){
		$mosaic_script='
			jQuery(function(){
			var medium=['.$medium_size.'],
			large=['.$large_size.'],
			path=jQuery(".mosaic-item");
			for (var i=0;i<path.length;i++) path.eq(medium[i]).attr("data-size","2");
			for (var i=0;i<path.length;i++) path.eq(large[i]).attr("data-size","3");
			}); 
			jQuery(window).load(function(){
				  var winWidth = jQuery(".mosaic-block").width();
				  if(winWidth < '.$one_col_mosaic.'){
					  col=1;
				  } else if (winWidth < '.$two_col_mosaic.'){
					  col=2;
				  } else {
					  col='.$count_block_mosaic.'
				  } 
				  jQuery("#wrap_'.$moduleid.'.mosaic-block.fade-to  .mosaic").fadeTo(1500,1);
				  jQuery("#wrap_'.$moduleid.' .mosaic").BlocksIt({numOfCol: col, offsetX: '.$margin_left_right.', offsetY: '.$margin_top_bottom.', blockElement: ".mosaic-item"});
				  jQuery("#rpBrooksTab a[href^=\'#related\']").click(function(){
				  setTimeout(function(){
					  var winWidth = jQuery(".mosaic-block").width();
						  if(winWidth < '.$one_col_mosaic.'){
					  col=1;
					  } else if (winWidth < '.$two_col_mosaic.'){
						  col=2;
					  } else {
						  col='.$count_block_mosaic.'
					  } 
					  jQuery("#wrap_'.$moduleid.' .mosaic").BlocksIt({numOfCol: col, offsetX: '.$margin_left_right.', offsetY: '.$margin_top_bottom.', blockElement: ".mosaic-item"})
				  },10);
				  });
				  var currentWidth = jQuery(".mosaic-block").width();
				  jQuery(window).resize(function(){
					  var winWidth = jQuery(".mosaic-block").width(); 
					  var conWidth; 
					  if(winWidth < 660){
						  conWidth = winWidth;
						  col = 1
					  } else if(winWidth < 880){
						  conWidth = winWidth; 
						  col = 3
					  } else if(winWidth < 945){
						  conWidth = winWidth;
						  col = 4;
					  } else {
						  conWidth = winWidth;
						  col = 5;
					  } if(conWidth != currentWidth) {
						  currentWidth = conWidth; 
						  jQuery(".mosaic").width(conWidth); 
						  jQuery("#wrap_'.$moduleid.' .mosaic").BlocksIt({numOfCol: col, offsetX: '.$margin_left_right.', offsetY: '.$margin_top_bottom.', blockElement: ".mosaic-item"});
					  } 
				  });	
			  });
		';
		return preg_replace('/\s\s+/', ' ',trim($mosaic_script));
	}
	public static function owlScript($moduleid, $resp_desctop, $resp_tablet, $resp_mobile, $nav_opt, $loop_opt, $dots_opt, $autoplay_opt, $autoplay_timeout, $autoplay_speed, $ll, $nav_left, $nav_right){
		$owl_script='
			jQuery(function(){
				jQuery(".dop_products").fadeIn(1500);
				jQuery("#owl-carousel-id-'.$moduleid.'").initOwlCarusel({
					items_desctop:'.$resp_desctop.',
					items_tablet:'.$resp_tablet.',
					items_mobile:'.$resp_mobile.',
					nav:'.$nav_opt.',
					loop:'.$loop_opt.',
					dots:'.$dots_opt.',
					autoplay:'.$autoplay_opt.',
					autoplayTimeout:'.$autoplay_timeout.',
					autoplaySpeed:'.$autoplay_speed.',
					lazyLoad:'.$ll.',
					navText:[\'<i class="'.$nav_left.'"></i>\',\'<i class="'.$nav_right.'"></i>\']
				});
			});
		';
		return preg_replace('/\s\s+/', ' ',trim($owl_script));
	}
	public static function owlblockScript($moduleid, $resp_desctop, $resp_tablet, $resp_mobile, $nav_opt, $loop_opt, $dots_opt, $autoplay_opt, $autoplay_timeout, $autoplay_speed, $ll, $nav_left, $nav_right){
		$owlblock_script='
			function initOwlInTab(tabType){
				jQuery("#owl-carousel-id-'.$moduleid.'-"+tabType).initOwlCarusel({
					items_desctop:'.$resp_desctop.',
					items_tablet:'.$resp_tablet.',
					items_mobile:'.$resp_mobile.',
					nav:'.$nav_opt.',
					loop:'.$loop_opt.',
					dots:'.$dots_opt.',
					autoplay:'.$autoplay_opt.',
					autoplayTimeout:'.$autoplay_timeout.',
					autoplaySpeed:'.$autoplay_speed.',
					lazyLoad:'.$ll.',
					navText:[\'<i class="'.$nav_left.'"></i>\',\'<i class="'.$nav_right.'"></i>\']
				});
			}
			jQuery(function(){
				jQuery(".dop_products").fadeIn(1500);
				initOwlInTab("hits");
				jQuery("a.js-dp-tabs").on("shown.bs.tab", function (e) {
					initOwlInTab(jQuery(e.target).data("tabtype"));
				 });
			});
		';
		return preg_replace('/\s\s+/', ' ',trim($owlblock_script));
	}
	public static function fleftScript($moduleid, $one_col, $two_col, $count_block, $minwidth_block, $margin_fleft, $padding_fleft){
		$fleft_script='
			function fleftWidth(mid, onecol, twocol, countblock, minwidthblock, mfl, pfl){
				var awv=jQuery(window).outerWidth(), 
				wrv=jQuery("#wrap_"+mid).outerWidth(), 
				mb=mfl, 
				pb=pfl;
				if (wrv>awv) {
					var wv=awv-1;
				} else {
					var wv=wrv-1;
				} 
				if (minwidthblock && minwidthblock!==0) {
					var wv=wv*minwidthblock/100;
					jQuery("#wrap_"+mid).css("width",wv);
				} 
				if (wv<onecol){
					var col=1;
				} else if (wv<twocol){
					var col=2;
				}  else {
					var col=countblock;
				}
				var mw=wv/col;
				jQuery("#wrap_"+mid+".fleft .modopprod_item").outerWidth(mw-2*mb);
				if (wv>481){
					jQuery("#wrap_"+mid+".fleft.large-first .modopprod_item:first").outerWidth(wv-2*mb).addClass("large-block");
				}
			}
			jQuery.fn.equalizeHeightsDop = function() {
			var maxHeight = this.map(function( i, e ) {
			return jQuery( e ).height();
			}).get(); return this.height( Math.max.apply( this, maxHeight ) );};
			jQuery(function(){ 
				fleftWidth('.$moduleid.', '.$one_col.','.$two_col.', '.$count_block.', '.$minwidth_block.', '.$margin_fleft.', '.$padding_fleft.');		
			});
			jQuery(window).load(function(){
				jQuery("#wrap_'.$moduleid.'.fleft.fade-to").fadeTo(1500,1); 
				jQuery("#wrap_'.$moduleid.'.fleft .modopprod_item.autoheight").equalizeHeightsDop();
				jQuery("#rpBrooksTab a[href^=\'#related\']").click(function(){
					setTimeout(function(){
						fleftWidth('.$moduleid.', '.$one_col.','.$two_col.', '.$count_block.', '.$minwidth_block.', '.$margin_fleft.', '.$padding_fleft.')
					},10);
				});
			});
			jQuery(window).resize(function(){
				fleftWidth('.$moduleid.', '.$one_col.','.$two_col.', '.$count_block.', '.$minwidth_block.', '.$margin_fleft.', '.$padding_fleft.');
			});	
		';
		return preg_replace('/\s\s+/', ' ',trim($fleft_script));
	}
	public static function fleftcategorymenuScript($moduleid, $one_col, $two_col, $count_block, $minwidth_block, $margin_fleft, $padding_fleft){
		$fleftcategorymenu_script='
			function fleftWidth(mid, onecol, twocol, countblock, minwidthblock, mfl, pfl){
				var awv=jQuery(window).outerWidth(), 
				wrv=jQuery("#wrap_"+mid).outerWidth(), 
				mb=mfl, 
				pb=pfl;
				if (wrv>awv) {
					var wv=awv-1;
				} else {
					var wv=wrv-1;
				} 
				if (minwidthblock && minwidthblock!==0) {
					var wv=wv*minwidthblock/100;
					jQuery("#wrap_"+mid).css("width",wv);
				} 
				if (wv<onecol){
					var col=1;
				} else if (wv<twocol){
					var col=2;
				}  else {
					var col=countblock;
				}
				var mw=wv/col;
				jQuery("#wrap_"+mid+".fleft .modopprod_item").outerWidth(mw-2*mb-5);
				if (wv>481){
					jQuery("#wrap_"+mid+".fleft.large-first .modopprod_item:first").outerWidth(wv-2*mb).addClass("large-block");
				}
			}
		
			jQuery.fn.equalizeHeightsDop = function() {
			var maxHeight = this.map(function( i, e ) {
			return jQuery( e ).height();
			}).get(); return this.height( Math.max.apply( this, maxHeight ) );};
			jQuery(function(){ 
				fleftWidth('.$moduleid.', '.$one_col.','.$two_col.', '.$count_block.', '.$minwidth_block.', '.$margin_fleft.', '.$padding_fleft.');
			});
			jQuery(window).load(function(){
				jQuery("#wrap_'.$moduleid.'.fleft.fade-to").fadeTo(1500,1); 
				jQuery("#wrap_'.$moduleid.'.fleft .modopprod_item.autoheight").equalizeHeightsDop();
			});
			jQuery(window).resize(function(){
				fleftWidth('.$moduleid.', '.$one_col.','.$two_col.', '.$count_block.', '.$minwidth_block.', '.$margin_fleft.', '.$padding_fleft.');
			});	
		';
		return preg_replace('/\s\s+/', ' ',trim($fleftcategorymenu_script));
	}
	public static function relatedScript($moduleid, $resp_desctop, $resp_tablet, $resp_mobile, $nav_opt, $loop_opt, $dots_opt, $autoplay_opt, $autoplay_timeout, $autoplay_speed, $ll, $nav_left, $nav_right){
		$related_script='
			function initOwlInTabRelated(){
				jQuery("#owl-carousel-id-'.$moduleid.'").initOwlCarusel({
					items_desctop:'.$resp_desctop.',
					items_tablet:'.$resp_tablet.',
					items_mobile:'.$resp_mobile.',
					nav:'.$nav_opt.',
					loop:'.$loop_opt.',
					dots:'.$dots_opt.',
					autoplay:'.$autoplay_opt.',
					autoplayTimeout:'.$autoplay_timeout.',
					autoplaySpeed:'.$autoplay_speed.',
					lazyLoad:'.$ll.',
					navText:[\'<i class="'.$nav_left.'"></i>\',\'<i class="'.$nav_right.'"></i>\']
					});
			}
			jQuery(function(){
				jQuery(".dop_products").fadeIn(1500);
				if (jQuery(".tab-content #related, .rpBrooksAccordion #related").is(":visible") || !jQuery(".tab-content #related, .rpBrooksAccordion #related").length){
					jQuery(".dop_products").fadeIn(1500);
					initOwlInTabRelated();
				}
				jQuery("#rpBrooksTab a").on("shown.bs.tab", function (e) {
					if (jQuery(e.target).attr("href")=="#related"){
						initOwlInTabRelated();
					}
				 });
				 jQuery("#rpBrooksCollapse").on("shown.bs.collapse", function (e) {
					if (jQuery(e.target).attr("id")=="related"){
						initOwlInTabRelated();
					}
				 });
				 jQuery("#rpBrooksCollapse").on("shown", function (e) {
					if (jQuery(e.target).attr("id")=="related"){
						initOwlInTabRelated();
					}
				 });
			});
		';
		return preg_replace('/\s\s+/', ' ',trim($related_script));
	}
	
}