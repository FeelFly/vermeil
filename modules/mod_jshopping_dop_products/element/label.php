<?php
defined('_JEXEC') or die('Restricted access');

class JFormFieldLabel extends JFormField {

  public $type ='Label';
  
  protected function getInput(){
        require_once (JPATH_SITE.'/components/com_jshopping/lib/factory.php'); 
		require_once (JPATH_SITE.'/components/com_jshopping/lib/functions.php');
		$db = JFactory::getDBO();
		$jshopConfig = JSFactory::getConfig();
		$jshopConfig->cur_lang = $jshopConfig->frontend_lang;  
        $tmp = new stdClass();
        $tmp->id = "";
        $tmp->name = JText::_('JALL');
        $element_1  = array($tmp);
        $productLabel = JTable::getInstance('productLabel', 'jshop');
        $listLabels = $productLabel->getListLabels();
        $elementes_select =array_merge($element_1 , $listLabels); 
        $ctrl  =  $this->name ;  
        $value        = empty($this->value) ? '' : $this->value; 
        
        return JHTML::_('select.genericlist', $elementes_select, $ctrl,'class="inputbox"','id', 'name', $value );
  }
}
?>
