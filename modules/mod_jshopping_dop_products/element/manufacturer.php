<?php

defined('_JEXEC') or die('Restricted access');

class JFormFieldManufacturer extends JFormField {

  public $type ='Manufacturer';
  
  protected function getInput(){
         require_once (JPATH_SITE.'/components/com_jshopping/lib/factory.php'); 
		require_once (JPATH_SITE.'/components/com_jshopping/lib/functions.php');
		$db = JFactory::getDBO();
		$jshopConfig = JSFactory::getConfig();
		$jshopConfig->cur_lang = $jshopConfig->frontend_lang; 
        $tmp = new stdClass();
        $tmp->id = "";
        $tmp->name = JText::_('JALL');
        $element_m  = array($tmp);
        $manufacturer = JTable::getInstance('manufacturer', 'jshop');    
		$dop_man = $manufacturer->getAllManufacturers("", "", "ASC");
        $elementes_select =array_merge($element_m , $dop_man); 
        $ctrl  =  $this->name ;  
		$ctrl .= '[]';
        $value        = empty($this->value) ? '' : $this->value; 
        
        return JHTML::_('select.genericlist', $elementes_select, $ctrl,'class="inputbox" id = "manufacturers_ordering" multiple="multiple"','manufacturer_id', 'name', $value );
  }
}
?>
