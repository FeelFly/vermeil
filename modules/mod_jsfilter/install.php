<?php

// No direct access.
defined('_JEXEC') or die;
if ( !defined('DS') ) define('DS', DIRECTORY_SEPARATOR);


class mod_jsfilterInstallerScript
{
	private $_name = "jsfilter";


	function install(&$ex)
	{
		$lang	= JFactory::getLanguage();
		$inst	= JInstaller::getInstance();
		$desc	= (string) $inst->getManifest()->description;
		$lang->load('mod_'.$this->_name.'.sys', JPATH_ROOT, null, true);
		$inst->set('message', JText::_($desc));

		// Front-end
		$src	= dirname(__FILE__).DS.'component';
		$dest	= JPATH_ROOT.DS.'components'.DS.'com_jshopping';

		if ( !JFolder::exists($dest) ) return false;
		
		if ( JFolder::exists($src) ) {
			foreach ( JFolder::folders($src) as $f ) {
				JFolder::copy( $src.DS.$f, $dest.DS.$f, '', true );
			}
		}

		// Проверка наличия ionCube
		if ( !extension_loaded('ionCube Loader') )
		{
			JError::raiseNotice( '500', JText::_('MJSF_ZEND_NOT_LOADED') );
		}

		// Дополнительные операции при установке дополнения
		$this->customInstall();

		// Очистка кэша обновлений
        $jCfg = JFactory::getConfig();
		$options = array(
			'defaultgroup' => $this->_name,
			'cachebase' => $jCfg->get('cache_path', JPATH_ADMINISTRATOR.'/cache')
		);
		$cache = JCache::getInstance('callback', $options);
		$cache->clean();

		return true;
	}


	function uninstall( &$ex )
	{
		$src	= JPATH_ROOT.DS.'components'.DS.'com_jshopping';
		$files	= array(
			$src.DS.'controllers'.DS.$this->_name.'.php'
		);

		foreach ($files as &$f) {
			if ( JFile::exists($f) ) {
				JFile::delete($f);
			}
		}

		return true;
	}


	function update( &$ex )
	{
		$this->install($ex);
	}


	function customInstall ()
	{
		$db = JFactory::getDbo();

		// Определение имени БД
		$q = "SELECT DATABASE()";
		$db->setQuery($q);
		$dbName = $db->loadResult();
		
		// Создание индекса (если не существует) для ID товаров в таблице зависимых атрибутов
		$q = "SELECT
				COUNT(*) as `cnt`
			FROM INFORMATION_SCHEMA.STATISTICS
			WHERE
				table_schema = '".$dbName."'
				AND
				table_name LIKE '%jshopping_products_attr'
				AND
				index_name='product_id'";
		$db->setQuery($q);
		$exist = $db->loadResult();

		if (!$exist) {
			$q = "ALTER TABLE `#__jshopping_products_attr` ADD INDEX (`product_id`)";
			$db->setQuery($q);
			$db->query();
		}

		// Создание индекса (если не существует) для ID товаров в таблице независимых атрибутов
		$q = "SELECT
				COUNT(*) as `cnt`
			FROM INFORMATION_SCHEMA.STATISTICS
			WHERE
				table_schema = '".$dbName."'
				AND
				table_name LIKE '%jshopping_products_attr2'
				AND
				index_name = 'product_id'";
		$db->setQuery($q);
		$exist = $db->loadResult();

		if (!$exist) {
			$q = "ALTER TABLE `#__jshopping_products_attr2` ADD INDEX (`product_id`)";
			$db->setQuery($q);
			$db->query();
		}

		// Создание таблицы хэшей
		$q = "CREATE TABLE IF NOT EXISTS `#__jsfilter`
			(
				`hash` VARCHAR(32) NOT NULL,
				`date` DATE NOT NULL,
				`hits` INT NOT NULL,
				`params` TEXT NOT NULL,
				PRIMARY KEY (`hash`)
			) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci";
		$db->setQuery($q);
		$db->query();
	}

}
