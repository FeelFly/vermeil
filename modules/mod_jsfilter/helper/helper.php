<?php

defined("_JEXEC") || exit();
if (!defined("DS")) {
	define("DS", DIRECTORY_SEPARATOR);
}
jimport("joomla.filesystem.folder");


class JsfilterHelper
{
	private $_cfg;
	private $_errors = array();
	private $_types = array();
	private $_fields = array();
	private $_sort_fields = array(
		"price" => array("name" => "price", "label" => "MJSF_CFG_SORT_PRICE_LABEL", "title" => "MJSF_CFG_SORT_PRICE_TITLE"),
		"name"  => array("name" => "name", "label" => "MJSF_CFG_SORT_NAME_LABEL", "title" => "MJSF_CFG_SORT_NAME_TITLE"),
		"hits"  => array("name" => "hits", "label" => "MJSF_CFG_SORT_HITS_LABEL", "title" => "MJSF_CFG_SORT_HITS_TITLE"),
		"date"  => array("name" => "date", "label" => "MJSF_CFG_SORT_DATE_LABEL", "title" => "MJSF_CFG_SORT_DATE_TITLE"),
		"mnf"   => array("name" => "mnf", "label" => "MJSF_CFG_SORT_MNF_LABEL", "title" => "MJSF_CFG_SORT_MNF_TITLE"),
		"qnty"  => array("name" => "qnty", "label" => "MJSF_CFG_SORT_QNTY_LABEL", "title" => "MJSF_CFG_SORT_QNTY_TITLE")
		);

	public function __construct()
	{
		$db = JFactory::getDbo();
		$lang = JFactory::getLanguage();
		$lang->load("mod_jsfilter", JPATH_ROOT);
		$fields = JFolder::files(JPATH_ROOT . "/modules/mod_jsfilter/helper/fields", ".*\.php");

		foreach ($fields as $f) {
			$tmp = explode(".", $f);
			$field = $tmp[0];
			$classname = "JsfilterField" . ucfirst($field);
			require_once (JPATH_ROOT . "/modules/mod_jsfilter/helper/fields/" . $field . ".php");
			$this->_fields[$field] = new $classname();
		}

		$types = JFolder::files(JPATH_ROOT . "/modules/mod_jsfilter/helper/types", ".*\.php");

		foreach ($types as $t) {
			$tmp = explode(".", $t);
			$type = $tmp[0];
			$classname = "JsfilterType" . ucfirst($type);
			require_once (JPATH_ROOT . "/modules/mod_jsfilter/helper/types/" . $type . ".php");
			$this->_types[$type] = new $classname();
		}
	}

	public function loadconfig(&$cfg)
	{
		$this->_cfg = $cfg;
	}

	public function buildform()
	{
		$sf = self::getParamsFromRequest();
		$roll = JRequest::getVar("roll", NULL, "COOKIE", "array");
		$filter = JFilterInput::getInstance();
		$user = JFactory::getUser();

		if (!$this->_cfg) {
			$this->setError(JText::_("MJSF_CONFIG_ERR"));
			return;
		}

		if (is_array($this->_cfg)) {
			$this->_cfg = JArrayHelper::toObject($this->_cfg);
		}

		$baseCfg = self::getBaseCfg();
		$baseCfg->cache_time = (int) $baseCfg->cache_time;

		if (!$baseCfg->cache_time) {
			$baseCfg->cache_time = 120;
		}

		$this->cleanHashes();
		$cid = JRequest::getInt("category_id");
		$catList = $this->buildCatList($cid);
		$manufacturer = JRequest::getInt("manufacturer_id");
		$addon = JTable::getInstance("addon", "jshop");
		$addon->loadAlias("user_group_product_price");
		$addonPresent = $addon->id;
		$userGid = 0;

		if ($addonPresent) {
			$jsUser = JSFactory::getUserShop();
			$userGid = $jsUser->usergroup_id;
		}

		$jsCfg = JSFactory::getConfig();
		$params = array("currencyId" => $jsCfg->currency_value, "userGroup" => $addonPresent ? $userGid : 0, "in_stock" => $baseCfg->stock_state == 1);
		$html = "";

		foreach ($this->_cfg->struct as $k => $cfg) {
			if (!$this->_fields[$cfg->field]) {
				continue;
			}

			if (!$this->_types[$cfg->type]) {
				continue;
			}

			if ($baseCfg->stock_state && ($cfg->field == "stock")) {
				continue;
			}

			$field = &$this->_fields[$cfg->field];
			$view = &$this->_types[$cfg->type];
			$cache = JFactory::getCache("jsfilter", "callback");
			$cState = $cache->getCaching();
			$cache->setCaching(1);
			$cache->setLifeTime($baseCfg->cache_time);
			$values = array();

			switch ($cfg->values->mode) {
			case 0:
				$values = $cache->get(array($field, "getAllValues"), array($cfg->ex_field, &$params));
				break;

			case 1:
				$values = $cache->get(array($field, "getFixedValues"), array(&$cfg, &$params));
				break;

			case 2:
				$values = $cache->get(array($field, "getDynamicValues"), array(&$catList, $manufacturer, &$cfg, &$params));
				break;
			}

			$cache->setCaching($cState);

			if (!$values) {
				continue;
			}

			$hidden = NULL;
			$popular = array();
			$cfg->values->popular = (array) $cfg->values->popular;

			if ($cfg->b_mode == 3) {
				$limit = (int) $cfg->b_limit;
				if ($limit && ($cfg->values->selection == 0) && $cfg->values->popular) {
					foreach ($values as &$ptr) {
						if (in_array($ptr->value, $cfg->values->popular) && $limit--) {
							$popular[] = $ptr;
						}
					}
				}
				else {
					if ($limit && ($cfg->values->selection == 1)) {
						foreach ($values as &$ptr) {
							if (!$limit--) {
								break;
							}

							$popular[] = $ptr;
						}
					}
				}
			}

			if (($cfg->b_mode != 3) && $cfg->values->sort) {
				if ($cfg->values->sort == 1) {
					uasort($values, "JsfilterHelper::cmpVal");
				}
				else {
					uasort($values, "JsfilterHelper::cmpValDesc");
				}
			}

			if (is_array($sf[$k][$cfg->field][$cfg->type])) {
				foreach ($sf[$k][$cfg->field][$cfg->type] as $i => &$pv) {
					$sf[$k][$cfg->field][$cfg->type][$i] = $filter->clean($sf[$k][$cfg->field][$cfg->type][$i], "string");
				}
			}
			else {
				$sf[$k][$cfg->field][$cfg->type] = $filter->clean($sf[$k][$cfg->field][$cfg->type], "string");
			}

			$cookieName = $this->_cfg->mid . "_" . $this->_cfg->index . "_" . $k . "_" . $cfg->field . "_" . $cfg->type;
			if (($cfg->b_mode == 2) || ($cfg->b_mode == 3) || ($cfg->b_mode == 4)) {
				$isBlockRoll = false;
			}
			else if ($cfg->b_mode == 3) {
				$isBlockRoll = true;
			}
			else {
				$isBlockRoll = (isset($roll[$cookieName]) ? $roll[$cookieName] == "true" : $cfg->b_mode == 1);
			}

			if ($cfg->b_comment) {
				JHtml::_("behavior.tooltip");
			}

			$html .= "<fieldset class=\"sf_block\">";
			$html .= ($cfg->b_mode == 2 ? "" : "<div class=\"sf_block_header\"><div class=\"sf_block_title\">" . $cfg->label . "</div><div class=\"" . ($cfg->b_mode == 3 ? "sf_ctrl_popular" : ($cfg->b_mode == 4 ? "" : "sf_ctrl")) . ($isBlockRoll ? " roll" : "") . "\" rel=\"" . ($cfg->b_mode == 3 ? "[" . JText::_("MJSF_POPULAR") . "]|[" . JText::_("MJSF_ALL") . "]" : "roll[" . $cookieName . "]") . "\">" . ($cfg->b_mode == 3 ? "[" . JText::_("MJSF_ALL") . "]" : "") . "</div>" . ($cfg->b_comment ? "<div class=\"block_comment hasTip\" title=\"" . htmlspecialchars($cfg->b_comment) . "\"></div>" : "") . "</div>");
			$html .= "<div class=\"sf_block_params\"" . ($isBlockRoll && ($cfg->b_mode != 3) ? " style=\"display:none;\"" : "") . ">";
			$varName = "sf[" . $k . "][" . $cfg->field . "][" . $cfg->type . "]";
			$userValues = NULL;

			if ($popular) {
				$view->load($cfg, $popular);
				$html .= "<div class=\"sf_popular\">" . $view->render($varName, $userValues) . "</div>";
				$view->load($cfg, $values);
				$html .= "<div class=\"sf_all\" style=\"display: none;\">" . $view->render($varName, $userValues) . "</div>";
			}
			else {
				$view->load($cfg, $values);
				$html .= $view->render($varName, $userValues);
			}

			$html .= "</div>";

			if (($cfg->type == "select") && $cfg->values->select_hide_disabled) {
				echo "<style>[name=\"sf[" . $k . "][" . $cfg->field . "][select][]\"] option:disabled { display: none; }</style>";
			}

			if ($this->_cfg->show_block_reset) {
				$html .= "<div class=\"wrap_block_reset\"><div class=\"block_reset\" style=\"display:none;\">" . ($this->_cfg->block_reset_text ? $this->_cfg->block_reset_text : JText::_("PJSF_CFG_BLOCK_RESET_TEXT_DEFAULT")) . "</div></div>";
			}

			$html .= "</fieldset>";
		}

		return $html;
	}

	public function getalltypes()
	{
		$list = array();

		foreach ($this->_types as $k => &$ptr) {
			$obj = new stdClass();
			$obj->type = $k;
			$obj->title = $ptr->getTitle();
			$obj->ex_type = $ptr->getExtSettings();
			$list[] = $obj;
		}

		usort($list, array("JsfilterHelper", "cmp"));
		return $list;
	}

	public function getallfields()
	{
		$list = array();

		foreach ($this->_fields as $name => &$ptr) {
			$obj = new stdClass();
			$obj->name = $name;
			$obj->title = $ptr->getTitle();
			$obj->ex_field = $ptr->getExtSettings();
			$list[] = $obj;
		}

		usort($list, array("JsfilterHelper", "cmp"));
		return $list;
	}

	public function getalllayouts()
	{
		jimport("joomls.filesystem.folder");
		return JFolder::folders(JPATH_ROOT . DS . "modules" . DS . "mod_jsfilter" . DS . "assets" . DS . "layout");
	}

	public function getallsortfields()
	{
		$list = $this->_sort_fields;

		foreach ($list as $k => &$f) {
			$list[$k]["title"] = JText::_($f["title"]);
			$list[$k]["label"] = JText::_($f["label"]);
		}

		return $list;
	}

	public function getmoduleid()
	{
		if (!$this->_cfg) {
			return 0;
		}

		if (is_array($this->_cfg)) {
			return $this->_cfg["mid"];
		}

		if (is_object($this->_cfg)) {
			return $this->_cfg->mid;
		}

		return 0;
	}

	public static function cmp($a, $b)
	{
		$al = strtolower($a->title);
		$bl = strtolower($b->title);

		if ($al == $bl) {
			return 0;
		}

		return $bl < $al ? 1 : -1;
	}

	public static function cmpval($a, $b)
	{
		$al = mb_strtolower($a->text);
		$bl = mb_strtolower($b->text);

		if ($al == $bl) {
			return 0;
		}

		return strnatcmp($al, $bl);
	}

	public static function cmpvaldesc($a, $b)
	{
		$al = mb_strtolower($a->text);
		$bl = mb_strtolower($b->text);

		if ($al == $bl) {
			return 0;
		}

		$res = strnatcmp($al, $bl);

		if (0 < $res) {
			$res = -1;
		}
		else if ($res < 0) {
			$res = 1;
		}

		return $res;
	}

	public function dofilter(&$sf)
	{
		if (!$this->_cfg) {
			$this->setError(JText::_("MJSF_FILTER_MODPARAMS_ERR"));
			return;
		}

		$baseCfg = self::getBaseCfg();

		if (!$sf) {
			foreach ($this->_cfg["struct"] as $index => &$c) {
				if (is_array($c)) {
					$fname = $c["field"];
					$tname = $c["type"];
				}
				else {
					$fname = $c->field;
					$tname = $c->type;
				}

				$sf[$index][$fname][$tname][] = 0;
			}
		}

		$cid = JRequest::getInt("category_id");
		$catList = $this->buildCatList($cid);
		$manufacturer = JRequest::getInt("manufacturer_id");
		$pids = NULL;
		$attrList = NULL;
		$specList = array();
		$filter = JFilterInput::getInstance();

		foreach ($sf as $index => &$block) {
			foreach ($block as $fname => &$values) {
				$field = $this->_fields[$fname];

				if (!$field) {
					continue;
				}

				if (!$values) {
					continue;
				}

				if ($field->special) {
					$specList[$index] = $sf[$index];
					continue;
				}

				foreach ($values as $tname => &$v) {
					$type = $this->_types[$tname];

					if (!$type) {
						continue;
					}

					if (is_array($v)) {
						foreach ($v as $i => &$pv) {
							$v[$i] = $filter->clean($v[$i], "string");
						}
					}
					else {
						$v = $filter->clean($v, "string");
					}

					$cfg = &$this->_cfg["struct"][$index];
					if (!$cfg || ($cfg["field"] != $fname) || ($cfg["type"] != $tname)) {
						continue;
					}

					$cond_id = $field->getCondID();
					$cond_name = $field->getCondName();
					$conditions = $type->getCondition($cond_id, $cond_name, $v);
					$res = $field->doFilter($conditions, $manufacturer, $cfg, $catList, $attrList);

					if (!$res && !is_array($res)) {
						continue;
					}

					$pids[$index] = $res;
				}
			}
		}

		if ($baseCfg->stock_state) {
			$this->_cfg["struct"][999] = array(
				"field"  => "stock",
				"type"   => "checkbox",
				"values" => array("logic" => 0)
				);
		}

		foreach ($specList as $index => &$block) {
			foreach ($block as $fname => &$values) {
				$field = $this->_fields[$fname];

				if (!$field) {
					continue;
				}

				if (!$values) {
					continue;
				}

				foreach ($values as $tname => &$v) {
					$type = $this->_types[$tname];

					if (!$type) {
						continue;
					}

					if (is_array($v)) {
						foreach ($v as $i => &$pv) {
							$v[$i] = $filter->clean($v[$i], "string");
						}
					}
					else {
						$v = $filter->clean($v, "string");
					}

					$cfg = &$this->_cfg["struct"][$index];
					if (!$cfg || ($cfg["field"] != $fname) || ($cfg["type"] != $tname)) {
						continue;
					}

					$cond_id = $field->getCondID();
					$cond_name = $field->getCondName();
					$conditions = $type->getCondition($cond_id, $cond_name, $v);
					$res = $field->doFilter($conditions, $manufacturer, $cfg, $catList, $attrList);

					if (!$res && !is_array($res)) {
						continue;
					}

					$pids[$index] = $res;
				}
			}
		}

		$result = NULL;

		if ($pids) {
			foreach ($pids as &$v) {
				if (!is_array($v)) {
					continue;
				}
				$v = array_unique($v);
				if ($result === NULL) {
					$result = $v;
					continue;
				}
				$result = array_intersect($result, $v);
				//break;
			}
		}

		$this->attrList = $attrList;
		return $result;
	}

	public function getactivevalues($pids)
	{
		if (!$this->_cfg) {
			return;
		}

		if ($pids === NULL) {
			return;
		}

		$cid = JRequest::getInt("category_id");
		$catList = $this->buildCatList($cid);

		if (is_array($this->_cfg)) {
			$struct = &$this->_cfg["struct"];
		}
		else if (is_object($this->_cfg)) {
			$struct = &$this->_cfg->struct;
		}
		else {
			return;
		}

		if (!isset($this->attrList)) {
			$this->attrList = NULL;
		}

		$result = array();

		foreach ($struct as $index => &$cfg) {
			if (is_array($cfg)) {
				$fname = $cfg["field"];
				$tname = $cfg["type"];
			}
			else {
				$fname = $cfg->field;
				$tname = $cfg->type;
			}

			if (!$this->_fields[$fname]) {
				continue;
			}

			$field = &$this->_fields[$fname];

			if (!$field->special) {
				continue;
			}

			$field->getActiveValues($pids, $catList, $cfg, $this->attrList, false);
		}

		foreach ($struct as $index => &$cfg) {
			if (is_array($cfg)) {
				$fname = $cfg["field"];
				$tname = $cfg["type"];
			}
			else {
				$fname = $cfg->field;
				$tname = $cfg->type;
			}

			if (!$this->_fields[$fname]) {
				continue;
			}

			$field = &$this->_fields[$fname];
			$name = "sf[" . $index . "][" . $fname . "][" . $tname . "]";

			if ($tname == "slider") {
				$tmp = $field->getActiveValues($pids, $catList, $cfg, $this->attrList, true);

				foreach ($tmp as $k => $v) {
					$v = str_replace(",", ".", $v);
					$tmp[$k] = (double) $v;
				}

				sort($tmp);
				$minVal = array_shift($tmp);
				$maxVal = ($tmp ? end($tmp) : $minVal);
				$values = array($minVal, $maxVal);
			}
			else {
				$name .= "[]";
				$values = $field->getActiveValues($pids, $catList, $cfg, $this->attrList, false);
			}

			$result[] = array("name" => $name, "type" => $tname, "values" => $values);
		}

		return $result;
	}

	public function getfieldallvalues($field, $ex_field)
	{
		if (!$this->_fields[$field]) {
			return;
		}

		$jsCfg = JSFactory::getConfig();
		$params = array("currencyId" => $jsCfg->currency_value);
		return $this->_fields[$field]->getAllValues($ex_field, $params);
	}

	public function buildcatlist($cid)
	{
		$cid = (int) $cid;
		$recurse = (is_object($this->_cfg) ? $this->_cfg->in_subcat : $this->_cfg["in_subcat"]);

		if (!$cid) {
			return array();
		}

		if (!$recurse) {
			return array($cid);
		}

		static $allCats = array();

		if (!$allCats) {
			$db = JFactory::getDbo();
			$user = JFactory::getUser();
			$q = "SELECT
					`category_id`,
					`category_parent_id` as `parent_id`
				FROM `#__jshopping_categories`
				WHERE
					`category_publish` = 1
					AND
					`access` IN (" . join(",", $user->getAuthorisedViewLevels()) . ")";
			$db->setQuery($q);
			$allCats = $db->loadObjectList("category_id");
		}

		if (!$allCats[$cid]) {
			return array($cid);
		}

		$list = array($cid);
		$this->getCatChildrens($allCats, $list, $cid);
		return $list;
	}

	public static function getbasecfg()
	{
		$table = JTable::getInstance("extension");
		$table->load(array("type" => "module", "element" => "mod_jsfilter"));
		$params = json_decode($table->params);
		return $params->cfg;
	}

	public static function getconfig($id)
	{
		$db = JFactory::getDbo();
		$id = (int) $id;

		if (!$id) {
			return;
		}

		$q = "SELECT
					`extension_id`,
					`params`
				FROM `#__extensions`
				WHERE
					`type` = 'plugin'
					AND `element` = 'jsfilter'
				LIMIT 1";
		$db->setQuery($q);
		$data = $db->loadObject();

		if ($data->extension_id) {
			$allParams = json_decode($data->params, true);

			if (isset($allParams["configs"][$id])) {
				$modParams = &$allParams["configs"][$id];
			}
			else {
				$modParams = NULL;
			}
		}
		else {
			$modParams = NULL;
		}

		return $modParams;
	}

	public static function saveconfig($id, &$cfg)
	{
		$db = JFactory::getDbo();
		$id = (int) $id;

		if ($id < 0) {
			return JText::_("PJSF_CONFIG_INDEX_ERROR");
		}

		$query = "SELECT
					`extension_id`,
					`params`
				FROM `#__extensions`
				WHERE
					`type` = 'plugin'
					AND `element` = 'jsfilter'
				LIMIT 1";
		$db->setQuery($query);
		$data = $db->loadObject();

		if (!$data->extension_id) {
			return "Extension error";
		}

		$allParams = json_decode($data->params, true);

		if (!isset($allParams["configs"])) {
			$allParams["configs"] = array();
		}

		if ($id == 0) {
			$last = end($allParams["configs"]);
			$id = ($last ? (int) $last["id"] : 0);
			++$id;
		}

		$cfg["id"] = $id;
		$allParams["configs"][$id] = $cfg;
		$query = "UPDATE `#__extensions`
				SET
					`params` = " . $db->Quote(json_encode($allParams)) . "
				WHERE
					`extension_id` = " . $data->extension_id . "
				LIMIT 1";
		$db->setQuery($query);

		if (!$db->query()) {
			return JText::_("PJSF_SAVE_ERROR") . ": " . $db->getErrorMsg();
		}
	}

	public static function rmconfig($id)
	{
		$db = JFactory::getDbo();
		$query = "SELECT
					`extension_id`,
					`params`
				FROM `#__extensions`
				WHERE
					`type` = 'plugin'
					AND `element` = 'jsfilter'
				LIMIT 1";
		$db->setQuery($query);
		$data = $db->loadObject();

		if (!$data->extension_id) {
			return "Extension error";
		}

		$allParams = json_decode($data->params, true);

		if (!isset($allParams["configs"])) {
			return JText::_("PJSF_CONFIG_INDEX_ERROR");
		}

		if (is_array($id)) {
			foreach ($id as $i) {
				$i = (int) $i;

				if (isset($allParams["configs"][$i])) {
					unset($allParams["configs"][$i]);
				}
			}
		}
		else {
			unset($allParams["configs"][$id]);
		}

		$query = "UPDATE `#__extensions`
				SET
					`params` = " . $db->Quote(json_encode($allParams)) . "
				WHERE
					`extension_id` = " . $data->extension_id . "
				LIMIT 1";
		$db->setQuery($query);

		if (!$db->query()) {
			return JText::_("PJSF_SAVE_ERROR") . ": " . $db->getErrorMsg();
		}
	}

	public static function getconfiglist($mid = 0)
	{
		$db = JFactory::getDbo();
		$mid = (int) $mid;
		$query = "SELECT
					`extension_id`,
					`params`
				FROM `#__extensions`
				WHERE
					`type` = 'plugin'
					AND `element` = 'jsfilter'
				LIMIT 1";
		$db->setQuery($query);
		$data = $db->loadObject();

		if (!$data->extension_id) {
			return;
		}

		$allParams = json_decode($data->params, true);
		if (!$allParams || !$allParams["configs"]) {
			return;
		}

		$cfgList = array();

		foreach ($allParams["configs"] as $id => &$cfg) {
			if (($mid == 0) || ($mid == $cfg["mid"])) {
				$cfgList[$id] = $cfg;
			}
		}

		return $cfgList;
	}

	public static function checkconfigsize()
	{
		$db = JFactory::getDbo();
		$columns = $db->getTableColumns("#__extensions");

		if ($columns["params"] == "text") {
			$q = "SELECT
					`params`
				FROM `#__extensions`
				WHERE
					`type` = 'plugin'
					AND `element` = 'jsfilter'
				LIMIT 1";
			$db->setQuery($q);
			$params = $db->loadResult();
			$paramsSize = strlen($params);

			if (60000 < $paramsSize) {
				return JText::_("PJSF_CONFIG_SIZE_WARNING");
			}
		}

		return "";
	}

	public static function getparamsfromrequest()
	{
		$sf = (array) JRequest::getVar("sf", NULL, "", "array");
		$hash = JRequest::getCmd("sf_params", "");

		if ($hash) {
			$db = JFactory::getDbo();
			$db->setQuery("SELECT * FROM `#__jsfilter` WHERE `hash` = " . $db->Quote($hash) . " LIMIT 1");
			$result = $db->loadObject();
			if ($result && $result->params) {
				$sf = unserialize($result->params);
			}
		}

		return $sf;
	}

	private function cleanhashes()
	{
		$db = JFactory::getDbo();
		$cfg = self::getBaseCfg();
		$days = (int) $cfg->hash_days;

		if (!$days) {
			$days = 15;
		}

		$q = "DELETE FROM `#__jsfilter` WHERE `date` < CURDATE() - INTERVAL " . $days . " DAY";
		$db->setQuery($q);
		$db->query();
	}

	public function geterrormsg()
	{
		return array_pop($this->_errors);
	}

	private function seterror($msg)
	{
		$this->_errors[] = $msg;
	}


	private function getcatchildrens(&$all, &$result, $id)
	{
		foreach ($all as &$c) {
			if ($c->parent_id == $id) {
				$result[] = $c->category_id;
				$this->getCatChildrens($all, $result, $c->category_id);
			}
		}
	}

}
