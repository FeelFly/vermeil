<?php

// no direct access
defined('_JEXEC') or die;

// для JSFactory::getLang() и buildTreeCategory()
require_once( JPATH_ROOT.DS.'components'.DS.'com_jshopping'.DS.'lib'.DS.'factory.php' );
require_once( JPATH_ROOT.DS.'components'.DS.'com_jshopping'.DS.'lib'.DS.'functions.php' );
require_once( dirname(__FILE__).DS."..".DS."field.php");


class JsfilterFieldEan extends JsfilterField
{
	protected $values = array();
	// кодовый набор символов для замены в conditions
	private $sep = '%@#';


	// ------------------------------------------------------------------------
	// Конструктор
	// ------------------------------------------------------------------------
	
	function __construct()
	{
		$this->name	= basename(__FILE__, ".php");
		$this->title= 'MJSF_FIELD_EAN';
	}


	// ------------------------------------------------------------------------
	// Получение списка всех доступных значений для текущего поля
	// ------------------------------------------------------------------------
	// ex_field		Значение параметра расширенной конфигурации
	// params		Дополнительные параметры
	// ------------------------------------------------------------------------

	public function getAllValues ($ex_field, &$params)
	{
		$db = JFactory::getDbo();
		$q = "(
				SELECT DISTINCT
					`product_ean` as `value`,
					`product_ean` as `text`
				FROM `#__jshopping_products`
				WHERE
					`product_publish` = 1
					AND
					`product_ean` != ''
			)
			UNION
			(
				SELECT DISTINCT
					a.`ean` as `value`,
					a.`ean` as `text`
				FROM `#__jshopping_products_attr` a
				LEFT JOIN `#__jshopping_products` p
						ON (a.`product_id` = p.`product_id`)
				WHERE
					p.`product_publish` = 1
					AND
					a.`ean` != ''
			)
			ORDER BY `value`";
						
		$db->setQuery($q);

		return $db->loadObjectList();
	}


	// ------------------------------------------------------------------------
	// Формирование списка для фиксированного набора значений
	// ------------------------------------------------------------------------
	// cfg		Конфигурация блока (строка конструктора)
	// params	Дополнительные параметры
	// ------------------------------------------------------------------------
	
	public function getFixedValues (&$cfg, &$params)
	{
		$values = array();

		$cfg->values->list = (array) $cfg->values->list;
		
		if ($cfg->values->list)
		{
			$db = JFactory::getDbo();

			foreach ($cfg->values->list as &$v)
			{
				$v = $db->quote($v);
			}

			$q = "(
					SELECT DISTINCT
						`product_ean` as `value`,
						`product_ean` as `text`
					FROM `#__jshopping_products`
					WHERE 
						`product_publish` = 1
						AND
						`product_ean` IN (".join(',', $cfg->values->list).")
				)
				UNION
				(
					SELECT DISTINCT
						a.`ean` as `value`,
						a.`ean` as `text`
					FROM `#__jshopping_products_attr` a
					LEFT JOIN `#__jshopping_products` p
						ON (a.`product_id` = p.`product_id`)
					WHERE
						p.`product_publish` = 1
						AND
						a.`ean` IN (".join(',', $cfg->values->list).")
				)
				ORDER BY `value`";
							
			$db->setQuery($q);

			$values = $db->loadObjectList();
		}

		return $values;
	}


	// ------------------------------------------------------------------------
	// Формирование списка для динамического набора значений
	// ------------------------------------------------------------------------
	// cid		Категории, для которых осуществляется выбор значений
	// cfg		Конфигурация блока (строка конструктора)
	// params	Дополнительные параметры
	// ------------------------------------------------------------------------
	
	public function getDynamicValues (&$cid, $manufacturer, &$cfg, &$params)
	{
		$db = JFactory::getDbo();
		$manufacturer = (int) $manufacturer;
		$values = array();

		// Подбор значений только внутри категории
		if (!$cid && !$manufacturer) return $values;
		// Для режима без подбора должен быть сформирован список значений
		if ( $cfg->values->selection == 0 && !isset($cfg->values->list) ) return $values;

		$cfg->values->list = (array) $cfg->values->list;
		foreach ($cfg->values->list as &$v)
		{
			$v = $db->quote($v);
		}
			

		$q = "(
				SELECT DISTINCT
					p.`product_ean` as `value`,
					p.`product_ean` as `text`
				FROM `#__jshopping_products` p
				".( ($cid)
					? "LEFT JOIN `#__jshopping_products_to_categories` c"
						." ON ( p.`product_id` = c.`product_id` )"
					: ''
				)."
				WHERE
					p.`product_publish` = 1
					AND
					p.`product_ean` != ''
					".( ($manufacturer)
							? "AND p.`product_manufacturer_id` = ".$manufacturer
							: ""
					)."
					".( ($params['in_stock'])
							? "AND p.`product_quantity` > 0"
							: ''
					)."
					".( ($cid)
						? "AND c.`category_id` IN (".join(',', $cid).")"
						: ""
					)."
					".( ($cfg->values->selection == 0)
						? "AND p.`product_ean` IN (".join(',', $cfg->values->list).")"
						: ''
					)."
			)
			UNION
			(
				SELECT DISTINCT
					a.`ean` as `value`,
					a.`ean` as `text`
				FROM `#__jshopping_products_attr` a
				LEFT JOIN `#__jshopping_products` p
						ON (a.`product_id` = p.`product_id`)
				".( ($manufacturer)
						? "LEFT JOIN `#__jshopping_products` p "
							."ON ( a.`product_id` = p.`product_id` )"
						: ""
				)."
				".( ($cid)
						? "LEFT JOIN `#__jshopping_products_to_categories` c"
							." ON ( a.`product_id` = c.`product_id` )"
						: ''
				)."
				WHERE
					p.`product_publish` = 1
					AND
					a.`ean` != ''
					".( ($manufacturer)
						? "AND p.`product_manufacturer_id` = ".$manufacturer
						: ""
					)."
					".( ($params['in_stock'])
						? "AND a.`count` > 0"
						: ''
					)."
					".( ($cid)
						? "AND c.`category_id` IN (".join(',', $cid).")"
						: ""
					)."
					".( ($cfg->values->selection == 0)
						? "AND a.`ean` IN (".join(',', $cfg->values->list).")"
						: ''
					)."
			)
			ORDER BY `value`";
		
		$db->setQuery($q);

		return $db->loadObjectList();
	}


	// ------------------------------------------------------------------------
	// Формирование списка доступных значений для указанных товаров
	// ------------------------------------------------------------------------
	// pids		Список ID товаров
	// catList	Список категорий для поиска
	// cfg		Конфигурация
	// attrList	Дополнительный список фильтрации (зависимые атрибуты)
	// valType	Тип значений: ID - false, названия - true
	// \return	Массив ID актуальных значений параметра
	// ------------------------------------------------------------------------

	public function getActiveValues (&$pids, &$catList, &$cfg, &$attrList, $valType)
	{
		$db = JFactory::getDbo();
		$values = array();

		// При отсутствии товаров в списке вызвращается пустой список значений
		if (!$pids) return $values;

		$q = "(
				SELECT DISTINCT
					p.`product_ean` as `ean`
				FROM `#__jshopping_products` p
				".( ($catList)
					? "LEFT JOIN `#__jshopping_products_to_categories` c"
						." ON ( p.`product_id` = c.`product_id` )"
					: ''
				)."
				WHERE
					p.`product_id` IN (".join(',', $pids).")
					AND
					p.`product_ean` != ''
					".( ($catList)
						? "AND c.`category_id` IN (".join(',', $catList).")"
						: ""
					)."
			)
			UNION
			(
				SELECT DISTINCT
					a.`ean` as `ean`
				FROM `#__jshopping_products_attr` a
				LEFT JOIN `#__jshopping_products` p
						ON (a.`product_id` = p.`product_id`)
				".( ($catList)
					? "LEFT JOIN `#__jshopping_products_to_categories` c"
						." ON ( a.`product_id` = c.`product_id` )"
					: ''
				)."
				WHERE
					p.`product_id` IN (".join(',', $pids).")
					AND
					a.`ean` != ''
					".( ($catList)
						? "AND c.`category_id` IN (".join(',', $catList).")"
						: ""
					)."
					".( ($attrList !== null)
						? ( ($attrList)
							? "AND a.`product_attr_id` IN ( ".join(',', $attrList)." )"
							: "AND 1 != 1"
						  )
						: ''
					)."
			)";

		$db->setQuery($q);
		$values = $db->loadColumn();
		
		return $values;
	}


	// ------------------------------------------------------------------------
	// Выполнение фильтрации данных
	// ------------------------------------------------------------------------
	// conditions	Условия для выбора даннных из БД (where)
	// cfg			Конфигурация блока (строка конструктора)
	// cid			Категории, в которых выполняется поиск товаров
	// attrList		Дополнительный список фильтрации (зависимые атрибуты)
	// \return		Массив ID отфильтрованных товаров
	// ------------------------------------------------------------------------

	public function doFilter ($conditions, $manufacturer, &$cfg, &$cid, &$attrList)
	{
		$db = JFactory::getDbo();
		
		// Формирование условия фильтрации в зависимости от выбранной логики
		$condition = join( (($cfg['values']['logic']) ? " AND " : " OR "), (array)$conditions );

		$condProd = str_replace($this->getCondID(), 'p.`product_ean`', $condition);
		$condAttr = str_replace($this->getCondID(), 'a.`ean`', $condition);

		$q = "(
				SELECT DISTINCT
					p.`product_id`
				FROM `#__jshopping_products` p
				".( ($cid)
					? "LEFT JOIN `#__jshopping_products_to_categories` c"
						." ON ( p.`product_id` = c.`product_id` )"
					: ''
				)."
				WHERE
					p.`product_publish` = 1
					AND p.`product_ean` != ''
					".( ($manufacturer)
						? "AND p.`product_manufacturer_id` = ".$manufacturer
						: ''
					)."
					".( ($cid)
						? "AND c.`category_id` IN (".join(',', $cid).")"
						: ""
					)."
					".( ($condProd)
						? "AND ( ".$condProd." )"
						: ''
					)."
			)
			UNION
			(
				SELECT DISTINCT
					a.`product_id`
				FROM `#__jshopping_products_attr` a
				LEFT JOIN `#__jshopping_products` p
						ON ( a.`product_id` = p.`product_id` )
				".( ($cid)
					? "LEFT JOIN `#__jshopping_products_to_categories` c"
						." ON ( a.`product_id` = c.`product_id` )"
					: ''
				)."
				WHERE
					p.`product_publish` = 1
					AND a.`ean` != ''
					".( ($manufacturer)
						? "AND p.`product_manufacturer_id` = ".$manufacturer
						: ''
					)."
					".( ($cid)
						? "AND c.`category_id` IN (".join(',', $cid).")"
						: ""
					)."
					".( ($attrList !== null)
						? ( ($attrList)
							? "AND a.`product_attr_id` IN ( ".join(',', $attrList)." )"
							: "AND 1 != 1"
						  )
						: ''
					)."
					".( ($condAttr)
						? "AND ( ".$condAttr." )"
						: ''
					)."
			)";

		$db->setQuery($q);

		return $db->loadColumn();
	}


	// ------------------------------------------------------------------------
	// Получение имени поля со значениями id (см. doFilter())
	// ------------------------------------------------------------------------

	public function getCondID ()
	{
		return "__".$this->sep."__";
	}
	

	// ------------------------------------------------------------------------
	// Получение имени поля со значениями названий (см. doFilter())
	// ------------------------------------------------------------------------

	public function getCondName ()
	{
		return "__".$this->sep."__";
	}
	
}
