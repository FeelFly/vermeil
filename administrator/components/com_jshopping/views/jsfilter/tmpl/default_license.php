<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.tooltip');
?>

<div class="width-100" id="desc-wrap">
<fieldset class="adminform long">

	<form action="index.php?option=com_jshopping&controller=jsfilter" method="post" name="adminForm" enctype="multipart/form-data">
		<?php if (!$this->isLocked) { ?>
		<ul class="adminformlist">

			<li>
				<label title="<?php echo JText::_('PJSF_LICENSE_TIP');?>" class="hasTip">
					<?php echo JText::_('PJSF_LICENSE');?>:
				</label>
				<input type="text" id="key" size="35" name="key" value="" />
			</li>

			<li>
				<label>
					<br />
					<input type="submit" value="<?php echo JText::_('PJSF_SAVE');?>" style="float:right;" />
				</label>
			</li>
			
		</ul>
		<?php } ?>

	<input type="hidden" name="task" value="license" />
</form>

</fieldset>
</div>

