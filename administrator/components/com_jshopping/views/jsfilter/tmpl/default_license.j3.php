<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.tooltip');

?>

<div class="form-horizontal">
<div class="row-fluid">
	
	<form action="index.php?option=com_jshopping&controller=jsfilter" method="post" name="adminForm" enctype="multipart/form-data">
		<?php if (!$this->isLocked) { ?>
		<div class="control-group">
			<div class="control-label">
				<label title="<?php echo JText::_('PJSF_LICENSE_TIP');?>" class="hasTip">
					<?php echo JText::_('PJSF_LICENSE');?>:
				</label>
			</div>
			<div class="controls">
				<input type="text" size="35" name="key" value="" />
			</div>
		</div>

		<div class="control-group">
			<div class="control-label">
			</div>
			<div class="controls">
				<input type="submit" value="<?php echo JText::_('PJSF_SAVE');?>" />
			</div>
		</div>

		<?php } ?>

		<input type="hidden" name="task" value="license" />
	</form>
	
</div>
</div>
