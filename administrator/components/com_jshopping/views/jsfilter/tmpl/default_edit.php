<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('stylesheet', 'administrator/components/com_jshopping/css/jsfilter/jsfilter.css');

JHtml::_('behavior.tooltip');
$jsCfg = JSFactory::getConfig();
JHtml::_('script', $jsCfg->live_path.'js/jquery/jquery-'.$jsCfg->load_jquery_version.'.min.js');
JHtml::_('script', $jsCfg->live_path.'js/jquery/jquery-noconflict.js');
JHtml::_('script', 'administrator/components/com_jshopping/js/jsfilter/jsfilter.js');

$cfg = &$this->cfg;

?>

<script>
	var SF_lockListMenu = [];
	var SF_lockListCats = [];
	var SFTip;
	
	jQuery(function() {
		// Создание объекта всплывающих подсказок для конструктора
		SFTip = new Tips(null, {'fixed': true, 'offset': {'x': 0, 'y': 35}});
		// Структура элементов фильтрации
		fields = <?php echo json_encode( ($cfg['struct']) ? $cfg['struct'] : array() ); ?>;
		buildStruct(fields);
		// Блокировка пунктов меню и категорий
		onChangeModule( jQuery('#cfg_mid').get(0) );
	});
</script>


<?php
// сэмплы для доп.полей
foreach ( $this->fields as &$f ) {
	
	// Доп.настройки для выбранного поля
	echo '<div id="sample_field_'.$f->name.'" style="display:none;">'
			.( ($f->ex_field)
				? '<div rel="'.$f->ex_field[0].'">'.$f->ex_field[1].'</div>'
				: '<input name="cfg[struct][ex_field][]" type="hidden" value="" />'
			)
		.'</div>';
}
foreach ( $this->types as &$t ) {
	// Доп.настройки для выбранного типа элемента
	echo '<div id="sample_type_'.$t->type.'" style="display:none;">'
			.( ($t->ex_type)
				? '<div rel="'.$t->ex_type[0].'">'.$t->ex_type[1].'</div>'
				: '<input name="cfg[struct][ex_type][]" type="hidden" value="" />'
			)
		.'</div>';
}
?>

<!--Шаблон формы выбора значений-->
<div id="popup_sample" style="display:none;">
	<div id="content_wrap" class="jsp_admin">
		<div id="msg"></div>
		
		<div id="loading">
			<img src="/media/system/images/modal/spinner.gif" />
			<span id="info"><?php echo JText::_('PJSF_VALUES_LOADING'); ?></span>
		</div>
		
		<div id="content">
			<h3><?php echo JText::_('PJSF_TITLE_VALUES_WINDOW'); ?></h3>

			<div class="row backlight">
				<label class="column col2">
					<?php echo JText::_('PJSF_CFG_VAL_MODE_LABEL'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_MODE_DESC'); ?></span>
				</div>
			</div>

			<div class="row config">
				<label class="column col2">
					<input type="radio" name="val[mode]" onchange="valModeHandler(this);" value="0" />
					<?php echo JText::_('PJSF_CFG_VAL_MODE_ALL'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_MODE_ALL_DESC'); ?></span>
				</div>
			</div>

			<div class="row config">
				<label class="column col2">
					<input type="radio" name="val[mode]" onchange="valModeHandler(this);" value="1" />
					<?php echo JText::_('PJSF_CFG_VAL_MODE_FIXED'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_MODE_FIXED_DESC'); ?></span>
				</div>
			</div>

			<div class="row config">
				<label class="column col2">
					<input type="radio" name="val[mode]" onchange="valModeHandler(this);" value="2" />
					<?php echo JText::_('PJSF_CFG_VAL_MODE_DYNAMIC'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_MODE_DYN_DESC'); ?></span>
				</div>
			</div>

			<div id="row_auto_sel">
				<div class="row backlight">
					<div class="column col2">
						<?php echo JText::_('PJSF_CFG_VAL_AUTO_SELECTION_LABEL'); ?>
					</div>
					<div class="column">
						<?php echo JText::_('PJSF_CFG_VAL_AUTO_SELECTION_DESC'); ?>
					</div>
				</div>	
			
				<div class="row config">			
					<div class="column col2">
						<label class="radiobtn">
							<input type="radio" name="val[selection]" onchange="valAutoSelectionHandler(this);" value="0" />
							<?php echo JText::_('JNO'); ?>
						</label>
					</div>
				</div>	
				<div class="row config">
					<div class="column col2">
						<label class="radiobtn">
							<input type="radio" name="val[selection]" onchange="valAutoSelectionHandler(this);" value="1" />
							<?php echo JText::_('JYES'); ?>
						</label>
					</div>
				</div>
			</div>

			<div class="row backlight">
				<label class="column col2">
					<?php echo JText::_('PJSF_CFG_VAL_LOGIC_LABEL'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_LOGIC_DESC'); ?></span>
				</div>
			</div>

			<div class="row config">			
				<label class="column col2">
					<input type="radio" name="val[logic]" value="0" />
					<?php echo JText::_('PJSF_CFG_VAL_LOGIC_OR'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_LOGIC_OR_DESC'); ?></span>
				</div>
			</div>

			<div class="row config">			
				<label class="column col2">
					<input type="radio" name="val[logic]" value="1" />
					<?php echo JText::_('PJSF_CFG_VAL_LOGIC_AND'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_LOGIC_AND_DESC'); ?></span>
				</div>
			</div>


			<div class="row backlight">
				<label class="column col2">
					<?php echo JText::_('PJSF_CFG_VAL_SORT_LABEL'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_SORT_DESCRIPTION'); ?></span>
				</div>
			</div>

			<div class="row config">
				<label class="column col2">
					<input type="radio" name="val[sort]" value="0" />
					<?php echo JText::_('PJSF_CFG_VAL_SORT_DEFAULT'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_SORT_DEFAULT_DESC'); ?></span>
				</div>
			</div>

			<div class="row config">			
				<label class="column col2">
					<input type="radio" name="val[sort]" value="1" />
					<?php echo JText::_('PJSF_CFG_VAL_SORT_ASC'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_SORT_ASC_DESC'); ?></span>
				</div>
			</div>

			<div class="row config">
				<label class="column col2">
					<input type="radio" name="val[sort]" value="2" />
					<?php echo JText::_('PJSF_CFG_VAL_SORT_DESC'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_SORT_DESC_DESC'); ?></span>
				</div>
			</div>


			<div id="type_select_opt" style="display:none;">
			<div class="row backlight">
				<label class="column col2">
					<?php echo JText::_('PJSF_CFG_SELECT_HIDE_DISABLED_LABEL'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_SELECT_HIDE_DISABLED_DESC'); ?></span>
				</div>
			</div>
			<div class="row config">
				<label class="column col2">
					<input type="radio" name="val[select_hide_disabled]" value="0" />
					<?php echo JText::_('JNO'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_SELECT_HIDE_DISABLED_NO_DESC'); ?></span>
				</div>
			</div>
			<div class="row config">
				<label class="column col2">
					<input type="radio" name="val[select_hide_disabled]" value="1" />
					<?php echo JText::_('JYES'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_SELECT_HIDE_DISABLED_YES_DESC'); ?></span>
				</div>
			</div>
			</div>

			
			<div id="row_values">
				<div class="row backlight">
					<label class="column col2">
						<?php echo JText::_('PJSF_CFG_VAL_SELECT_LABEL'); ?>
					</label>
				</div>
				
				<div id="values" class="row config">
					<div class="val_item">
						<div class="val_title col2"></div>
						<div class="val_select">
							<button onclick="toggleCheckValues(this, 'val_select');" rel="0">
								<?php echo JText::_('PJSF_CFG_VAL_TOGLE_BUTTON'); ?>
							</button>
						</div>
						<div class="val_popular">
							<button onclick="toggleCheckValues(this, 'val_popular');" rel="0">
								<?php echo JText::_('PJSF_CFG_VAL_TOGLE_BUTTON'); ?>
							</button>
						</div>
					</div>
				</div>
			</div>

			<div class="row backlight">
				<label class="column col2">
					<?php echo JText::_('PJSF_CFG_VAL_COMMENT_LABEL'); ?>
				</label>
				<div class="column">
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_COMMENT_DESC'); ?></span>
				</div>
			</div>
			
			<div class="row config">			
				<label class="column col2">
					<button id="commentBtn" class="btn"><?php echo JText::_('PJSF_CFG_VAL_COMMENT_COPY'); ?></button>
				</label>
				<div class="column">
					<textarea name="comment"></textarea>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div id="values_sample" style="display:none;">
	<div class="val_item">
		<div class="val_title col2"></div>
		<div class="val_select">
			<input name="val[list]" type="checkbox" value="" />
		</div>
		<div class="val_popular">
			<input name="val[popular]" type="checkbox" value="" />
		</div>
	</div>
</div>


 <!-- Конструктор (блоки) -->
<table style="display:none;">
<tbody id="sample">
	<tr class="constructor">
	<td class="const-td">
		<div class="column col4">
			<?php
			echo JHtml::_(
				'select.genericlist',
				$this->fields,
				'cfg[struct][field][]',
				'onchange="addEx(this, \'field\'); fieldHandler(this);"',
				'name',
				'title',
				0,
				'sfield'
			);
			?>
		</div>
	</td>
	<td class="const-td">
		<div class="column col4">
			<?php
			echo JHtml::_(
				'select.genericlist',
				$this->types,
				'cfg[struct][type][]',
				'onchange="addEx(this, \'type\')"',
				'type',
				'title',
				0,
				'stype'
			);
			?>
		</div>
	</td>
	<td class="const-td">
		<div class="column col4 sfTip" rel="<?php echo JText::_('PJSF_BLOCK_TYPE_TIP'); ?>">
			<select id="b_mode" name="cfg[struct][b_mode][]">
				<option value="0"><?php echo JText::_('PJSF_UNCOLLAPSED'); ?></option>
				<option value="1"><?php echo JText::_('PJSF_COLLAPSED'); ?></option>
				<option value="4"><?php echo JText::_('PJSF_DONT_COLLAPSE'); ?></option>
				<option value="2"><?php echo JText::_('PJSF_WITHOUT_TITLE'); ?></option>
				<option value="3"><?php echo JText::_('PJSF_POPULAR'); ?></option>
			</select>
		</div>
	</td>
	<td class="const-td">
		<div class="column col4 sfTip" rel="<?php echo JText::_('PJSF_BLOCK_LIMIT_TIP'); ?>">
			<input type="text" id="b_limit" name="cfg[struct][b_limit][]" value="" />
		</div>
	</td>
	<td class="const-td">
		<div class="column col4 sfTip" rel="<?php echo JText::_('PJSF_LABEL_TIP'); ?>">
			<input type="text" id="slabel" name="cfg[struct][label][]" value="<?php echo JText::_('PJSF_LABEL'); ?>" onfocus="if (this.value=='<?php echo JText::_('PJSF_LABEL'); ?>') this.value='';" onblur="if (this.value=='') this.value='<?php echo JText::_('PJSF_LABEL'); ?>';" />
		</div>
	</td>
	<td class="const-td">
		<div id="ex_field" class="">
		</div>
	</td>
	<td class="const-td">
		<div id="ex_type" class="">	
		</div>
	</td>
	<td class="const-btn">
		<input type="hidden" id="values" name="cfg[struct][values][]" value="val%5Bmode%5D=2&val%5Bselection%5D=1&val%5Blogic%5D=0&val%5Bsort%5D=0&val%5Bselect_hide_disabled%5D=0" />
		<input type="hidden" id="comment" name="cfg[struct][comment][]" value="" />

		<div class="">
			<a class="button-add" href="javascript:void(0);" onclick="var el = addBlock(jQuery(this).closest('.constructor').get(0), 'sample', 'struct_wrap'); buildExFields(el);" title="<?php echo JText::_('PJSF_ADD');?>" >
				<span class="btn-add"></span>
			</a>
			<a class="button-val" href="javascript:void(0);" onclick="selectValues( jQuery(this).closest('.constructor') );" title="<?php echo JText::_('PJSF_BTN_VALUES');?>" >
				<span class="btn-val"></span>
			</a>
			<a class="button-rem" href="javascript:void(0);" onclick="rmBlock(jQuery(this).closest('.constructor').get(0), 'struct_wrap')" title="<?php echo JText::_('PJSF_REMOVE');?>">
				<span class="btn-rem"></span>
			</a>
		</div>
	</td>
	</tr>
</tbody>
</table>


<div class="jsp_admin">
    <form class="form-horizontal" action="index.php?option=com_jshopping&controller=jsfilter" method="post" name="adminForm" enctype="multipart/form-data">

		<div class="row backlight">
			<div><?php echo JText::_('PJSF_TITLE_GENERIC_PARAMS'); ?></div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_NAME_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_NAME_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<input type="text" name="cfg[name]" value="<?php echo $cfg['name']; ?>" />
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_NAME_INFO'); ?></span>
			</div>
		</div>
		
		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_TITLE_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_TITLE_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<input type="text" name="cfg[title]" value="<?php echo $cfg['title']; ?>" />
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_TITLE_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_MODULE_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_MODULE_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.genericlist',
								$this->modules,
								'cfg[mid]',
								'autocomplete="off" onchange="onChangeModule(this)"',
								'id',
								'title',
								$cfg['mid'],
								'cfg_mid'
				);
				?>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_MODULE_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_LAYOUTS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_LAYOUTS_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<select name="cfg[layout]">
				<?php
				foreach ($this->layouts as &$layout) {
					echo '<option value="'.$layout.'" '.(($cfg['layout'] == $layout) ? 'selected="selected"' : '').'>'.$layout.'</option>';
				}
				?>
				</select>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_LAYOUTS_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_FILTER_MODE_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_FILTER_MODE_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<select name="cfg[deactivate_values]" autocomplete="off">
					<option value="0" <?php echo (!(int) $cfg['deactivate_values']) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_FILTER_MODE_CLASSIC');?></option>
					<option value="1" <?php echo ($cfg['deactivate_values'] == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_FILTER_MODE_DEACTIVATE');?></option>
					<option value="2" <?php echo ($cfg['deactivate_values'] == 2) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_FILTER_MODE_STEP_BY_STEP');?></option>
				</select>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_DEACTIVATE_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_PUBLISHED_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_PUBLISHED_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[published]',
								'',
								$cfg['published'],
								'JYES', 'JNO',
								'cfg_published'
				);
				?>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_PUBLISHED_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_SEARCH_IN_SUBCAT_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_SEARCH_IN_SUBCAT_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[in_subcat]',
								'',
								$cfg['in_subcat'],
								'JYES', 'JNO',
								'cfg_in_subcat'
				);
				?>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_SEARCH_IN_SUBCAT_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_SHOW_IN_PROD_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_SHOW_IN_PROD_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[in_prod]',
								'',
								$cfg['in_prod'],
								'JYES', 'JNO',
								'cfg_in_prod'
				);
				?>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_SHOW_IN_PROD_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_TIP_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_TIP_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<select name="cfg[show_tip]" autocomplete="off">
					<option value="0" <?php echo (!(int) $cfg['show_tip']) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_TIP_NONE');?></option>
					<option value="1" <?php echo ($cfg['show_tip'] == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_TIP_SHOW');?></option>
					<option value="2" <?php echo ($cfg['show_tip'] == 2) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_TIP_AUTO');?></option>
				</select>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_TIP_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.genericlist',
								$this->sort_fields,
								'cfg[default_sorting]',
								'autocomplete="off"',
								'name',
								'title',
								$cfg['default_sorting'],
								'cfg_default_sorting'
				);
				?>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_DIRECTION_DESCRIPTION'); ?>">
					<?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_DIRECTION_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[default_sorting_dir]',
								'',
								$cfg['default_sorting_dir'],
								'PJSF_CFG_DEFAULT_SORTING_DIRECTION_DESC', 'PJSF_CFG_DEFAULT_SORTING_DIRECTION_ASC',
								'cfg_default_sorting_dir'
				);
				?>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_DIRECTION_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_DIRECTION_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_DIRECTION_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[mod_direction]',
								'',
								$cfg['mod_direction'],
								'PJSF_DIRECTION_HORIZONTAL', 'PJSF_DIRECTION_VERTICAL',
								'cfg_mod_direction'
				);
				?>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_DIRECTION_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_BUTTONS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_BUTTONS_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[show_buttons]',
								'',
								(isset($cfg['show_buttons'])) ? $cfg['show_buttons'] : 1,
								'JYES', 'JNO',
								'cfg_show_buttons'
				);
				?>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_BUTTONS_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_SHOW_BLOCK_RESET_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_SHOW_BLOCK_RESET_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
						'select.booleanlist',
						'cfg[show_block_reset]',
						'class="btn-group"',
						$cfg['show_block_reset'],
						'JYES', 'JNO',
						'cfg_show_block_reset'
						);
				?>
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_SHOW_BLOCK_RESET_INFO'); ?></span>
			</div>
		</div>

		<div class="row">
			<div class="column col1">
				<label class="column col1 hasTip" title="<?php echo JText::_('PJSF_CFG_BLOCK_RESET_TEXT_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_BLOCK_RESET_TEXT_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<input class="span" type="text" name="cfg[block_reset_text]" value="<?php echo ($cfg['block_reset_text']) ? $cfg['block_reset_text'] : JText::_('PJSF_CFG_BLOCK_RESET_TEXT_DEFAULT'); ?>" />
			</div>
			<div class="column">
				<span class="comment"><?php echo JText::_('PJSF_CFG_BLOCK_RESET_TEXT_INFO'); ?></span>
			</div>
		</div>


		<div class="row backlight">
			<div><?php echo JText::_('PJSF_TITLE_CFG_BIND'); ?></div>
		</div>

		<div class="row">
			<div class="column col1">
			</div>
			<div class="column col2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_MENUS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_MENUS_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_CATS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_CATS_LABEL'); ?>
				</label>
			</div>
			<div class="column col2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_MANUFACTURERS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_MANUFACTURERS_LABEL'); ?>
				</label>
			</div>			
		</div>
		<div class="row">		
			<div class="column col1"> </div>
			<div class="column col2">
				<select id="cfg_menus" name="cfg[menus][]" autocomplete="off" multiple="multiple" size="10" onchange="syncSelectionHandler(this);">
					<?php
					foreach ($this->menuTree as &$mm) {
						echo '<optgroup label="'.$mm->title.'">';
						echo JHtml::_('select.options', $mm->links, 'value', 'text', $cfg['menus']);
						echo '</optgroup>';
					}
					?>
				</select>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.genericlist',
								$this->catTree,
								'cfg[cats][]',
								'autocomplete="off" multiple="multiple" size="10" onchange="syncSelectionHandler(this);"',
								'category_id',
								'name',
								$cfg['cats'],
								'cfg_cats'
				);
				?>
			</div>
			<div class="column col2">
				<?php
				echo JHtml::_(
								'select.genericlist',
								$this->manufacturers,
								'cfg[manufacturers][]',
								'autocomplete="off" multiple="multiple" size="10" onchange="syncSelectionHandler(this);"',
								'id',
								'name',
								$cfg['manufacturers'],
								'cfg_manufacturers'
				);
				?>
			</div>
		</div>

		<div class="row backlight">
			<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_CONSTRUCTOR_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_CONSTRUCTOR_LABEL'); ?>
			</label>
		</div>

		<div class="row">
			<table class="table table-striped">
				<tbody id="struct_wrap">
				</tbody>
			</table>
		</div>
			
		<input name="task" type="hidden" value="" />
		<input name="id" type="hidden" value="<?php echo $cfg['id']; ?>" />
		<input name="cid" type="hidden" value="<?php echo $cfg['id']; ?>" />
		<input type="hidden" value="1" name="boxchecked">
	</form>
</div>
