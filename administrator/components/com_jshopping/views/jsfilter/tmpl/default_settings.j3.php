<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('stylesheet', 'administrator/components/com_jshopping/css/jsfilter/jsfilter.j3.css');

JHtml::_('behavior.tooltip');
JHtml::_('script', 'administrator/components/com_jshopping/js/jsfilter/jsfilter.js');

$cfg = &$this->cfg;

?>

<script>
	jQuery(function() {
		// Структура списка сортировки
		var fields = <?php echo json_encode((array)$cfg->sort); ?>;
		buildSortList(fields);
	});
</script>


<div id="sample_sort_list" style="display:none;">
	<div class="row-fluid constructor">
		<div class="span">
			<?php
			echo JHtml::_(
				'select.genericlist',
				$this->sort_fields,
				'cfg[sort][]',
				'autocomplete="off"',
				'name',
				'title',
				null,
				'cfg_sort'
			);
			?>
			<a class="btn" href="javascript:void(0);" onclick="var el = addBlock(this.parentNode.parentNode, 'sample_sort_list', 'sort_list_wrap');" title="<?php echo JText::_('PJSF_ADD');?>" >
				<i class="icon-plus-2"></i>
			</a>
			<a class="btn" href="javascript:void(0);" onclick="rmBlock(this.parentNode.parentNode, 'sort_list_wrap')" title="<?php echo JText::_('PJSF_REMOVE');?>">
				<i class="icon-delete"></i>
			</a>
		</div>
	</div>
</div>


<div class="jsp_admin">
    <form class="form-horizontal" action="index.php?option=com_jshopping&controller=jsfilter" method="post" name="adminForm" enctype="multipart/form-data">
		
		<div class="alert alert-info">
			<?php echo JText::_('PJSF_CFG_BASE_CONFIG_TITLE'); ?>
		</div>
		
		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_CONTENT_SELECTOR_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_CONTENT_SELECTOR_LABEL'); ?>:
			</label>
			<input type="text" name="cfg[content_selector]" value="<?php echo ($cfg->content_selector) ? $cfg->content_selector : 'div.jshop' ; ?>">
		</div>
		
		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_CACHE_TIME_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_CACHE_TIME_LABEL'); ?>:
			</label>
			<input type="text" name="cfg[cache_time]" value="<?php echo ($cfg->cache_time) ? (int) $cfg->cache_time : 120; ?>" />
		</div>

		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_HASH_DAYS_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_HASH_DAYS_LABEL'); ?>:
			</label>
			<input type="text" name="cfg[hash_days]" value="<?php echo ($cfg->hash_days) ? (int) $cfg->hash_days : 15; ?>" />
		</div>
		
		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_AUTO_FILTRATION_TIMEOUT_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_AUTO_FILTRATION_TIMEOUT_LABEL'); ?>:
			</label>
			<input type="text" name="cfg[filter_timeout]" value="<?php echo ($cfg->filter_timeout) ? (int) $cfg->filter_timeout : 2000; ?>" />
		</div>

		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_REPLACE_PANEL_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_REPLACE_PANEL_LABEL'); ?>:
			</label>
			<div class="radio btn-group">
				<?php
					echo JHtml::_(
							'select.booleanlist',
							'cfg[replace_panel]',
							'class="btn-group"',
							$cfg->replace_panel,
							'JYES', 'JNO',
							'cfg_replace_panel'
						);
				?>
			</div>
		</div>

		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_SHOW_SORTCOUNT_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_SHOW_SORTCOUNT_LABEL'); ?>:
			</label>
			<div class="radio btn-group">
				<?php
					echo JHtml::_(
							'select.booleanlist',
							'cfg[show_sortcount]',
							'class="btn-group"',
							$cfg->show_sortcount,
							'JYES', 'JNO',
							'cfg_show_sortcount'
						);
				?>
			</div>
		</div>

		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_SORTCOUNT_LIST_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_SORTCOUNT_LIST_LABEL'); ?>:
			</label>
			<input type="text" name="cfg[sortcount_list]" value="<?php echo ($cfg->sortcount_list) ? $cfg->sortcount_list : JText::_('PJSF_CFG_SORTCOUNT_LIST_DEFAULT'); ?>" />
		</div>

		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_STOCK_STATE_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_STOCK_STATE_LABEL'); ?>:
			</label>
			<select name="cfg[stock_state]">
				<option value="0" <?php echo (!$cfg->stock_state) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_STOCK_STATE_NONE'); ?></option>
				<option value="1" <?php echo ($cfg->stock_state == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_STOCK_STATE_ACTIVE'); ?></option>
				<option value="2" <?php echo ($cfg->stock_state == 2) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_STOCK_STATE_INACTIVE'); ?></option>
			</select>
		</div>

		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_PROD_TMPL_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_PROD_TMPL_LABEL'); ?>:
			</label>
			<select name="cfg[prod_tmpl]">
				<option value="category" <?php echo (!$cfg->prod_tmpl || $cfg->prod_tmpl == 'category') ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_PROD_TMPL_CAT'); ?></option>
				<option value="search" <?php echo ($cfg->prod_tmpl == 'search') ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_PROD_TMPL_SEARCH'); ?></option>
			</select>
		</div>

		<div class="row-fluid">
			<label class="span hasTip" title="<?php echo JText::_('PJSF_CFG_ONLOAD_CODE_DESC'); ?>">
				<?php echo JText::_('PJSF_CFG_ONLOAD_CODE_LABEL'); ?>:
			</label>
			<textarea name="cfg[onload_code]"><?php echo $cfg->onload_code; ?></textarea>
		</div>
		
		
		<div class="alert alert-info">
			<?php echo JText::_('PJSF_CFG_SORTING_LABEL'); ?>
		</div>
		
		<div class="row-fluid">
			<div id="sort_list_wrap" class="span"></div>
		</div>


		<div class="row-fluid">
			<input class="btn btn-primary" type="submit" value="<?php echo JText::_('PJSF_SAVE'); ?>" />
			<input name="task" type="hidden" value="save_settings" />
		</div>
	</form>
</div>
