<?php

defined( '_JEXEC' ) or die( 'Restricted access' );


JHtml::_( 'stylesheet', 'administrator/components/com_jshopping/css/jsfilter/jsfilter.css' );

$jsCfg = JSFactory::getConfig();
JHtml::_('script', $jsCfg->live_path.'js/jquery/jquery-'.$jsCfg->load_jquery_version.'.min.js');
JHtml::_('script', $jsCfg->live_path.'js/jquery/jquery-noconflict.js');
JHtml::_('script', 'administrator/components/com_jshopping/js/jsfilter/jsfilter.js');

?>


<div class="width-100">
<fieldset class="adminform long">

	<form id="adminForm" name="adminForm" action="index.php?option=com_jshopping&controller=jsfilter" method="post" enctype="multipart/form-data">
	<table class = "adminlist">
		<thead>
		<tr>
			<th width="20">
				<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
			</th>
			<th>
				<?php echo JText::_('PJSF_LIST_COLUMN_TITLE'); ?>
			</th>
			<th width="250">
				<?php echo JText::_('PJSF_LIST_COLUMN_MODULE'); ?>
			</th>
			<th width="150">
				<?php echo JText::_('PJSF_LIST_COLUMN_MOD_POSITION'); ?>
			</th>
			<th width="50">
				<?php echo JText::_('PJSF_LIST_COLUMN_PUBLISHED'); ?>
			</th>
		</tr>
		</thead>

		<?php
		if ($this->list)
		{
			$num = 0;
			foreach ($this->list as &$cfg)
			{
				$mod = &$this->modules[$cfg->mid];
				echo '<tr>'
						.'<td>'
							.JHtml::_('grid.id', $num++, $cfg->id, false)
						.'</td>'
						.'<td>'
							.'<a href="index.php?option=com_jshopping&controller=jsfilter&layout=edit&id='.$cfg->id.'">'
								.$cfg->name
							.'</a>'
						.'</td>'
						.'<td class="mod_title '.( ($this->modules[$cfg->mid]->published) ? "published" : "unpublished").'">'
							.$mod->title
						.'</td>'
						.'<td class="position '.( ($this->modules[$cfg->mid]->published) ? "published" : "unpublished").'">'
							.$mod->position
						.'</td>'
						.'<td align="center">'
							.'<a href="javascript:void(0);" onclick="publication(\''.$cfg->id.'\', '.( ($cfg->published == 1) ? '0' : '1'  ).')">'
								.'<img id="img_'.$cfg->id.'" src="components/com_jshopping/images/'.( ($cfg->published == 1) ? 'tick.png' : 'publish_x.png'  ).'" style="float:none;" />'
							.'</a>'
						.'</td>'
					.'</tr>';
			}
		}

		echo '<tfoot><tr><td colspan="6">'.$this->pagination->getListFooter().'</td></tr></tfoot>';
		?>
		
    </table>
	
    <input name="task" type="hidden" value="" />
    <input name="boxchecked" type="hidden" value="" />
    
	</form>
	
</fieldset>
</div>

