<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('stylesheet', 'administrator/components/com_jshopping/css/jsfilter/jsfilter.j3.css');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');
JHtml::_('script', 'administrator/components/com_jshopping/js/jsfilter/jsfilter.js');
// JHtml::_('formbehavior.chosen', 'form select:not([multiple="multiple"])');
JHtml::_( 'jquery.ui', array('core', 'sortable') );
JHtml::_('script', 'media/jui/js/jquery.ui.sortable.min.js');


$cfg = &$this->cfg;

?>

<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>

<div id="j-main-container" class="span10">
<script>
	var SF_lockListMenu = [];
	var SF_lockListCats = [];
	var SF_lockListManuf = [];
	var SFTip;
	
	jQuery(function() {
		// Создание объекта всплывающих подсказок для конструктора
		SFTip = new Tips(null, {'fixed': true, 'offset': {'x': 0, 'y': 35}});
		// Структура элементов фильтрации
		fields = <?php echo json_encode( ($cfg['struct']) ? $cfg['struct'] : array() ); ?>;
		buildStruct(fields);
		// Блокировка пунктов меню и категорий
		onChangeModule( jQuery('#cfg_mid').get(0) );

		var opt = {
			axis: 'y',
			cursor: 'move',
			handle: '.sortable-handler',
			items: 'tr.constructor'
		};
		jQuery("#struct_wrap").sortable();
	});
</script>


<?php
// сэмплы для доп.полей
foreach ( $this->fields as &$f ) {
	
	// Доп.настройки для выбранного поля
	echo '<div id="sample_field_'.$f->name.'" style="display:none;">'
			.( ($f->ex_field)
				? '<div rel="'.$f->ex_field[0].'">'.$f->ex_field[1].'</div>'
				: '<input name="cfg[struct][ex_field][]" type="hidden" value="" />'
			)
		.'</div>';
}
foreach ( $this->types as &$t ) {
	// Доп.настройки для выбранного типа элемента
	echo '<div id="sample_type_'.$t->type.'" style="display:none;">'
			.( ($t->ex_type)
				? '<div rel="'.$t->ex_type[0].'">'.$t->ex_type[1].'</div>'
				: '<input name="cfg[struct][ex_type][]" type="hidden" value="" />'
			)
		.'</div>';
}
?>

<!--Шаблон формы выбора значений-->
<div id="popup_sample" style="display:none;">
	<div id="content_wrap" class="jsp_admin">
		<div id="msg"></div>
		
		<div id="loading">
			<img src="/media/system/images/modal/spinner.gif" />
			<span id="info"><?php echo JText::_('PJSF_VALUES_LOADING'); ?></span>
		</div>
		
		<div id="content">
			<h3><?php echo JText::_('PJSF_TITLE_VALUES_WINDOW'); ?></h3>
		
			<div class="alert alert-info">
				<span class="span2">
					<strong><?php echo JText::_('PJSF_CFG_VAL_MODE_LABEL'); ?></strong>
				</span>
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_MODE_DESC'); ?></span>
			</div>

			<div class="row-fluid">
				<label class="radio span2 offset1">
					<input type="radio" name="val[mode]" onchange="valModeHandler(this);" value="0" />
					<?php echo JText::_('PJSF_CFG_VAL_MODE_ALL'); ?>
				</label>

					<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_MODE_ALL_DESC'); ?></span>

			</div>

			<div class="row-fluid">
				<label class="radio span2 offset1">
					<input type="radio" name="val[mode]" onchange="valModeHandler(this);" value="1" />
					<?php echo JText::_('PJSF_CFG_VAL_MODE_FIXED'); ?>
				</label>
					<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_MODE_FIXED_DESC'); ?></span>
			</div>

			<div class="row-fluid">
				<label class="radio span2 offset1">
					<input type="radio" name="val[mode]" onchange="valModeHandler(this);" value="2" />
					<?php echo JText::_('PJSF_CFG_VAL_MODE_DYNAMIC'); ?>
				</label>
					<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_MODE_DYN_DESC'); ?></span>
			</div>

			<div id="row_auto_sel">
				<div class="alert alert-info">
					<span class="span2">
						<strong><?php echo JText::_('PJSF_CFG_VAL_AUTO_SELECTION_LABEL'); ?></strong>
					</span>
						<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_AUTO_SELECTION_DESC'); ?></span>
					</dl>
				</div>	
			
				<div class="row-fluid">			
						<label class="radio span2 offset1">
							<input type="radio" name="val[selection]" onchange="valAutoSelectionHandler(this);" value="0" />
							<?php echo JText::_('JNO'); ?>
						</label>
						<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_AUTO_SELECTION_FIXED'); ?></span>
				</div>	
				<div class="row-fluid">
						<label class="radio span2 offset1">
							<input type="radio" name="val[selection]" onchange="valAutoSelectionHandler(this);" value="1" />
							<?php echo JText::_('JYES'); ?>
						</label>
						<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_AUTO_SELECTION_DYNAMIC'); ?></span>
				</div>
			</div>

			<div class="alert alert-info">
			    <span class="span2">
					<strong><?php echo JText::_('PJSF_CFG_VAL_LOGIC_LABEL'); ?></strong>
				</span>
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_LOGIC_DESC'); ?></span>
			</div>

			<div class="row-fluid">			
				<label class="radio span2 offset1">
					<input type="radio" name="val[logic]" value="0" />
					<?php echo JText::_('PJSF_CFG_VAL_LOGIC_OR'); ?>
				</label>
					<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_LOGIC_OR_DESC'); ?></span>
			</div>

			<div class="row-fluid">			
				<label class="radio span2 offset1">
					<input type="radio" name="val[logic]" value="1" />
					<?php echo JText::_('PJSF_CFG_VAL_LOGIC_AND'); ?>
				</label>
					<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_LOGIC_AND_DESC'); ?></span>
			</div>
			
			<div class="alert alert-info">
			    <span class="span2">
					<strong><?php echo JText::_('PJSF_CFG_VAL_SORT_LABEL'); ?></strong>
				</span>
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_SORT_DESCRIPTION'); ?></span>
			</div>

			<div class="row-fluid">			
				<label class="radio span2 offset1">
					<input type="radio" name="val[sort]" value="0" />
					<?php echo JText::_('PJSF_CFG_VAL_SORT_DEFAULT'); ?>
				</label>
					<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_SORT_DEFAULT_DESC'); ?></span>
			</div>

			<div class="row-fluid">			
				<label class="radio span2 offset1">
					<input type="radio" name="val[sort]" value="1" />
					<?php echo JText::_('PJSF_CFG_VAL_SORT_ASC'); ?>
				</label>
					<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_SORT_ASC_DESC'); ?></span>
			</div>

			<div class="row-fluid">			
				<label class="radio span2 offset1">
					<input type="radio" name="val[sort]" value="2" />
					<?php echo JText::_('PJSF_CFG_VAL_SORT_DESC'); ?>
				</label>
					<span class="help-inline"><?php echo JText::_('PJSF_CFG_VAL_SORT_DESC_DESC'); ?></span>
			</div>

			
			<div id="type_select_opt" style="display:none;">
			<div class="alert alert-info">
			    <span class="span2">
					<strong><?php echo JText::_('PJSF_CFG_SELECT_HIDE_DISABLED_LABEL'); ?></strong>
				</span>
					<span class="comment"><?php echo JText::_('PJSF_CFG_SELECT_HIDE_DISABLED_DESC'); ?></span>
			</div>
			<div class="row-fluid">			
				<label class="radio span2 offset1">
					<input type="radio" name="val[select_hide_disabled]" value="0" />
					<?php echo JText::_('JNO'); ?>
				</label>
					<span class="help-inline"><?php echo JText::_('PJSF_CFG_SELECT_HIDE_DISABLED_NO_DESC'); ?></span>
			</div>
			<div class="row-fluid">			
				<label class="radio span2 offset1">
					<input type="radio" name="val[select_hide_disabled]" value="1" />
					<?php echo JText::_('JYES'); ?>
				</label>
					<span class="help-inline"><?php echo JText::_('PJSF_CFG_SELECT_HIDE_DISABLED_YES_DESC'); ?></span>
			</div>
			</div>


			<div id="row_values">
				<div class="alert alert-info">
				    <span class="span2">
						<strong><?php echo JText::_('PJSF_CFG_VAL_SELECT_LABEL'); ?></strong>
					</span>
						<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_SELECT_TITLE'); ?></span>
					</dl>
				</div>
				
				<div id="values" class="row-fluid">
					<div class="val_item row-fluid">
						<div class="val_title span2 offset1">&nbsp;</div>
						<div class="val_select span2">
							<button onclick="toggleCheckValues(this, 'val_select');" rel="0">
								<?php echo JText::_('PJSF_CFG_VAL_TOGLE_BUTTON'); ?>
							</button>
						</div>
						<div class="val_popular span2">
							<button onclick="toggleCheckValues(this, 'val_popular');" rel="0">
								<?php echo JText::_('PJSF_CFG_VAL_TOGLE_BUTTON'); ?>
							</button>
						</div>
					</div>
				</div>
			</div>

			<div class="alert alert-info">
			    <span class="span2">
					<strong><?php echo JText::_('PJSF_CFG_VAL_COMMENT_LABEL'); ?></strong>
				</span>
					<span class="comment"><?php echo JText::_('PJSF_CFG_VAL_COMMENT_DESC'); ?></span>
			</div>

			<div class="row-fluid">			
				<label class="radio span2 offset1">
					<button id="commentBtn" class="btn"><?php echo JText::_('PJSF_CFG_VAL_COMMENT_COPY'); ?></button>
				</label>
					<span class="help-inline">
						<textarea name="comment"></textarea>
					</span>
			</div>
		
		</div>
	</div>
</div>

<div id="values_sample" style="display:none;">
	<div class="val_item row-fluid">
		<div class="val_title span2 offset1"></div>
		<div class="val_select span2">
			<input name="val[list]" type="checkbox" value="" />
		</div>
		<div class="val_popular span2">
			<input name="val[popular]" type="checkbox" value="" />
		</div>
	</div>
</div>


 <!-- Конструктор (блоки) -->
<table style="display:none;">
<tbody id="sample">
	<tr class="constructor">
	<td class="move">
		<span class="sortable-handler">
			<i class="icon-menu"></i>
		</span>
	</td>
	<td class="const-td">
		<div class="">
			<?php
			echo JHtml::_(
				'select.genericlist',
				$this->fields,
				'cfg[struct][field][]',
				'class="span" onchange="addEx(this, \'field\'); fieldHandler(this);"',
				'name',
				'title',
				0,
				'sfield'
			);
			?>
		</div>
	</td>
	<td class="const-td">
		<div class="">
			<?php
			echo JHtml::_(
				'select.genericlist',
				$this->types,
				'cfg[struct][type][]',
				'class="span" onchange="addEx(this, \'type\')"',
				'type',
				'title',
				0,
				'stype'
			);
			?>
		</div>
	</td>
	<td class="const-td">
		<div class="sfTip" rel="<?php echo JText::_('PJSF_BLOCK_TYPE_TIP'); ?>">
			<select class="span" id="b_mode" name="cfg[struct][b_mode][]">
				<option value="0"><?php echo JText::_('PJSF_UNCOLLAPSED'); ?></option>
				<option value="1"><?php echo JText::_('PJSF_COLLAPSED'); ?></option>
				<option value="4"><?php echo JText::_('PJSF_DONT_COLLAPSE'); ?></option>
				<option value="2"><?php echo JText::_('PJSF_WITHOUT_TITLE'); ?></option>
				<option value="3"><?php echo JText::_('PJSF_POPULAR'); ?></option>
			</select>
		</div>
	</td>
	<td class="const-limit">
		<div class="sfTip" rel="<?php echo JText::_('PJSF_BLOCK_LIMIT_TIP'); ?>">
			<input class="span" type="text" id="b_limit" name="cfg[struct][b_limit][]" value="" />
		</div>
	</td>
	<td class="const-td">
		<div class="sfTip" rel="<?php echo JText::_('PJSF_LABEL_TIP'); ?>">
			<input class="span" type="text" id="slabel" name="cfg[struct][label][]" value="<?php echo JText::_('PJSF_LABEL'); ?>" onfocus="if (this.value=='<?php echo JText::_('PJSF_LABEL'); ?>') this.value='';" onblur="if (this.value=='') this.value='<?php echo JText::_('PJSF_LABEL'); ?>';" />
		</div>
	</td>
	<td class="const-td">	
		<div id="ex_field" class="">
		</div>
	</td>
	<td class="const-ex-type">
		<div id="ex_type" class="">	
		</div>
	</td>

	<td class="const-btn">	
			<input type="hidden" id="values" name="cfg[struct][values][]" value="val%5Bmode%5D=2&val%5Bselection%5D=1&val%5Blogic%5D=0&val%5Bsort%5D=0&val%5Bselect_hide_disabled%5D=0" />
			<input type="hidden" id="comment" name="cfg[struct][comment][]" value="" />
		<div class="">
			<a class="btn button-add" href="javascript:void(0);" onclick="var el = addBlock(jQuery(this).closest('.constructor').get(0), 'sample', 'struct_wrap'); buildExFields(el);" title="<?php echo JText::_('PJSF_ADD');?>" >
				<i class="icon-plus-2"></i>
			</a>
			<a class="btn button-val" href="javascript:void(0);" onclick="selectValues( jQuery(this).closest('.constructor') );" title="<?php echo JText::_('PJSF_BTN_VALUES');?>" >
				<i class="icon-cog"></i>
			</a>
			<a class="btn button-rem" href="javascript:void(0);" onclick="rmBlock(jQuery(this).closest('.constructor').get(0), 'struct_wrap')" title="<?php echo JText::_('PJSF_REMOVE');?>">
				<i class="icon-delete"></i>
			</a>
		</div>
	</td>
	</tr>
</tbody>
</table>


<div class="jsp_admin row-fluid">
    <form class="form-horizontal" action="index.php?option=com_jshopping&controller=jsfilter" method="post" id="adminForm" name="adminForm" enctype="multipart/form-data">

		<div class="alert alert-info">
			<h4><?php echo JText::_('PJSF_TITLE_GENERIC_PARAMS'); ?></h4>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_NAME_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_NAME_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<input class="span" type="text" name="cfg[name]" value="<?php echo $cfg['name']; ?>" />
			</div>
			<div class="span8">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_NAME_INFO'); ?></span>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_TITLE_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_TITLE_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<input class="span" type="text" name="cfg[title]" value="<?php echo $cfg['title']; ?>" />
			</div>
			<div class="span8">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_TITLE_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_MODULE_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_MODULE_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<?php
				echo JHtml::_(
								'select.genericlist',
								$this->modules,
								'cfg[mid]',
								'class="span" autocomplete="off" onchange="onChangeModule(this)"',
								'id',
								'title',
								$cfg['mid'],
								'cfg_mid'
				);
				?>
			</div>
			<div class="span8">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_MODULE_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_LAYOUTS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_LAYOUTS_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<select class="span" name="cfg[layout]">
				<?php
				foreach ($this->layouts as &$layout) {
					echo '<option value="'.$layout.'" '.(($cfg['layout'] == $layout) ? 'selected="selected"' : '').'>'.$layout.'</option>';
				}
				?>
				</select>
			</div>
			<div class="span8">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_LAYOUTS_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_FILTER_MODE_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_FILTER_MODE_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<select name="cfg[deactivate_values]" class="span" autocomplete="off">
					<option value="0" <?php echo (!(int) $cfg['deactivate_values']) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_FILTER_MODE_CLASSIC');?></option>
					<option value="1" <?php echo ($cfg['deactivate_values'] == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_FILTER_MODE_DEACTIVATE');?></option>
					<option value="2" <?php echo ($cfg['deactivate_values'] == 2) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_FILTER_MODE_STEP_BY_STEP');?></option>
				</select>
			</div>
			<div class="span6">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_DEACTIVATE_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_TIP_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_TIP_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<select name="cfg[show_tip]" class="span" autocomplete="off">
					<option value="0" <?php echo (!(int) $cfg['show_tip']) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_TIP_NONE');?></option>
					<option value="1" <?php echo ($cfg['show_tip'] == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_TIP_SHOW');?></option>
					<option value="2" <?php echo ($cfg['show_tip'] == 2) ? 'selected="selected"' : ''; ?>><?php echo JText::_('PJSF_CFG_TIP_AUTO');?></option>
				</select>
			</div>
			<div class="span6">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_TIP_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<?php
				echo JHtml::_(
								'select.genericlist',
								$this->sort_fields,
								'cfg[default_sorting]',
								'class="span" autocomplete="off"',
								'name',
								'title',
								$cfg['default_sorting'],
								'cfg_default_sorting'
				);
				?>
			</div>
			<div class="span8">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_DIRECTION_DESCRIPTION'); ?>">
					<?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_DIRECTION_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<div class="radio btn-group">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[default_sorting_dir]',
								'class="btn-group"',
								$cfg['default_sorting_dir'],
								'PJSF_CFG_DEFAULT_SORTING_DIRECTION_DESC', 'PJSF_CFG_DEFAULT_SORTING_DIRECTION_ASC',
								'cfg_default_sorting_dir'
				);
				?>
				</div>
			</div>
			<div class="span6">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_DEFAULT_SORTING_DIRECTION_INFO'); ?></span>
			</div>
		</div>


		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_PUBLISHED_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_PUBLISHED_LABEL'); ?>
				</label>
			</div>	
			
			<div class="span2">
			<div class="radio btn-group">
				<?php
					echo JHtml::_(
						'select.booleanlist',
						'cfg[published]',
						'class="btn-group"',
						$cfg['published'],
						'JYES', 'JNO',
						'cfg_published'
						);
				?>
			</div>
			</div>
			<div class="span8">
					<span class="control-label"><?php echo JText::_('PJSF_CFG_PUBLISHED_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_SEARCH_IN_SUBCAT_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_SEARCH_IN_SUBCAT_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<div class="radio btn-group">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[in_subcat]',
								'class="btn-group"',
								$cfg['in_subcat'],
								'JYES', 'JNO',
								'cfg_in_subcat'
				);
				?>
				</div>
			</div>
			<div class="span8">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_SEARCH_IN_SUBCAT_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_SHOW_IN_PROD_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_SHOW_IN_PROD_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<div class="radio btn-group">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[in_prod]',
								'class="btn-group"',
								$cfg['in_prod'],
								'JYES', 'JNO',
								'cfg_in_prod'
				);
				?>
				</div>
			</div>
			<div class="span6">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_SHOW_IN_PROD_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_BUTTONS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_BUTTONS_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<div class="radio btn-group">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[show_buttons]',
								'class="btn-group"',
								(isset($cfg['show_buttons'])) ? $cfg['show_buttons'] : 1,
								'JYES', 'JNO',
								'cfg_show_buttons'
				);
				?>
				</div>
			</div>
			<div class="span6">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_BUTTONS_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_SHOW_BLOCK_RESET_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_SHOW_BLOCK_RESET_LABEL'); ?>
				</label>
			</div>	
			
			<div class="span2">
			<div class="radio btn-group">
				<?php
					echo JHtml::_(
						'select.booleanlist',
						'cfg[show_block_reset]',
						'class="btn-group"',
						$cfg['show_block_reset'],
						'JYES', 'JNO',
						'cfg_show_block_reset'
						);
				?>
			</div>
			</div>
			<div class="span8">
					<span class="control-label"><?php echo JText::_('PJSF_CFG_SHOW_BLOCK_RESET_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_BLOCK_RESET_TEXT_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_BLOCK_RESET_TEXT_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<input class="span" type="text" name="cfg[block_reset_text]" value="<?php echo ($cfg['block_reset_text']) ? $cfg['block_reset_text'] : JText::_('PJSF_CFG_BLOCK_RESET_TEXT_DEFAULT'); ?>" />
			</div>
			<div class="span8">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_BLOCK_RESET_TEXT_INFO'); ?></span>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_DIRECTION_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_DIRECTION_LABEL'); ?>
				</label>
			</div>
			<div class="span2">
				<div class="radio btn-group">
				<?php
				echo JHtml::_(
								'select.booleanlist',
								'cfg[mod_direction]',
								'class="btn-group"',
								$cfg['mod_direction'],
								'PJSF_DIRECTION_HORIZONTAL', 'PJSF_DIRECTION_VERTICAL',
								'cfg_mod_direction'
				);
				?>
				</div>
			</div>
			<div class="span6">
				<span class="control-label"><?php echo JText::_('PJSF_CFG_DIRECTION_INFO'); ?></span>
			</div>
		</div>
		

		<div class="alert alert-info">
			<h4><?php echo JText::_('PJSF_TITLE_CFG_BIND'); ?></h4>
		</div>

		<div class="row-fluid">
			<div class="span2"> </div>
			<div class="span3">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_MENUS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_MENUS_LABEL'); ?>
				</label>
			</div>
			<div class="span3">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_CATS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_CATS_LABEL'); ?>
				</label>
			</div>
			<div class="span3">
				<label class="control-label hasTip" title="<?php echo JText::_('PJSF_CFG_MANUFACTURERS_DESC'); ?>">
					<?php echo JText::_('PJSF_CFG_MANUFACTURERS_LABEL'); ?>
				</label>
			</div>
		</div>
		
		<div class="row-fluid">		
			<div class="span2">
			</div>
			<div class="span3">
				<select class="span" id="cfg_menus" name="cfg[menus][]" autocomplete="off" multiple="multiple" size="10" onchange="syncSelectionHandler(this);">
					<?php
					foreach ($this->menuTree as &$mm) {
						echo '<optgroup label="'.$mm->title.'">';
						echo JHtml::_('select.options', $mm->links, 'value', 'text', $cfg['menus']);
						echo '</optgroup>';
					}
					?>
				</select>
			</div>
			<div class="span3">
				<?php
				echo JHtml::_(
								'select.genericlist',
								$this->catTree,
								'cfg[cats][]',
								'class="span" autocomplete="off" multiple="multiple" size="10" onchange="syncSelectionHandler(this);"',
								'category_id',
								'name',
								$cfg['cats'],
								'cfg_cats'
				);
				?>
			</div>
			<div class="span3">
				<?php
				echo JHtml::_(
								'select.genericlist',
								$this->manufacturers,
								'cfg[manufacturers][]',
								'class="span" autocomplete="off" multiple="multiple" size="10" onchange="syncSelectionHandler(this);"',
								'id',
								'name',
								$cfg['manufacturers'],
								'cfg_manufacturers'
				);
				?>
			</div>
		</div>

		<div class="alert alert-info">
			<h4><?php echo JText::_('PJSF_CFG_CONSTRUCTOR_LABEL'); ?></h4>
		</div>

		<div class="row-fluid">
			<table id="constructor_table" class="table table-striped">
				<tbody id="struct_wrap">
				</tbody>
			</table>
		</div>
			
		<input name="task" type="hidden" value="" />
		<input name="id" type="hidden" value="<?php echo $cfg['id']; ?>" />
		<input name="cid" type="hidden" value="<?php echo $cfg['id']; ?>" />
		<input type="hidden" value="1" name="boxchecked">

	</form>
</div>
</div>
