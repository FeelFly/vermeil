<?php

defined( '_JEXEC' ) or die( 'Restricted access' );
if ( !defined('DS') ) define('DS', DIRECTORY_SEPARATOR);


jimport( 'joomla.application.component.view');


if (IS_J2x) {
	class JshoppingViewJsfilterBase extends JView {}
} else {
	class JshoppingViewJsfilterBase extends JViewLegacy {}
}


class JshoppingViewJsfilter extends JshoppingViewJsfilterBase
{
	
    public function display ($tmpl = null)
    {
		$app = JFactory::getApplication();
		$error = "";

		// Панель инструментов
		JToolBarHelper::title( JText::_('PJSF_TITLE') );
		addSubmenu("other");

		if ( class_exists(JHtmlSidebar) )
        {
        	$sidebar = JHtmlSidebar::render();
        } else {
        	$sidebar = null;
        }

		
		// Проверка наличия обработчика
		if ( $tmpl && method_exists($this, "display_".$tmpl) ) {
			// Вызов обработчика, формирующего данные для текущего вида
			$error = $this->{"display_".$tmpl}();
			
		} else {
			// Вывод списка доступных модулей и конфигураций фильтра
			$this->_display();
		}

		
		$bar = JToolBar::getInstance('toolbar');
		// Кнопка общих настроек
		$bar->appendButton(
							'Popup',
							'options',
							JText::_('PJSF_SETTINGS_BUTTON'),
							'index.php?option=com_jshopping&amp;controller=jsfilter&amp;layout=settings&amp;tmpl=component',
							875, 400, 0, 0,
							'');
		
		if ($error) {
			$app->enqueueMessage($error, 'error');
		}

        if (!IS_J2x) {
			if ($tmpl) {
				$tmpl .= '.j3';
			} else {
				$this->setLayout('default.j3');
			}
		}

		$doc = JFactory::getDocument();
		$doc->addScriptDeclaration( "IS_J2x = ".((IS_J2x) ? 'true' : 'false').";" );

		
		$this->assignRef('sidebar', $sidebar);


		parent::display($tmpl);

        // Вывод версии дополнения
		$xml = JFactory::getXML(JPATH_ADMINISTRATOR.'/manifests/packages/pkg_jsfilter.xml');
		echo '<center>'
					.'<a href="http://vv.ru" target="_blank">'
						.'JoomShopping Smart Filter'
					.'</a>'
					.' '
					.JText::_('PJSF_VERSION_LABEL').$xml->version
			.'</center>';
	}


	// ========================================================================
	// 			Вывод списка доступных модулей и конфигураций фильтра
	// ========================================================================
	
	private function _display ()
	{
		// Вывод списка доступных модулей фильтра
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();
		$tmpl = null;

		$context	= "jshoping.jsfilter";
		$limit		= $app->getUserStateFromRequest($context.'.limit', 'limit', $app->getCfg('list_limit'), 'int');
		$limitstart = $app->getUserStateFromRequest($context.'.limitstart', 'limitstart', 0, 'int');

		// Проверка размера конфигурации и формата поля в БД
		$msg = JsfilterHelper::checkConfigSize();
		if ($msg)
		{
			$app->enqueueMessage($msg, 'error');
		}

		// Получение списка модулей jsfilter
		$query = "SELECT *
				FROM `#__modules`
				WHERE
					`module` = 'mod_jsfilter'";
		$db->setQuery($query);
		$modules = $db->loadObjectList('id');

		// Формирование списка конфигураций
		$cfgList = JsfilterHelper::getConfigList();
		$totalConfigs = count($cfgList);
		$list = array_slice($cfgList, $limitstart, $limit);

		if (!$list) {
			$limitstart = 0;
			$app->setUserState($context.'.limitstart', 0);
			$list = array_slice($cfgList, 0, $limit);
		}
		
		foreach ($list as $k => &$cfg) {
			$list[$k] = JArrayHelper::toObject($cfg);
		}

		// Сортировка списка конфигураций
		if ($totalConfigs) {
			usort( $list, array($this, 'configsCmp') );
		} else {
			$app->enqueueMessage(JText::_('PJSF_CONFIGS_NOT_FOUND'), 'notice');
		}

		// Страничный вывод
		jimport('joomla.html.pagination');
		$pagination = new JPagination($totalConfigs, $limitstart, $limit);

		// Кнопки на панели управления
		JToolBarHelper::addNew();
		// Кнопка копирования конфигураций
		$bar = JToolBar::getInstance('toolbar');
		$bar->appendButton(
							'Standard',
							'save-copy',
							JText::_('PJSF_COPY_BUTTON'),
							'cfg_copy',
							false);
		JToolBarHelper::deleteList();

		$this->assignRef('list', 		$list);
		$this->assignRef('modules', 	$modules);
		$this->assignRef('pagination', 	$pagination);
	}


	// ========================================================================
	// 			Функция сравнения при сортировке списка конфигураций
	// ========================================================================

	public function configsCmp ($a, $b)
	{
		return strcmp($a->title, $b->title);
	}


	// ========================================================================
	// 						Лицензирование
	// ========================================================================
	
	private function display_license ()
	{
		$isLocked = JRequest::getInt('lock_app');
		$this->assign('isLocked', $isLocked);
	}
	

	// ========================================================================
	// 					Настройки конфигурации фильтра
	// ========================================================================
	
	private function display_edit ()
	{
		$app	= JFactory::getApplication();
		$db 	= JFactory::getDbo();
		$id		= JRequest::getInt('id', 0);


		// Проверка размера конфигурации и формата поля в БД
		$msg = JsfilterHelper::checkConfigSize();
		if ($msg)
		{
			$app->enqueueMessage($msg, 'error');
		}
		
		
		// Формирование списка модулей фильтров
		$query = "SELECT
						`id`,
						`title`
				FROM `#__modules`
				WHERE
					`module` = 'mod_jsfilter'";
		$db->setQuery($query);
		$modules = $db->loadObjectList();

		if (!$modules) return JText::_('PJSF_MODULES_NOT_FOUND');

		// Список пунктов меню Joomla
		require_once(JPATH_ADMINISTRATOR.'/components/com_menus/helpers/menus.php');
		$menuTree = MenusHelper::getMenuLinks();

		// Список категорий Joomshopping
		$catTree = buildTreeCategory(0);

		// Список производителей
		$manufacturers = JSFactory::getAllManufacturer();

		// Загрузка конфигурации
		$cfg = JsfilterHelper::getConfig($id);
		
		if ($id && !$cfg) {
			$app->enqueueMessage(JText::_('PJSF_CONFIG_INDEX_ERROR'), 'error');
		}

		// Создание минимального набора параметров при создании новой конфигурации
		if ( !$cfg ) {
			$cfg = array();
			$cfg['id'] = 0;
			$cfg['name'] = JText::_('PJSF_CFG_DEFAULT_TITLE');
			$cfg['published'] = 1;
			$cfg['values']['mode'] = 0;
			$cfg['values']['selection'] = 1;
		}

		// Кодирование параметров для блока выбранных значений
		foreach ($cfg['struct'] as $i => &$row) {

			if ($row['values']['list']) {
				$row['values']['list'] = array_combine($row['values']['list'], $row['values']['list']);
			} else {
				$row['values']['list'] = array();
			}

			if ($row['values']['popular']) {
				$row['values']['popular'] = array_combine($row['values']['popular'], $row['values']['popular']);
			} else {
				$row['values']['popular'] = array();
			}

			$row['values']['sort'] = (int) $row['values']['sort'];
			$row['values']['select_hide_disabled'] = (int) $row['values']['select_hide_disabled'];
			
			// Формирование итоговой строки параметров
			$row['values'] = http_build_query( array('val' => $row['values']) );
		}

		// Формирование перечней полей фильтрации, типов полей и шаблонов
		require_once(JPATH_ROOT.'/modules/mod_jsfilter/helper/helper.php');
		$helper	= new JsfilterHelper();
		$types	= $helper->getAllTypes();
		$fields	= $helper->getAllFields();
		$layouts= $helper->getAllLayouts();

		// Параметр сортировки по умолчанию
		$sort_fields = $helper->getAllSortFields();
		if ( !$cfg['default_sorting'] )
		{
			reset($sort_fields);
			$cfg['default_sorting'] = key($sort_fields);
		}


		// Панель инструментов
		$title = ($mid) ? JText::_('PJSF_CONFIG_EDIT_TITLE') : JText::_('PJSF_CONFIG_NEW_TITLE');
		JToolBarHelper::title($title);
		JToolBarHelper::save();
		JToolBarHelper::apply();
		JToolBarHelper::cancel();
		// Кнопка копирования конфигураций
		$bar = JToolBar::getInstance('toolbar');
		$bar->appendButton(
							'Standard',
							'save-copy',
							JText::_('PJSF_COPY_BUTTON'),
							'cfg_copy',
							false);
		if ($mid) {
			JToolBarHelper::deleteList();
		}

        $this->assignRef('modules',			$modules);
        $this->assignRef('cfg',				$cfg);
        $this->assignRef('types', 			$types);
        $this->assignRef('fields', 			$fields);
        $this->assignRef('layouts', 		$layouts);
        $this->assignRef('menuTree', 		$menuTree);
        $this->assignRef('catTree', 		$catTree);
        $this->assignRef('manufacturers', 	$manufacturers);
        $this->assignRef('lockListMenu',	$lockListMenu);
        $this->assignRef('lockListCats',	$lockListCats);
        $this->assignRef('sort_fields', 	$sort_fields);
	}


	// ========================================================================
	// 					Общие параметры конфигураций фильтров
	// ========================================================================
	
	private function display_settings ()
	{
		// Получение общих параметров конфигураций
		// хранятся в параметрах модуля фильтра (в extensions)
		$table = JTable::getInstance('extension');
		$table->load( array('type' => 'module', 'element' => 'mod_jsfilter') );
		$params	= json_decode($table->params);

		if ($params && $params->cfg) {
			$cfg = &$params->cfg;
		} else {
			$cfg = new stdClass;
		}
		
		require_once(JPATH_ROOT.'/modules/mod_jsfilter/helper/helper.php');
		$helper	= new JsfilterHelper();

		// Поля сортировки
		$sort_fields = $helper->getAllSortFields();
		$dummy = array(
			'name'	=> '',
			'title'	=> JText::_('PJSF_SELECT_VALUE')
		);
		array_unshift($sort_fields, $dummy);


		$this->assignRef('cfg', 			$cfg);
		$this->assignRef('sort_fields', 	$sort_fields);
	}
    
}
?>
