
function publication (id, state)
{
	var url = jQuery('form[name="adminForm"]').attr('action');

	jQuery.getJSON( 
		url, 
		{
			'task'	: 'publish',
			'id'	: id,
			'state'	: state
		},
		function (data, status) {
			if (status != 'success') return;

			if (!data.status) {
				alert(data.message);
				return;
			}

			var img	= jQuery('#img_'+id);
			var src = 'components/com_jshopping/images/';

			if (state) {
				src += 'tick.png';
			} else {
				src += 'publish_x.png';
			}
			img.attr('src', src);
			img.closest('a').attr('onclick', 'publication(\''+id+'\', '+((state) ? 0 : 1)+')');
		}
	);
}


// ==================================================================
// 					Параметры модуля
// ==================================================================
var constrId = 0;

function addBlock (node, sid, wrap_id)
{
	var wrap	= jQuery('#'+wrap_id);
	var sample	= jQuery('#'+sid).get(0).children[0];
	var new_el	= jQuery(sample).clone();

	if (node) {
		new_el.insertAfter(node);
	} else {
		new_el.appendTo(wrap);
	}

	// Замена select'ов для бутстрапа
	// if (!IS_J2x) {
		// new_el.find("select").each(function() {
			// var el = jQuery(this);
			// var id = el.attr('id');
			// el.attr('id', id+"_"+constrId);
			// constrId++;
			// el.chosen({
				// disable_search_threshold : 10,
				// allow_single_deselect : true
			// });
		// });
	// }

	// Создание подсказок
	new_el.find('.sfTip').each(function() {
		SFTip.attach(this);
	});

	return new_el;
}


function rmBlock (node, wrap_id)
{
	if (!node) return;

	var wrap = jQuery('#'+wrap_id);
	if (wrap.children().length > 1) {
		// Удаление подсказок
		jQuery(node).find('.sfTip').each(function() {
			SFTip.detach(this);
		});
		// Удаление блока
		node.remove();
	}
}


function addEx (el, block)
{
	if (!el) return;

	var wrap 	= jQuery(el).closest('.constructor').find('#ex_'+block);
	var field	= el.value;
	var sample	= jQuery('#sample_'+block+'_'+field).get(0);

	if ( !sample || !wrap ) return;

	// Удаление подсказки
	SFTip.detach( wrap.get(0).children[0] );
	
	wrap.empty();

	var src		= sample.children[0];
	var new_el	= jQuery(src).clone();

	new_el.appendTo(wrap);

	// Добавление обработчика смены значения
	var ex = new_el.find("select");
	if (ex) {
		ex.attr('onchange', "fieldHandler(this)");
	}

	// Обновление всплывающей посказки
	var text = new_el.attr('rel');
	if (text) {
		SFTip.attach(new_el.get(0));
	}
}


function buildStruct (list)
{
	var wrap = jQuery('#struct_wrap');

	if ( !list || !list.length ) {
		var el = addBlock(null, 'sample', 'struct_wrap');
		buildExFields(el);
		return;
	}

	var last_el	= null;
	jQuery(list).each(function() {
		last_el = addBlock(last_el, 'sample', 'struct_wrap');

		last_el.find('#stype').val(this.type);
		last_el.find('#sfield').val(this.field);
		last_el.find('#slabel').val(this.label);
		last_el.find('#b_mode').val(this.b_mode);
		last_el.find('#b_limit').val(this.b_limit);
		last_el.find('#values').val(this.values);
		last_el.find('#comment').val(this.b_comment);

		buildExFields(last_el, this);
	});
}


function buildExFields (row, params)
{
	if (!params) params = {};
	
	var input;

	// Поле
	addEx(row.find('#sfield').get(0), 'field');
	input = row.find('#struct_ex_field');
	if (input) {
		input.val( (params.ex_field) ? params.ex_field : '' );
	}

	// Тип
	addEx(row.find('#stype').get(0), 'type');
	input = row.find('#struct_ex_type');
	if (input) {
		input.val( (params.ex_type) ? params.ex_type : '' );
	}
}
	

function buildSortList (list)
{
	var wrap = jQuery('#sort_list_wrap');

	if ( !list || !list.length ) {
		addBlock(null, 'sample_sort_list', 'sort_list_wrap');
		return;
	}

	var last_el	= null;
	for (i = 0; i < list.length; i++) {
		last_el = addBlock(last_el, 'sample_sort_list', 'sort_list_wrap');
		jQuery(last_el).find('#cfg_sort').val(list[i]);
	}
}


function selectValues (wrap)
{
	if (!wrap) return;
	wrap = jQuery(wrap);
	
	// Создание модального окна для выбора значений
	var sample = jQuery('#popup_sample').children(0);
	var tmpl = sample.clone();
	var content = tmpl.find('#content');

	// Формирование данных о типе выбранного поля (для получения описания)
	var fType = wrap.find('#sfield').val();
	var fElement = wrap.find('#stype').val();
	var exField = wrap.find('#struct_ex_field');
	var targetButton = content.find('#commentBtn');
	var textArea = content.find('textarea[name="comment"]');

	// Управление показом опции для списков
	if (fElement == 'select' || fElement == 'multiselect')
	{
		content.find('#type_select_opt').show();
	}


	if (fType == 'attr' || fType == 'efield')
	{
		var exVal = (exField.length) ? exField.val() : '';
		
		
		targetButton.on('click', function() {
			getBlockComment(textArea, fType, exVal);
		});
		targetButton.show();
	} else {
		targetButton.hide();
	}

	// Формирование значения описания
	var currComment = wrap.find('#comment');
	var textAreaVal = (currComment.length) ? currComment.val() : '';

	content.hide();

	SqueezeBox.open(
		tmpl.get(0),
		{
			handler	: 'adopt',
			size	: {x: 1024, y: 600},
			onClose	: function() {
				// Сохранение значений в строке конструктора
				var target = wrap.find('#values');

				if (target.length) {
					var data = content.find('input, select');
					target.val( data.serialize() );
				}

				// Сохранение комментария
				target = wrap.find('#comment');
				if (target) {
					var el = content.find('textarea[name="comment"]');
					data = el.text( el.val() ).html().replace(/\"/g, '&quot;');
					target.val(data);
				}
			}
		}
	);

	// Получение списка данных
	var cfg = wrap.find('input, select');
	
	jQuery.post( 
		'index.php?option=com_jshopping&controller=jsfilter&task=get_values', 
		cfg,
		function (data, status) {
			if (status != 'success') return;

			if (!data.status) {
				tmpl.find('#msg').html(data.message);
				tmpl.find('#loading').hide();
				return;
			}

			// Формирование полей выбора значений
			var pattern = jQuery('#values_sample').children(0);
			var list_wrap = content.find('#values');
			jQuery(data.values).each(function() {
				var el = pattern.clone();
				el.find('.val_title').html(this.text);
				
				var inp = el.find('.val_select > input')
				inp.val(this.value).attr('name', inp.attr('name')+'['+this.value+']');
				
				inp = el.find('.val_popular > input');
				inp.val(this.value).attr('name', inp.attr('name')+'['+this.value+']');
				
				el.appendTo(list_wrap);
			});

			// Установка сохраненных ранее значений
			var values = wrap.find('#values').val();
			if (values) {
				// content.deserialize(values, true);
				content.deserialize(values);
			}

			// Установка значения описания
			textArea.val(textAreaVal);

			// Сокрытие ненужных полей
			valModeHandler( content.find('input[name="val[mode]"]:checked').get(0) );

			// Отображение формы подбора значений
			content.show();
			tmpl.find('#loading').hide();
		},
		'json'
	);
}


function toggleCheckValues (el, className)
{
	el = jQuery(el);
	var wrap = el.closest('#values');
	if (!wrap) return;

	var inputs = wrap.find('.' + className + ' input[type="checkbox"]');
	var state = (el.attr('rel') == 1);
	
	inputs.each(function(index) {
		this.checked = (state) ? false : true;
	});

	if (state) {
		el.attr('rel', 0);
	} else {
		el.attr('rel', 1);
	}
}


function valModeHandler (el)
{
	if (!el) return;
	
	var mode = el.value;
	var wrap = jQuery(el).closest('#content');

	var row_auto_sel = wrap.find('#row_auto_sel');
	var row_values = wrap.find('#row_values');

	// Все
	if (mode == 0) {
		row_auto_sel.hide();
		row_values.hide();
	// Фиксированный
	} else if (mode == 1) {
		row_auto_sel.hide();
		row_values.show();
	// Динамический
	} else if (mode == 2) {
		row_auto_sel.show();
		var inp = row_auto_sel.find('input:checked');

		if ( inp.val() == 1) {
			row_values.hide();
		} else {
			row_values.show();
		}
	}
}


function valAutoSelectionHandler (el)
{
	var mode = el.value;
	var wrap = jQuery(el).closest('#content');

	var row_values = wrap.find('#row_values');

	if (mode == 1) {
		row_values.hide();
	} else {
		row_values.show();
	}
}


function fieldHandler (el)
{
	// Сброс выбранных значений при смене типа поля (установка начальных значений)
	var wrap = jQuery(el).closest('.row.constructor');
	var val = jQuery("#sample #values").attr("value");
	
	wrap.find('#values').val(val);
}


function onChangeModule (el)
{
	var mid = el.value;
	var id = jQuery(el).closest('form').get(0).id.value;
	var menu = jQuery('#cfg_menus')[0];
	var cats = jQuery('#cfg_cats')[0];
	var manufacturers = jQuery('#cfg_manufacturers')[0];

	// Блокировка списков меню и категорий
	menu.disabled = true;
	cats.disabled = true;
	manufacturers.disabled = true;
	
	// Отправка запроса на получение списков блокировок
	jQuery.getJSON( 
		'index.php?option=com_jshopping&controller=jsfilter',
		{
			'task'	: 'get_locks',
			'mid'	: mid,
			'id' 	: id
		},
		function (data, status)
		{
			// Снятие блокировки с обновлямых элементов
			menu.disabled = false;
			cats.disabled = false;
			manufacturers.disabled = false;
			
			if (status != 'success') return;

			if (!data.status) {
				alert(data.message);
				return;
			}

			// Обновление блокировок
			SF_lockListMenu = data.menus;
			SF_lockListCats = data.cats;
			SF_lockListManuf = data.manufacturers;
			updateLocks();
		}
	);
}


function updateLocks () {

	// var menu = jQuery('#cfg_menus');
	// syncSelectionHandler( menu.get(0) );
// 
	// var cats = jQuery('#cfg_cats');
	// syncSelectionHandler( cats.get(0) );

	var cats = jQuery('#cfg_manufacturers');
	syncSelectionHandler( cats.get(0) );
}


function syncSelectionHandler (el)
{
	if (!el) return;

	var target = jQuery(el);

	// Проверка каждого блока на наличие выбранных позиций (в порядке убыывания приоритета)
	var targets = [ jQuery('#cfg_menus'), jQuery('#cfg_cats'), jQuery('#cfg_manufacturers') ];
	var locks = [ SF_lockListMenu, SF_lockListCats, SF_lockListManuf ];
	var active = null;

	jQuery(targets).each(function(index)
	{
		// Поиск выбранных значений
		var block = this;
		block.find('option').each(function()
		{
			if (active) {
				// Выбран блок для активации.
				// Снятие выделения со всех значений, если текущий блок не активный
				if (block != active && this.selected) {
					this.selected = false;
				}
			} else if (this.selected) {
				active = block;
			}

			var isLocked = (jQuery.inArray(this.value, locks[index]) != -1);
			if (isLocked) {
				this.selected = false;
				this.disabled = true;
			}
		});
	});

	// Деактивация неактивных блоков
	jQuery(targets).each(function() {
		if (!active || this == active) {
			this[0].disabled = false;
		} else {
			this[0].disabled = true;
		}
	});
}


function getBlockComment (textArea, fType, exVal)
{
	if (!textArea || !textArea.length) return;

	// Блокировка поля
	textArea[0].disabled = true;
	
	// Запрос на получение описания
	jQuery.getJSON( 
		'index.php?option=com_jshopping&controller=jsfilter',
		{
			'task'	: 'get_desc',
			'type'	: fType,
			'ex_val': exVal
		},
		function (data, status)
		{
			textArea[0].disabled = false;

			if (status != 'success' || !data.status) return;

			textArea.val(data.desc);

		}
	).fail(function(jqxhr, textStatus, error) {
		textArea[0].disabled = false;
		textArea.val('');
	});
}



/**
* @author Maxim Vasiliev
* Date: 21.01.2010
* Time: 14:00
*/

(function($)
{
        /**
         * jQuery.deserialize plugin
         * Fills elements in selected containers with data extracted from URLencoded string
         * @param data URLencoded data
         * @param clearForm if true form will be cleared prior to deserialization
         */
        $.fn.deserialize = function(data, clearForm)
        {
                this.each(function(){
                        deserialize(this, data, !!clearForm);
                });
        };

        /**
         * Fills specified form with data extracted from string
         * @param element form to fill
         * @param data URLencoded data
         * @param clearForm if true form will be cleared prior to deserialization
         */
        function deserialize(element, data, clearForm)
        {
                var splits = decodeURIComponent(data).split('&'),
                        i = 0,
                        split = null,
                        key = null,
                        value = null,
                        splitParts = null;

                if (clearForm)
                {
                        $('input[type="checkbox"],input[type="radio"]', element).removeAttr('checked');
                        $('select,input[type="text"],input[type="password"],input[type="hidden"],textarea', element).val('');
                }

                var kv = {};
                while(split = splits[i++]){
                        splitParts = split.split('=');
                        key = splitParts[0] || '';
                        value = (splitParts[1] || '').replace(/\+/g, ' ');
                        
                        if (key != ''){
                                if( key in kv ){
                                        if( $.type(kv[key]) !== 'array' )
                                                kv[key] = [kv[key]];
                                        
                                        kv[key].push(value);
                                }else
                                        kv[key] = value;                                
                        }
                }

                for( key in kv ){
                        value = kv[key];
                        
                        $('input[type="checkbox"][name="'+ key +'"][value="'+ value +'"],input[type="radio"][name="'+ key +'"][value="'+ value +'"]', element).attr('checked', 'checked');
                        $('select[name="'+ key +'"],input[type="text"][name="'+ key +'"],input[type="password"][name="'+ key +'"],input[type="hidden"][name="'+ key +'"],textarea[name="'+ key +'"]', element).val(value);
                }
        }
})(jQuery);

