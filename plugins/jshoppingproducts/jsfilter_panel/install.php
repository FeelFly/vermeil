<?php

// No direct access.
defined('_JEXEC') or die;


class plgJshoppingproductsJsfilter_panelInstallerScript
{
	function install (&$plg)
	{
		// Включение плагина
		$table = JTable::getInstance('extension');
		$table->load( array('element' => 'jsfilter_panel', 'folder' => 'jshoppingproducts') );
		$table->publish();

		return true;
	}


	function uninstall (&$plg)
	{
		return true;
	}


	function update (&$plg)
	{
		$this->install($plg);
	}
}
