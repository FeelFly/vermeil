<?php

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

require_once(JPATH_ROOT.'/modules/mod_jsfilter/helper/helper.php');


class plgJshoppingproductsJsfilter_panel extends JPlugin
{
	public function __construct (&$subject, $config)
	{
		parent::__construct($subject, $config);

		$lang = JFactory::getLanguage();
		$lang->load( 'mod_jsfilter', JPATH_ROOT );
	}


	public function onBeforeDisplayProductListView (&$view)
	{
		$task = JRequest::getCmd('controller');
		$baseCfg = JsfilterHelper::getBaseCfg();

		// Принудительный вывод панели в результатах фильтрации
		// Замена стандартной панели на страницах магазина (опция)
		if ($task != 'jsfilter' && !$baseCfg->replace_panel) return false;

		// Замена панели
		$this->buildPanel($view);

		return true;
	}


	private function buildPanel (&$view)
	{
		$app = JFactory::getApplication();
		$baseCfg = JsfilterHelper::getBaseCfg();
		$jsCfg = &$view->config;
		$cfgId = $view->cfgId;


		// Поиск актуального пути с шаблоном для магазина
		$list = $view->get('_path');
		$path = '';

		foreach ($list['template'] as &$tmpl)
		{
			if ( JFolder::exists($tmpl) )
			{
				$path = $tmpl;
				break;
			}
		}

		$path = ($path)
				? dirname($path.'../'.$view->template_block_form_filter)
				: dirname($jsCfg->template_path.$jsCfg->template.'/'.$view->template_block_form_filter);

		// Проверка наличия файла шаблона замены панели
		if ( !JFile::exists($path.'/sortpanel_link.php') )
		{
			$result = JFile::copy(dirname(__FILE__).'/sortpanel_link.php', $path.'/sortpanel_link.php');
			if (!$result) return false;
		}

		// Замена шаблона панели
		$view->template_block_form_filter = 'list_products/sortpanel_link.php';


		// Формирование параметров для панели
		$helper	= new JsfilterHelper();
		$baseCfg = JsfilterHelper::getBaseCfg();
		

		$context = "jshoping.searclist.front.product";
		if (!$view->orderby)
		{
			$view->assign( 'orderby', $app->getUserStateFromRequest($context.'orderby', 'sf_orderby', $baseCfg->sort[0], 'cmd') );
		}

		if (!$view->order)
		{
			$view->assign( 'order', $app->getUserStateFromRequest($context.'order', 'sf_order', 'asc', 'cmd') );
		}

		if (!$view->limit)
		{
			if ($view->pagination_obj)
			{
				$limit = $view->pagination_obj->limit;
			}
			else
			{
				$limit = $app->getUserStateFromRequest($context.'limit', 'sf_limit', $jsCfg->count_products_to_page, 'int');
			}
			
			if (!$limit) $limit = $jsCfg->count_products_to_page;

			$view->assign('limit', $limit);
		}

		// Общее кол-во товаров
		$view->product_count = (int) $view->product_count;
		if (!$view->product_count)
		{
			$total = count($view->rows);
			if ($view->pagination_obj)
			{
				$total = $view->pagination_obj->total;
			}

			$view->assign('product_count', $total);
		}

		if (!$view->mid)
		{
			$view->assign('mid', -1);
		}

        // Поля сортировки
        $allSortFields = $helper->getAllSortFields();
		$sortFields = array();

		foreach ($baseCfg->sort as $fname)
		{
			if (!$fname) continue;

			$f = &$allSortFields[$fname];
			$sortFields[$f['name']] = $f['label'];
		}

		// Блок выбора количества товаров на странице фильтрации
		$limitList = array();
		if ($baseCfg->show_sortcount && $baseCfg->sortcount_list)
		{
			$sortcount = explode( ",", trim($baseCfg->sortcount_list) );
			foreach ($sortcount as $count)
			{
				$count = trim($count);

				$limitList[] = array(
					'text' => $count,
					'value' => (int) $count
				);
			}
		}


		// Блок с элементом фильтрации результата по наличию
		$stockVal = -1;
		if ($baseCfg->stock_state)
		{
			$sfData = JRequest::getVar('sf', null, '', 'array');

			if ( isset($sfData[999]['stock']['checkbox']) )
			{
				$stockVal = ( $sfData[999]['stock']['checkbox'][0] ) ? 1 : 0;
			} else {
				$stockVal = ($baseCfg->stock_state == 1) ? 1 : 0;
			}
		}


		JHtml::_('script', JUri::root().'modules/mod_jsfilter/assets/jsfilter.js');


		$view->assign('ajaxSelector',		$baseCfg->content_selector);
		$view->assign('sortFields', 		$sortFields);
        $view->assign("limitList", 			$limitList);
        $view->assign("showStockSelector", 	$baseCfg->stock_state);
        $view->assign("stockVal", 			$stockVal);
        $view->assign('show_product_count',	$baseCfg->show_sortcount);

		return true;
	}
}
